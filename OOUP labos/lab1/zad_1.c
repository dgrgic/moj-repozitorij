#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* PTRFUN je pokazivač na funkciju tipa void koja vraca pokazivac na char const */
typedef char const* (*PTRFUN)(void);

typedef struct{
	PTRFUN *vtable;
	char *name;
} Animal;

void animalPrintGreeting(Animal *animal){
	printf("%s pozdravlja: %s\n", animal->name, animal->vtable[0]());
}

void animalPrintMenu(Animal *animal){
	printf("%s voli %s\n", animal->name, animal->vtable[1]());
}

char const* dogGreet(void){
  return "vau!";
}

char const* dogMenu(void){
  return "kuhanu govedinu";
}

PTRFUN dogFunctions[2] = {
	(PTRFUN)dogGreet,
	(PTRFUN)dogMenu
};

void constructDog(Animal* dog, char *name){
	dog->name = name;
	dog->vtable = dogFunctions;
}

Animal* createDog(char *name){
	Animal* dog = (Animal*)malloc(sizeof(Animal));
	constructDog(dog, name);
	return dog;
}

char const* catGreet(void){
  return "mijau!";
}

char const* catMenu(void){
  return "konzerviranu tunjevinu";
}

PTRFUN catFunctions[2] = {
	(PTRFUN)catGreet,
	(PTRFUN)catMenu
};

void constructCat(Animal* cat, char *name){
	cat->name = name;
	cat->vtable = catFunctions;
}

Animal* createCat(char *name){
	Animal* cat = (Animal*)malloc(sizeof(Animal));
	constructCat(cat, name);
	return cat;
}

Animal* createNDogs(int n){
	int i;
	Animal* dogs = malloc(n*sizeof(Animal));
	for(i=0; i<n; i++){
		constructDog(&dogs[i],"name");
		printf("Dog %d created\n", i+1);
	}
	return dogs;
}

void testAnimals(void){
  Animal* p1 = createDog("Hamlet");
  Animal* p2 = createCat("Ofelija");
  Animal* p3 = createDog("Polonije");

  animalPrintGreeting(p1);
  animalPrintGreeting(p2);
  animalPrintGreeting(p3);

  animalPrintMenu(p1);
  animalPrintMenu(p2);
  animalPrintMenu(p3);

  free(p1); free(p2); free(p3);
}

int main(void){
	Animal* dogs;
	testAnimals();
	dogs = createNDogs(5);
	free(dogs);
	return 0;
}
