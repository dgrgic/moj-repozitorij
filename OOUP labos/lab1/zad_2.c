#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef enum {false, true} bool;

struct Unary_Function;
/* ptrfun je pokazivac na funkciju koja vraca double a prima "this" i double*/
typedef double (*ptrfun)(struct Unary_Function*, double);

typedef struct Unary_Function{
	ptrfun *vtable;
	int lower_bound;
	int upper_bound;
}Unary_Function;

double negative_value_at(Unary_Function* uf, double x){
	ptrfun pfun = (ptrfun)uf->vtable[0];
	return -pfun(uf,x);
}

void tabulate(Unary_Function* uf){
	int x;
	ptrfun pfun = (ptrfun)uf->vtable[0];
	for(x = uf->lower_bound; x <= uf->upper_bound; x++){
		printf("f(%d)=%lf\n", x, pfun(uf, x));
	}
}

bool same_functions_for_ints(Unary_Function* f1, Unary_Function* f2, double tolerance){
	int x;
	double delta;
	ptrfun pfun1 = (ptrfun)f1->vtable[0];
	ptrfun pfun2 = (ptrfun)f2->vtable[0];
	if(f1->lower_bound != f2->lower_bound){
		return false;
	}
	if(f1->upper_bound != f2->upper_bound){
		return false;
	}
	for(x = f1->lower_bound; x <= f1->upper_bound; x++) {
        delta = pfun1(f1,x) - pfun2(f2, x);
        if(delta < 0){
        	delta = -delta;
        }
        if(delta > tolerance){
        	return false;
        }
      }
      return true;
}

ptrfun unary_vtable[2]={
	(ptrfun)NULL,
	(ptrfun)negative_value_at
};

void construct_unary(Unary_Function* uf, int lb, int ub){
	uf->lower_bound = lb;
	uf->upper_bound = ub;
	uf->vtable = unary_vtable;
}

Unary_Function* create_unary(int lb, int ub){
	Unary_Function* uf = malloc(sizeof(Unary_Function));
	construct_unary(uf, lb, ub);
	return uf;
}

 /* square mora imati atribute kao unary jer se treba alocirati memorija jednaka (veca) unary*/
typedef struct{
	ptrfun *vtable;
	int lower_bound;
	int upper_bound;
}Square;

double square_value_at(Square* sq, double x){
	return x*x;
}

ptrfun square_vtable[2] = {
	(ptrfun)square_value_at,
	(ptrfun)negative_value_at,
};

void construct_square(Square* sq, int lb, int ub){
	construct_unary((Unary_Function*) sq, lb, ub);
	sq->vtable = square_vtable;
}

Square* create_square(int lb, int ub){
	Square* sq = malloc(sizeof(Square));
	construct_square(sq, lb, ub);
	return sq;
}

typedef struct{
	ptrfun *vtable;
	int lower_bound;
	int upper_bound;
	double a;
	double b;
}Linear;

double linear_value_at(Linear* lin, double x){
	return (lin->a)*x+(lin->b);
}

ptrfun linear_vtable[2]={
	(ptrfun)linear_value_at,
	(ptrfun)negative_value_at
};

void construct_linear(Linear* lin, int lb, int ub, double a, double b){
	construct_unary((Unary_Function*) lin, lb, ub);
	lin->a = a;
	lin->b = b;
	lin->lower_bound = lb;
	lin->upper_bound = ub;
	lin->vtable = linear_vtable;
}

Linear* create_linear(int lb, int ub, double a, double b){
	Linear* lin = malloc(sizeof(Linear));
	construct_linear(lin,lb,ub,a,b);
	return lin;
}

int main(void){

	Unary_Function* f1 = (Unary_Function*)create_square(-2,2);
	Unary_Function* f2 = (Unary_Function*)create_linear(-2,2,5,-2);
	ptrfun pfun = (ptrfun)f2->vtable[1];

	tabulate(f1);
	tabulate(f2);
	printf("f1==f2: %s\n", same_functions_for_ints(f1,f2,1E-6) ? "DA" : "NE");
	printf("neg_val f2(1) = %lf\n", pfun(f2, 1.0));
	free(f1);
	free(f2);
	return 0;
}
