#include <stdio.h>
#include <iostream>
class B{
public:
	virtual int prva()=0;
	virtual int druga()=0;
};

class D: public B{
public:
	virtual int prva(){return 0;}
	virtual int druga(){return 42;}
};

typedef int (*pfun)();

void funkcija(B* pb){
//	std::cout << "Adresa objekta " << "\n";
//	std::cout << pb << "\n";

	pfun** pointerNaVirtualnu = (pfun**) pb;

	pfun* virtualna = *pointerNaVirtualnu;
	pfun f1 = *virtualna;
	pfun f2 = *(virtualna+1);

	std::cout << f1() << "\n" << f2();
}

int main(){
	D b;
	funkcija(&b);
	return 0;
}
