#include <stdio.h>
#include <iostream>
class CoolClass{
	public:
		virtual void set(int x){x_=x;};
		virtual int get(){return x_;};
	private:
		int x_;
};
class PlainOldClass{
	public:
		void set(int x){x_=x;};
		int get(){return x_;};
	private:
		int x_;
};

int main(){
	CoolClass cc;
	PlainOldClass poc;
	std::cout << sizeof(cc) << "\n";
	std::cout << sizeof(poc);
}
