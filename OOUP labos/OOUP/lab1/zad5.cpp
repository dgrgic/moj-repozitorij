#include <stdio.h>
#include <stdlib.h>

class B{
public:
  virtual int prva()=0;
  virtual int druga()=0;
};

class D: public B{
public:
  virtual int prva(){return 0;}
  virtual int druga(){return 42;}
};

typedef int (*pfun)();

void function(B* pb){

	pfun** pvirtualna = (pfun**) pb;
	pfun* virtualna = *pvirtualna;
	pfun f1 = *virtualna;
	pfun f2 = *(virtualna+1);

	printf("%d\n", f1());
	printf("%d\n", f2());

}

void functionJedanCast(B* pb){
	pfun f1 = **( (pfun**) pb);
	pfun f2 = *(*( (pfun**) pb)+1);

	printf("%d\n", f1());
	printf("%d\n", f2());
}

int main(){
	D b;
	function(&b);
	printf("A sad s jednim castom\n");
	functionJedanCast(&b);
}