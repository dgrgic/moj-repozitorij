#include <stdio.h>
#include <stdlib.h>
#define bool int
#define true 1
#define false 0

/*typedef void (*funptr)(void);*/


struct Unary_Function;

typedef double (*funptrDUD)(struct Unary_Function*, double);
typedef void (*funptrVU)(struct Unary_Function*);
/* ==============================================
   RAZRED Unary Function
   ============================================== */

typedef struct Unary_Function{
  funptrDUD *vtable;
  int lower_bound;
  int upper_bound;

} Unary_Function;




//void tabulate

void tabulate(Unary_Function* uf){
  int x;
  for(x = uf->lower_bound; x <= uf->upper_bound; x++) {
    printf("f(%d)=%lf\n", x, uf->vtable[0](uf,x));
  }
}

//

funptrDUD UnaryFunctionVTable[2] = {
  (funptrDUD) NULL, /*UnaryFunctionValueAt*/
  (funptrDUD) NULL

};

static bool same_functions_for_ints(Unary_Function *f1, Unary_Function *f2, double tolerance) {
  int x;
  double delta;

  if(f1->lower_bound != f2->lower_bound) return false;
  if(f1->upper_bound != f2->upper_bound) return false;

  for(x = f1->lower_bound; x <= f1->upper_bound; x++) {
    delta = f1->vtable[0](f1,x) - f2->vtable[0](f1,x);
    if(delta < 0) 
      delta = -delta;
    if(delta > tolerance) 
      return false;
  }
  return true;
}

void constructUnaryFunction(Unary_Function *u, int lb, int ub){
  u->vtable = UnaryFunctionVTable;
  u->lower_bound = lb;
  u->upper_bound = ub;
}

Unary_Function* createUnaryFunction(int lb, int ub){
  Unary_Function *u;
  u = malloc(sizeof(Unary_Function));
  constructUnaryFunction(u, lb, ub);
  return u;
}


/* ==============================================
   RAZRED Square
   ============================================== */

typedef struct {
  funptrDUD *vtable;
  int lower_bound;
  int upper_bound;

} Square;

double SquareValueAt(Square *s, double x){
  return x*x;
}

double SquareNegativeValueAt(Square *s, double x){
  return -SquareValueAt(s, x);
}


funptrDUD SquareVTable[2] = {
  (funptrDUD) SquareValueAt,
  (funptrDUD) SquareNegativeValueAt

};

void constructSquare(Square *s, int lb, int ub){
  constructUnaryFunction((Unary_Function*) s, lb, ub);
  s->vtable = SquareVTable;
}

Square* createSquare(int lb, int ub){
  Square *s;
  s = malloc(sizeof(Square));
  constructSquare(s, lb, ub);
  return s;
}

/* ==============================================
   RAZRED Linear
   ============================================== */

typedef struct {
  funptrDUD *vtable;
  int lower_bound;
  int upper_bound;
  double a;
  double b;

} Linear;

double LinearValueAt(Linear *l, double x){
  return (l->a)*(x) + (l->b);
}

double LinearNegativeValueAt(Linear *l, double x){
  return -LinearValueAt(l, x);
}

funptrDUD LinearVTable[2] = {
  (funptrDUD) LinearValueAt,
  (funptrDUD) LinearNegativeValueAt

};

void constructLinear(Linear *l, int lb, int ub, double a_coef, double b_coef){
  constructUnaryFunction((Unary_Function*) l, lb, ub); 
  l->vtable = LinearVTable;
  l->a = a_coef;
  l->b = b_coef;
}

Linear* createLinear(int lb, int ub, int a_coef, int b_coef){
  Linear *l;
  l = malloc(sizeof(Linear));
  constructLinear(l, lb, ub, a_coef, b_coef);
  return l;
}

/* ==============================================
   FUNKCIJA main
   ============================================== */

int main() {
  Unary_Function *f1, *f2;

  f1 = (Unary_Function*) createSquare(-2, 2);
  tabulate(f1);
  f2 = (Unary_Function*) createLinear(-2, 2, 5, -2);
  tabulate(f2);

  printf("f1==f2: %s\n", same_functions_for_ints(f1, f2, 1E-6) ? "DA" : "NE");
  printf("neg_val f2(1) = %lf\n", f2->vtable[1](f2,1.0));
  free(f1);
  free(f2);
  return 0;
}


/*-------------------C C PLUS PLUS O------------------*/

/*



#include <stdio.h>
#include <stdlib.h>

class Unary_Function {
  private:
    int lower_bound;
    int upper_bound;
  public:
    Unary_Function(int lb, int ub) : lower_bound(lb), upper_bound(ub) {};
    virtual double value_at(double x) = 0;
    virtual double negative_value_at(double x) {
      return -value_at(x);
    }
    void tabulate() {
      for(int x = lower_bound; x <= upper_bound; x++) {
        printf("f(%d)=%lf\n", x, value_at(x));
      }
    };
    static bool same_functions_for_ints(Unary_Function *f1, Unary_Function *f2, double tolerance) {
      if(f1->lower_bound != f2->lower_bound) return false;
      if(f1->upper_bound != f2->upper_bound) return false;
      for(int x = f1->lower_bound; x <= f1->upper_bound; x++) {
        double delta = f1->value_at(x) - f2->value_at(x);
        if(delta < 0) delta = -delta;
        if(delta > tolerance) return false;
      }
      return true;
    };
};

class Square : public Unary_Function {
  public:
    Square(int lb, int ub) : Unary_Function(lb, ub) {};
    virtual double value_at(double x) {
      return x*x;
    };
};

class Linear : public Unary_Function {
  private:
    double a;
    double b;
  public:
    Linear(int lb, int ub, double a_coef, double b_coef) : Unary_Function(lb, ub), a(a_coef), b(b_coef) {};
    virtual double value_at(double x) {
      return a*x + b;
    };
};

int main() {
  Unary_Function *f1 = new Square(-2, 2);
  f1->tabulate();
  Unary_Function *f2 = new Linear(-2, 2, 5, -2);
  f2->tabulate();
  printf("f1==f2: %s\n", Unary_Function::same_functions_for_ints(f1, f2, 1E-6) ? "DA" : "NE");
  printf("neg_val f2(1) = %lf\n", f2->negative_value_at(1.0));
  delete f1;
  delete f2;
  return 0;
}

*/