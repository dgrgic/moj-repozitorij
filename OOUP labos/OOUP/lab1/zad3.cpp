#include <stdio.h>
#include <stdlib.h>

class CoolClass{
  public:
    virtual void set(int x){x_=x;};
    virtual int get(){return x_;};
  private:
    int x_;
};

class PlainOldClass{
  public:
    void set(int x){x_=x;};
    int get(){return x_;};
  private:
    int x_;
};


int main() {
  printf("sizeof(PlainOldClass) = %d\n", sizeof(PlainOldClass));  // = 4
  printf("sizeof(CoolClass) = %d\n", sizeof(CoolClass)); // = 8


  CoolClass c;
  PlainOldClass p;
  printf("sizeof(PlainOldClass) c = %d\n", sizeof(c));  // = 4
  printf("sizeof(CoolClass) p = %d\n", sizeof(p)); // = 8
  return 0;
}