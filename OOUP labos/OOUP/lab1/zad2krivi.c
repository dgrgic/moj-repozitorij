#include <stdio.h>
#include <stdlib.h>

/*typedef char const* (*PTRFUN)(); */

typedef double (*FunPtrDD)(double);
typedef void (*FunPtrDUD)(Unary_Function*, double);
typedef void (*FunPtrVU)(Unary_Function*);
typedef static bool (*FunPtrBool)(Unary_Function*, Unary_Function*, double);


void tabulate(Unary_Function *u){
  int x, lb, ub;
  lb = u->lower_bound;
  ub = u->upper_bound;
  for(int x = lb; x <= ub; x++) {
        printf("f(%d)=%lf\n", x, u->value_at_ptr(x));
      }

}

bool same_functions_for_ints(Unary_Function *f1, Unary_Function *f2, double tolerance) {
  if(f1->lower_bound != f2->lower_bound) return false;
  if(f1->upper_bound != f2->upper_bound) return false;
  for(int x = f1->lower_bound; x <= f1->upper_bound; x++) {
    double delta = f1->value_at(x) - f2->value_at(x);
    if(delta < 0) delta = -delta;
    if(delta > tolerance) return false;
  }
  return true;
}

double negative_value_at(Unary_Function* u, double x){
  return u->value_at_ptr(x);

}

typedef struct {
  int lower_bound;
  int upper_bound;
  FunPtrDUD value_at_ptr;
  FunPtrDUD negative_value_at_ptr = &negative_value_at;
  FunPtrVU tabulate_ptr = &tabulate;
  /*FunPtrBool same_functions_for_ints_ptr;*/

} Unary_Function;

double squareValueAt (Unary_Function* u, double x){
  return x*x;

}

double linearValueAt (Unary_Function* u, double x){

}

Unary_Function* newSquare(int lb, int ub){
  Unary_Function* s;
  s = malloc(sizeof(Unary_Function));
  s->lower_bound = lb;
  s->upper_bound = ub;

  s->value_at_ptr = &squareValueAt;



}

Unary_Function* newLinear(int lb, int ub, int a_coef, int b_coef){
  
}

/*---------------------------------------------------------------*/

class Unary_Function {
  private:
    int lower_bound;
    int upper_bound;
  public:
    Unary_Function(int lb, int ub) : lower_bound(lb), upper_bound(ub) {};
    virtual double value_at(double x) = 0;
    virtual double negative_value_at(double x) {
      return -value_at(x);
    }
    void tabulate() {
      for(int x = lower_bound; x <= upper_bound; x++) {
        printf("f(%d)=%lf\n", x, value_at(x));
      }
    };
    static bool same_functions_for_ints(Unary_Function *f1, Unary_Function *f2, double tolerance) {
      if(f1->lower_bound != f2->lower_bound) return false;
      if(f1->upper_bound != f2->upper_bound) return false;
      for(int x = f1->lower_bound; x <= f1->upper_bound; x++) {
        double delta = f1->value_at(x) - f2->value_at(x);
        if(delta < 0) delta = -delta;
        if(delta > tolerance) return false;
      }
      return true;
    };
};

class Square : public Unary_Function {
  public:
    Square(int lb, int ub) : Unary_Function(lb, ub) {};
    virtual double value_at(double x) {
      return x*x;
    };
};

class Linear : public Unary_Function {
  private:
    double a;
    double b;
  public:
    Linear(int lb, int ub, double a_coef, double b_coef) : Unary_Function(lb, ub), a(a_coef), b(b_coef) {};
    virtual double value_at(double x) {
      return a*x + b;
    };
};

int main() {
  Unary_Function *f1 = new Square(-2, 2);
  f1->tabulate();
  Unary_Function *f2 = new Linear(-2, 2, 5, -2);
  f2->tabulate();
  printf("f1==f2: %s\n", Unary_Function::same_functions_for_ints(f1, f2, 1E-6) ? "DA" : "NE");
  printf("neg_val f2(1) = %lf\n", f2->negative_value_at(1.0));
  delete f1;
  delete f2;
  return 0;
}