#include <stdio.h>
#include <stdlib.h>
#include <string.h>


typedef char const* (*PTRFUN)();

char const* dogGreet(void){
return "vau!";
}
char const* dogMenu(void){
return "kuhanu govedinu";
}
char const* catGreet(void){
return "mijau!";
}
char const* catMenu(void){
return "konzerviranu tunjevinu";
}

/*Struktura Animal*/
typedef struct {
	char * name;
	PTRFUN* functs;
} Animal;

PTRFUN catFuncts[2];
PTRFUN dogFuncts[2];

/*ispis funkcija*/
void animalPrintGreeting(Animal* a){
	char* name = a->name;
	printf("%s pozdravlja: %s\n", name, a->functs[0]());
	return;
}

void animalPrintMenu(Animal* a){
	char* name = (*a).name;
	printf("%s voli %s\n", name, (*a).functs[1]());
	return;
}

/*konstruiranje macaka i pasa*/
void constructDog(Animal* dog, char* petName){
	(*dog).name = petName;
	(*dog).functs = dogFuncts;
	return;
}

void constructCat(Animal* cat, char* petName){
	(*cat).name = petName;
	(*cat).functs = catFuncts;
	return;
}

/*kreiranje macaka i pasa*/
Animal* createDog(char* petName){
	Animal* dog;
	dog = malloc(sizeof(Animal));
	constructDog(dog, petName);
	return dog;
}

Animal* createCat(char* petName){
	Animal* cat;
	cat = malloc(sizeof(Animal));
	constructCat(cat, petName);
	return cat;
}

/*stvaranje n pasa*/

Animal** createNDogs(int n){
	int i;

	Animal** dogs;
	dogs = (Animal**) malloc(n*sizeof(Animal));

	for (i = 0; i<n; i++){
		/*char* iDogName = concat("name", 0x30);*/
		dogs[i] = createDog("DogName");
		printf("%s%d\n", dogs[i]->name, i);

	}

	return dogs;
}

/*testiranje*/
void testAnimals(void){
  Animal* p1=createDog("Hamlet");
  Animal* p2=createCat("Ofelija");
  Animal* p3=createDog("Polonije");

  animalPrintGreeting(p1);
  animalPrintGreeting(p2);
  animalPrintGreeting(p3);

  animalPrintMenu(p1);
  animalPrintMenu(p2);
  animalPrintMenu(p3);

  free(p1); free(p2); free(p3);
}

/*main funkcija*/
int main(void){
	/*
	int i;
	char* iDogName;
	char broj = 0x30;
	*/
	Animal** doggies;

	catFuncts[0] = &catGreet;
	catFuncts[1] = &catMenu;
	dogFuncts[0] = &dogGreet;
	dogFuncts[1] = &dogMenu;

	testAnimals();

	/*
	iDogName = "name";
	
	printf("%c\n", broj);
	broj++;
	printf("%c\n", broj);

	for (i = 0; i<12; i++){
		broj++;
		printf("%c\n", broj);
	}
	
	iDogName = strncat(iDogName, &broj, 1);
	printf("%s\n", iDogName);

	*/
	
	
	doggies = createNDogs(16);
	free(doggies);
	

	return 0;
}
