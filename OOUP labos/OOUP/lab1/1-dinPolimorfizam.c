#include <stdio.h>
#include <stdlib.h>


/*funkcije za pse*/
char const* dogGreet(void){
	return "vau!";
}

char const* dogMenu(void){
	return "kuhanu govedinu";
}

/*funkcije za macke*/
char const* catGreet(void){
	return "mijau!";
}

char const* catMenu(void){
	return "konzerviranu tunjevinu";
}

/*tablica pointera na funkcije*/
typedef char const* (*PTRFUN)();

PTRFUN catFuncts[2];
PTRFUN dogFuncts[2];

/*struktura Animal*/
typedef struct {
	char * ime;
	/*tablicaFunkcija * tablica;*/
	/*char const* (*animalGreeting)();
	char const* (*animalMenu)();*/
	PTRFUN* functs;
} Animal;


/*konstruiranje macaka i pasa*/
void constructDog(Animal* dog, char* imeLjubimca){
	(*dog).ime = imeLjubimca;
	/*(*dog).animalGreeting = &dogGreet;
	(*dog).animalMenu = &dogMenu;*/
	(*dog).functs = dogFuncts;
	return;
}

void constructCat(Animal* cat, char* imeLjubimca){
	(*cat).ime = imeLjubimca;
	/*(*cat).animalGreeting = &catGreet;
	(*cat).animalMenu = &catMenu;*/
	(*cat).functs = catFuncts;
	return;
}

/*kreiranje macaka i pasa*/
Animal* createDog(char* imeLjubimca){
	Animal* dog;
	dog = malloc(sizeof(Animal));
	constructDog(dog, imeLjubimca);
	/*(*dog).ime = imeLjubimca;
	(*dog).animalGreeting = &dogGreet;
	(*dog).animalMenu = &dogMenu;*/
	return dog;
}

Animal* createCat(char* imeLjubimca){
	Animal* cat;
	cat = malloc(sizeof(Animal));
	constructCat(cat, imeLjubimca);
	/*(*cat).ime = imeLjubimca;
	(*cat).animalGreeting = &dogGreet;
	(*cat).animalMenu = &dogMenu;*/
	return cat;
}


/*ispis funkcija*/
void animalPrintGreeting(Animal* a){
	char* ime = (*a).ime;
	/*char* greeting = a->animalGreeting();*/
	printf("%s pozdravlja: %s\n", ime, a->functs[0]());
	return;
}

void animalPrintMenu(Animal* a){
	char* ime = (*a).ime;
	/*char* menu = (*a).animalMenu();*/
	printf("%s voli %s\n", ime, (*a).functs[1]());
	return;
}

/*testiranje*/
void testAnimals(void){
  Animal* p1=createDog("Hamlet");
  Animal* p2=createCat("Ofelija");
  Animal* p3=createDog("Polonije");

  animalPrintGreeting(p1);
  animalPrintGreeting(p2);
  animalPrintGreeting(p3);

  animalPrintMenu(p1);
  animalPrintMenu(p2);
  animalPrintMenu(p3);

  free(p1); free(p2); free(p3);
}

/*main funkcija*/
int main(void){
	catFuncts[0] = &catGreet;
	catFuncts[1] = &catMenu;
	dogFuncts[0] = &dogGreet;
	dogFuncts[1] = &dogMenu;

	testAnimals();

	return 0;
}