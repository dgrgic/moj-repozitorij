package model;

public abstract class Animal {
    public abstract String name();
    public abstract String greet();
    public abstract String menu();

    public void animalPrintGreeting(){
        //...
        String line = String.format( "%s pozdravlja: %s\n", this.name(), this.greet());
        System.out.println(line);
    }

    public void animalPrintMenu(){
        //...
        String line = String.format( "%s voli: %s\n", this.name(), this.menu());
        System.out.println(line);
    }

}
