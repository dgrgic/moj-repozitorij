package model.plugins;
import model.Animal;

public class Parrot extends Animal{

    private String animalName;

    public Parrot(String name){
        this.animalName = name;
    }

    @Override
    public String name() {
        return this.animalName;
    }

    @Override
    public String greet() {
        return "Sviju ti opanaka!";
    }

    @Override
    public String menu() {
        return "brazilske orascice";
    }
}
