package model.plugins;

import model.Animal;

public class Tiger extends Animal {

    private String animalName;

    public Tiger(String name){
        this.animalName = name;
    }

    @Override
    public String name() {
        return this.animalName;
    }

    @Override
    public String greet() {
        return "RAWR!";
    }

    @Override
    public String menu() {
        return "mlako mlijeko";
    }
}
