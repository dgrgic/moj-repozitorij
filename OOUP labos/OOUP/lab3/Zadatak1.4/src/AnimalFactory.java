import model.Animal;

import java.lang.reflect.Constructor;

public class AnimalFactory {
    public static Animal newInstance(String animalKind, String name){
        //...

        try {
            Class<Animal> clazz = null;
            clazz = (Class<Animal>)Class.forName("model.plugins."+animalKind);
            Constructor<?> ctr = clazz.getConstructor(String.class);
            Animal animal = (Animal) ctr.newInstance(name);
            return animal;

        } catch (Exception e) {
            System.out.println("DRAMA!!!!!!!!!!!!!!!!!!!!");
            e.printStackTrace();
        }

        return null;
    }
}
