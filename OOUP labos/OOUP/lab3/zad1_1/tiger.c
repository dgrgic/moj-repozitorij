#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "myfactory.h"
//#include "animal.h"


typedef char const* (*PTRFUN)();

/*Struktura Tiger*/
typedef struct {
	PTRFUN* functs;
	char const* name;
} Tiger;

/*Struktura Animal*/
typedef struct {
	PTRFUN* functs;
	// vtable entries:
	// 0: char const* name(void* this);
	// 1: char const* greet();
	// 2: char const* menu();
} Animal;


char const* tigerMenu(void){
	// printf(" * tigerMenu\n");
return "mlako mlijeko";
}
char const* tigerGreet(void){
	// printf(" * tigerGreet\n");
return "Mijau! Rawr";
}

char const* getTigerName(void* this){
	// printf(" * getTigerName\n");
return ((Tiger* ) this)->name;
}

PTRFUN tigerFuncts[3] = {
	(PTRFUN) getTigerName,
	(PTRFUN) tigerGreet,
	(PTRFUN) tigerMenu
};

/*konstruiranje Tigra*/
void constructTiger(Tiger* tiger, char const* petName){
	// printf("ulazim u constructTiger\n");
	//PTRFUN tigerFuncts[3];
	(*tiger).name = petName;
	// printf("NAME: %s\n", petName );


	// tigerFuncts[0] = &getTigerName;
	// tigerFuncts[1] = &tigerGreet;
	// tigerFuncts[2] = &tigerMenu;
	(*tiger).functs = tigerFuncts;

	// printf("NAME2: %s\n", tiger->functs[0](tiger) );
	return;
}

void* create(char const* petName){
	// printf("ulazim u createTiger\n");
	Tiger* tiger = malloc(sizeof(Tiger));
	//printf("LALALLALA\n");
	constructTiger(tiger, petName);

	// printf("Rezultat tiger:\n");
	// printf("\t tigerName = %s\n", tiger->name);
	// printf("\t tigerName2 = %s\n", tiger->functs[0](tiger));
	// printf("\t tigerGreet = %s\n", tiger->functs[1]());
	// printf("\t tigerFood = %s\n", tiger->functs[2]());




	// printf("izlazim iz createTiger\n");
	return tiger;
}
