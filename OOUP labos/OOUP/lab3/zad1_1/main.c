#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "myfactory.h"
//#include "animal.h"

typedef char const* (*PTRFUN)();


/*Struktura Animal*/
typedef struct {
	PTRFUN* functs;
	// vtable entries:
	// 0: char const* name(void* this);
	// 1: char const* greet();
	// 2: char const* menu();
} Animal;




// parrots and tigers defined in respective dynamic libraries

// animalPrintGreeting and animalPrintMenu similar as in lab 1
/*ispis funkcija*/
void animalPrintGreeting(Animal* a){
	// printf(" -- animalPrintGreeting\n");
	char const* name = a->functs[0](a);
	// printf("%s ++++\n", name);
	printf("%s pozdravlja: %s\n", name, a->functs[1]());
	// printf(" ++ animalPrintGreeting\n");
	return;
}

void animalPrintMenu(Animal* a){
	// printf(" -- animalPrintMenu\n");
	char const* name = (*a).functs[0](a);
	// printf("%s ++++\n", "OJ");
	printf("%s voli %s\n", name, (*a).functs[2]());
	// printf(" ++ animalPrintMenu\n");
	return;
}


/*main funkcija za provjeru*/
int main(void){
	//printf("PRVI PUT.\n");
	Animal* p1 = (Animal*)myfactory("parrot", "Modrobradi");

	
	

	//printf("DRUGI PUT.\n");
	Animal* p2 = (Animal*)myfactory("tiger", "Strasko");

	if (!p1 || !p2){
		printf("Creation of plug-in objects failed.\n");
		exit(1);
	}

	// printf("-----------FINITO-----------.\n\n");

	
	
	// printf("\nSto mu gromova?\n");
	animalPrintGreeting(p1);//"Sto mu gromova!"
	// printf("\nMijau?\n");
	animalPrintGreeting(p2);//"Mijau!"
	// printf("\nbrazilski orascici?\n");
	animalPrintMenu(p1);//"brazilske orahe"
	// printf("\nkakvo mlijeko?\n");
	animalPrintMenu(p2);//"mlako mlijeko"
	
	free(p1); free(p2);

	return 0;
}