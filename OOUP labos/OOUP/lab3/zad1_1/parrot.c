#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "myfactory.h"
//#include "animal.h"


typedef char const* (*PTRFUN)();

/*Struktura Parrot*/
typedef struct {
	PTRFUN* functs;
	char const* name;
} Parrot;

/*Struktura Animal*/
typedef struct {
	PTRFUN* functs;
	// vtable entries:
	// 0: char const* name(void* this);
	// 1: char const* greet();
	// 2: char const* menu();
} Animal;



char const* parrotMenu(void){
	// printf(" * parrotMenu\n");
return "brazilske orahe";
}
char const* parrotGreet(void){
	// printf(" * parrotGreet\n");
return "Sviju ti cuda!";
}

char const* getParrotName(void* this){
	// printf(" * getParrotName\n");
return ((Parrot* ) this)->name;
}

PTRFUN parrotFuncts[3] = {
	(PTRFUN) getParrotName,
	(PTRFUN) parrotGreet,
	(PTRFUN) parrotMenu
};


/*konstruiranje Papige*/
void constructParrot(Parrot* parrot, char const* petName){
	// printf("-- ulazim u constructParrot\n");
	//PTRFUN parrotFuncts[3];

	// printf("NAME: %s\n", petName );

	(*parrot).name = petName;
	// printf("heyhey\n");
	 //parrotFuncts[0] = &getParrotName;
	 //parrotFuncts[1] = &parrotGreet;
	 //parrotFuncts[2] = &parrotMenu;
	parrot->functs = parrotFuncts;
	

	// printf("NAME2: %s\n", parrot->functs[0](parrot) );

	// printf("\t parrotGreet = %s\n", parrot->functs[1]());
	//  printf("\t parrotFood = %s\n", parrot->functs[2]());
	// printf("++ izlazim constructParrot\n");
	return;
}

void* create(char const* petName){
	// printf("ulazim u createParrot\n");
	Parrot* parrot = malloc(sizeof(Parrot));
	constructParrot(parrot, petName);


	//  printf("Rezultat parrot:\n");
	//  printf("\t parrotName = %s\n", parrot->name);
	//  printf("\t parrotName2 = %s\n", parrot->functs[0](parrot));
	//  printf("\t parrotGreet = %s\n", parrot->functs[1]());
	//  printf("\t parrotFood = %s\n", parrot->functs[2]());

	// printf("izlazim iz createParrot\n");
	return parrot;
}