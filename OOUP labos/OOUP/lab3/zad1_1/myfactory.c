#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include <dlfcn.h>
#include <windows.h>

//#include "animal.h"

char* concat(const char *s1, const char *s2)
{
    char *result = malloc(strlen(s1)+strlen(s2)+1);//+1 for the null-terminator
    //in real code you would check for errors in malloc here
    strcpy(result, s1);
    strcat(result, s2);
    return result;
}

typedef char const* (*PTRFUN)();

/*Struktura Animal*/
typedef struct {
	PTRFUN* functs;
	// vtable entries:
	// 0: char const* name(void* this);
	// 1: char const* greet();
	// 2: char const* menu();
} Animal;

typedef Animal* (*CREFUNC)(char const*);


//typedef-at CREFUNC pointer

void* myfactory(char const* libname, char const* ctorarg){

	// printf("ulazim u myFactory\n");
	// printf("\t libname = %s, ctorarg = %s\n", libname, ctorarg);
	/*
		libname - vrsta zivotinje
		ctorarg - ime zivotinje
		
	*/
	HINSTANCE hinstLib;

	char const* fullLibname = (char const*) concat("./",libname);
	fullLibname = (char const*) concat(fullLibname, ".dll");
	//fullLibname = (char const*) concat("K:\\Labs\\OOUP\\lab3\\zad1\\",fullLibname);

	// printf("\t fullLibname = %s\n", fullLibname);

	hinstLib = LoadLibrary( TEXT(fullLibname) );
	//printf("\t hinstLib = %s\n", hinstLib);

	if (hinstLib != NULL){
	CREFUNC createFunc = (CREFUNC) GetProcAddress(hinstLib, "create");

	Animal* animal = createFunc(ctorarg);

	// printf(" \n");

	// printf("MY FACTORY Rezultat animal:\n");
	// printf("\t animalName2 = %s\n", animal->functs[0](animal));
	// printf("\t animalGreet = %s\n", animal->functs[1]());
	// printf("\t animalFood = %s\n", animal->functs[2]());

	return animal;
	}

	FreeLibrary(hinstLib);

	printf("Zabranjeno voce\n");


	// printf("Rezultat animal:\n");
	// //printf("\t animalName = %s\n", animal->functs[0](animal));
	// printf("\t animalGreet = %s\n", animal->functs[1]());
	// printf("\t animalFood = %s\n", animal->functs[2]());

	printf("\n");

	return NULL;


}

