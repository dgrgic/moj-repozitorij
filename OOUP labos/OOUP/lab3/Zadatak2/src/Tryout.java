import TextEditorPack.TextEditorModel;

import java.util.Iterator;

public class Tryout {

    public static void main(String[] args){
        TextEditorModel tem = new TextEditorModel("HEy!\nWhat's up?\nIs u OK?\nSto cemo od zivota");

        Iterator<String> i = tem.allLines();
        System.out.println("**** prvi iterator");
        while (i.hasNext()){
            System.out.println(i.next());
        }

        i = tem.linesRange(1,3);
        System.out.println("**** drugi iterator");
        while (i.hasNext()){
            System.out.println(i.next());
        }
    }
}
