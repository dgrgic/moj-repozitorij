package LocationPack;

public class Location {
    private int row;
    private int col;

    public Location(int row, int col) {
        this.row = row;
        this.col = col;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getCol() {
        return col;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public boolean isGreaterThan(Location l){
        if (this.getRow() > l.getRow()){
            //System.out.format("1");
            return true;
        } else if (this.getRow() < l.getRow()){
            //System.out.format("2");
            return  false;
        } else {
            if (this.getCol() > l.getCol()){
                //System.out.format("3");
                return true;
            } else {
                //System.out.format("4");
                return false;
            }
        }
    }
}
