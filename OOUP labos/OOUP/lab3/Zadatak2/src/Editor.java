

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Editor extends JFrame{
    /*
     * Omogucava prikaz i jednostavno uredjivanje teksta
     *   TextEditorModel - enkapsulira podatke o tekstu
     *                   - polozaju kursora
     *                   - selekciji teksta
     *
     * */

    private JTextArea textArea = new JTextArea(20, 60);
    private JFileChooser fc = new JFileChooser();



    //CONSTRUCTOR
    public Editor(){
        JScrollPane scrollPane = new JScrollPane(textArea,
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

        //FILTER FOR TXT FILES
        FileFilter txtFilter = new FileNameExtensionFilter("Plain text", "txt");
        fc.setFileFilter(txtFilter);

        //MENU AND MENU ITEMS
        add(scrollPane);
        JMenuBar menuBar = new JMenuBar();
        setJMenuBar(menuBar);
        JMenu file = new JMenu("File");
        menuBar.add(file);

        //ADDING ACTIONS TO FILE MENU ITEMS
        file.add(Open);
        file.add(Save);
        file.addSeparator();
        file.add(Exit);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        //setSize(300, 300);
        setVisible(true);
    }

    //ACTIONS
    Action Open = new AbstractAction("Open File") {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION){
                openFile(fc.getSelectedFile().getAbsolutePath());
            }
        }
    };

    Action Save = new AbstractAction("Save File") {
        @Override
        public void actionPerformed(ActionEvent e) {
            saveFile();
        }
    };

    Action Exit = new AbstractAction("Exit") {
        @Override
        public void actionPerformed(ActionEvent e) {
            System.exit(0);
        }
    };

    //METHODS
    public void openFile(String fileName){
        FileReader fr = null;
        try {
            System.out.println("Duplicate");
            fr = new FileReader(fileName);
            textArea.read(fr, null);
            fr.close();
            setTitle(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveFile(){
        if (fc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION){
            FileWriter fw = null;
            try {
                System.out.println("Duplicate");
                fw = new FileWriter(fc.getSelectedFile().getAbsoluteFile() + ".txt");
                textArea.write(fw);
                fw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args){
        new Editor();
    }
}
