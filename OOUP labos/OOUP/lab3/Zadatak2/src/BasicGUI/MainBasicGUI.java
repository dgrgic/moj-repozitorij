package BasicGUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class MainBasicGUI extends JFrame{

    public MainBasicGUI() {
        setTitle("Zadatak 2.1");
        setSize(960, 960);
        setVisible(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

    }

    @Override
    public void paint(Graphics g){
        super.paint(g);
        int width = getWidth();
        int height = getHeight();
        g.setColor(Color.RED);
        g.drawLine(0, height/2, width, height/2);
        g.drawLine( width/2, 0, width/2, height);
        g.setColor(Color.GREEN);
        g.drawLine(0, 300, 960, 300);
        g.drawLine(100, 0, 100, 960);

        g.setColor(Color.BLACK);
        g.setFont(new Font(null, Font.BOLD, 30));
        g.drawString("JAVA IS COOL!! ", 200, 200);
        g.setFont(new Font(null, Font.BOLD, 20));
        g.drawString("PYTHON IS BETTER", 200, 400);
    }

    public static void main(String[] args){
        MainBasicGUI window = new MainBasicGUI();
        window.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER ){
                    System.out.println("Enter typed!");
                }
                
            }

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER ){
                    System.out.println("Enter pressed!");
                    window.setVisible(false);
                    window.dispose();
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER ){
                    System.out.println("Enter released!");
                }

            }
        });

    }
}
