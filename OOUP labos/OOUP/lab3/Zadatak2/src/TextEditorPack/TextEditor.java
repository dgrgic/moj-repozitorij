package TextEditorPack;

import LocationPack.Location;
import LocationPack.LocationRange;
import TextEditorPack.ClipboardPack.ClipboardStack;
import TextEditorPack.CursorPack.CursorObserver;
import TextEditorPack.TextObserverPack.TextObserver;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Rectangle2D;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

public class TextEditor extends JFrame implements CursorObserver, TextObserver{
    /*
     * Omogucava prikaz i jednostavno uredjivanje teksta
     *   TextEditorModel - enkapsulira podatke o tekstu
     *                   - polozaju kursora
     *                   - selekciji teksta
     *
     * */

    private JTextArea textArea = new JTextArea(20, 60);
    private JFileChooser fc = new JFileChooser();
    private TextEditorModel teModel;
    private Location cursorLocation = new Location(0,0);
    private ClipboardStack clipboard = new ClipboardStack();
    private JLabel cursorLabel;

    public JTextArea getTextArea() {
        return textArea;
    }

    public JFileChooser getFc() {
        return fc;
    }

    //CONSTRUCTOR
    public TextEditor(){
        /*
        JScrollPane scrollPane = new JScrollPane(textArea,
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

        //FILTER FOR TXT FILES
        FileFilter txtFilter = new FileNameExtensionFilter("Plain text", "txt");
        fc.setFileFilter(txtFilter);
        */

        cursorLabel = new JLabel("Cursor: row - 0, column - 0");
        add(cursorLabel);
        cursorLabel.setVerticalAlignment(JLabel.BOTTOM);


        //MENU AND MENU ITEMS
        //add(scrollPane);
        JMenuBar menuBar = new JMenuBar();
        setJMenuBar(menuBar);
        JMenu file = new JMenu("File");
        menuBar.add(file);
        JMenu edit = new JMenu("Edit");
        menuBar.add(edit);
        JMenu move = new JMenu("Move");
        menuBar.add(move);

        //ADDING ACTIONS TO FILE MENU ITEMS
        file.add(Open);
        file.add(Save);
        file.addSeparator();
        file.add(Exit);

        //ADDING ACTIONS TO EDIT MENU ITEMS
        edit.add(Undo);
        edit.add(Redo);
        edit.addSeparator();
        edit.add(Cut);
        edit.add(Copy);
        edit.add(Paste);
        edit.add(PasteAndTake);
        edit.addSeparator();
        edit.add(DeleteSelection);
        edit.add(ClearDocument);

        //ADDING ACTIONS TO MOVE MENU ITEMS
        move.add(CursorStart);
        move.add(CursorEnd);





        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        pack();
        setLocation(100, 100);
        setSize(400, 900);
        setVisible(true);

        //DODAVANJE LISTENERA NA KEYSTROKES - MICANJE KURSORA
        this.addKeyListener(new KeyListener() {
            private Location startLocation = new Location(0,0);
            private boolean shift = false;
            private boolean availableForClipboard;

            @Override
            public void keyTyped(KeyEvent e) {
                return;
            }

            @Override
            public void keyPressed(KeyEvent e) {



                if (this.shift == false && teModel.getSelectionRange() != null){
                    availableForClipboard = true;
                    //System.out.println(availableForClipboard);
                    //printLines(teModel.getSelectedRows());
                } else if (e.isControlDown() && availableForClipboard){
                    //System.out.println(availableForClipboard);
                    System.out.println("To je druga prica vec");
                } else {
                    availableForClipboard = false;
                    //System.out.println(availableForClipboard);
                }

                // CTRL + C, X, V KOMBINACIJE
                if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_C) {
                    System.out.println("ctrl + c");

                    if (availableForClipboard){
                        clipboard.push(teModel.getSelectedRows());
                    }

                    teModel.setSelectionRange(null);
                    this.startLocation = new Location(0,0);

                } else if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_X) {
                    System.out.println("ctrl + x");
                    if (availableForClipboard){
                        //System.out.println(teModel.getSelectedRows());
                        clipboard.push(teModel.getSelectedRows());
                        teModel.deleteRange(teModel.getSelectionRange());
                    }

                    teModel.setSelectionRange(null);
                    this.startLocation = new Location(0,0);

                } else if (e.isControlDown() && e.isAltDown() &&
                        e.getKeyCode() == KeyEvent.VK_V) {
                    System.out.println("ctrl + alt + v");
                    if (!clipboard.empty()){
                        List<String> retci = clipboard.pop();
                        teModel.insert(retci);

                        teModel.setSelectionRange(null);
                        this.startLocation = new Location(0,0);
                    } else {
                        System.out.println("Nema se sta nalijepit :p");
                    }
                } else if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_V) {
                    System.out.println("ctrl + v");
                    if (!clipboard.empty()){
                        List<String> retci = clipboard.pop();
                        teModel.insert(retci);
                        clipboard.push(retci);

                        teModel.setSelectionRange(null);
                        this.startLocation = new Location(0,0);
                    } else {
                        System.out.println("Nema se sta nalijepit :p");
                    }
                }

                //SHIFT
                else if (e.getKeyCode() == KeyEvent.VK_SHIFT){
                    System.out.println("Shift pressed");
                    this.startLocation.setRow( teModel.getCursorLocation().getRow() );
                    this.startLocation.setCol( teModel.getCursorLocation().getCol() );
                    this.shift = true;
                }
                /*
                * PONASANJE ZA MICANJE KURSORA
                 */
                else if (e.getKeyCode() == KeyEvent.VK_KP_LEFT ||
                        e.getKeyCode() == KeyEvent.VK_LEFT){
                    boolean mozeLi = teModel.moveCursorLeft();
                    if (mozeLi){
                        repaint();
                    } else {
                        System.out.println("Moze li? Reka san NE MOZE!");
                    }
                }

                else if (e.getKeyCode() == KeyEvent.VK_KP_RIGHT ||
                        e.getKeyCode() == KeyEvent.VK_RIGHT){
                    boolean mozeLi = teModel.moveCursorRight();
                    if (mozeLi){
                        repaint();
                    } else {
                        System.out.println("Moze li? Reka san NE MOZE!");
                    }
                }

                else if (e.getKeyCode() == KeyEvent.VK_KP_UP ||
                        e.getKeyCode() == KeyEvent.VK_UP){
                    boolean mozeLi = teModel.moveCursorUp();
                    if (mozeLi){
                        repaint();
                    } else {
                        System.out.println("Moze li? Reka san NE MOZE!");
                    }
                }

                else if (e.getKeyCode() == KeyEvent.VK_KP_DOWN ||
                        e.getKeyCode() == KeyEvent.VK_DOWN){
                    boolean mozeLi = teModel.moveCursorDown();
                    if (mozeLi){
                        repaint();
                    } else {
                        System.out.println("Moze li? Reka san NE MOZE!");
                    }
                }

                /*
                 * PONASANJE ZA BRISANJE
                 */
                else if (e.getKeyCode() == KeyEvent.VK_DELETE){
                    if (this.shift == false && teModel.getSelectionRange() != null){
                        teModel.deleteRange(teModel.getSelectionRange());
                    } else {
                        teModel.deleteAfter();
                    }


                }
                else if (e.getKeyCode() == KeyEvent.VK_BACK_SPACE){
                    if (this.shift == false && teModel.getSelectionRange() != null){
                        teModel.deleteRange(teModel.getSelectionRange());
                    } else {
                        teModel.deleteBefore();
                    }


                }

                /*
                 * PONASANJE ZA PISANJE (SLOVA)
                 */
                else if ((e.getKeyCode() >= 65 && e.getKeyCode()<= 90)
                        || (e.getKeyCode() >= 96 && e.getKeyCode() <= 105)
                        || e.getKeyCode() == KeyEvent.VK_SPACE) {
                    teModel.insert(e.getKeyChar());
                }
                // pritiskom na "1" isprobavamo umetanje stringa
                else if (e.getKeyCode() == KeyEvent.VK_1){
                    teModel.insert("--<3--");

                }
                /*
                 * PONASANJE ZA ENTER
                 */
                else if (e.getKeyCode() == KeyEvent.VK_ENTER){
                    teModel.enterPressed();
                }

                if (this.shift == false && teModel.getSelectionRange() != null
                        && e.isControlDown() == false){
                    teModel.setSelectionRange(null);
                    this.startLocation = new Location(0,0);
                    //repaint();
                }

                if (this.shift == true){
                    Location start = this.startLocation;
                    Location end = teModel.getCursorLocation();

                    if (start.isGreaterThan(end)){
                        //System.out.format("zasto sam ja tu??");
                        Location temp = end;
                        end = start;
                        start = temp;
                    }
                    /*
                    System.out.format("AFTER\n***********Start: %s, %d \n***********End: %d, %d\n",
                            this.startLocation.getRow(), this.startLocation.getCol(),
                            teModel.getCursorLocation().getRow(), teModel.getCursorLocation().getCol());

                    System.out.format("LOLOLOLO\nStart: %s, %d \n  +++End: %d, %d\n",
                            start.getRow(), start.getCol(),
                            end.getRow(), end.getCol());
                    */
                    teModel.setSelectionRange(new LocationRange(start, end));
                }

            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_SHIFT){
                    System.out.println("Shift released");
                    this.shift = false;
                }
            }
        });


    }

    public TextEditor(String text){
        this();
        System.out.println("Novi SUPERKUL konstruktor");
        this.teModel = new TextEditorModel(text);
        this.teModel.attachCursorObserver(this);
        this.teModel.attachTextObserver(this);
    }

    public void printLines(List<String> lines){
        for(String line: lines){
            System.out.println(line);
        }
    }

    @Override
    public void paint(Graphics g){
        super.paint(g);
        Iterator<String> iterator = this.teModel.allLines();

        Font myFont = new Font(null, Font.BOLD, 20);
        g.setFont(myFont);
        //FontMetrics fontMetrics = new FontMetrics(myFont);
        int currentRow = 0;
        int startingX = 20;
        int startingY = 100;
        int height = startingY;
        Location start = new Location(0,0);
        Location end = new Location(0,0);

        boolean highlightPresent = false;
        if (this.teModel.getSelectionRange() != null){
            highlightPresent = true;
            start = this.teModel.getSelectionRange().getStart();
            end = this.teModel.getSelectionRange().getEnd();


        }

        while(iterator.hasNext()){
            String line = iterator.next();
            //System.out.println(line);
            if (this.cursorLocation.getRow() == currentRow){
                int col = this.cursorLocation.getCol();
                line = line.substring(0, col) +
                        "|" + line.substring(col, line.length());
            }

            if (highlightPresent == true){

                //AKO JE SAMO JEDAN RED HIGHLIGHTAN
                if (start.getRow() == end.getRow()){
                    if (currentRow == start.getRow()){
                        highlighter(line, g, start, end, startingX, height);

                    }

                }
                //AKO JE VISE REDOVA HIGHLIGHTANO
                else {
                    //System.out.println("Nismo jos tu");

                    if (currentRow == start.getRow()){
                        Location newEnd = new Location(currentRow, line.length());
                        highlighter(line, g, start, newEnd, startingX, height);

                    } else if (currentRow > start.getRow() && currentRow < end.getRow()){
                        Location newEnd = new Location(currentRow, line.length());
                        Location newBeginning = new Location(currentRow, 0);
                        highlighter(line, g, newBeginning, newEnd, startingX, height);

                    } else if (currentRow == end.getRow()){
                        Location newBeginning = new Location(currentRow, 0);
                        highlighter(line, g, newBeginning, end, startingX, height);
                    }
                }
            }


            /*
            //FARBANJE 2. REDA TEKSTA
            if (currentRow == 1){
                FontMetrics fm = g.getFontMetrics();
                Rectangle2D rect = fm.getStringBounds(line, g);

                g.setColor(Color.YELLOW);
                g.fillRect(startingX,
                        height - fm.getAscent(),
                        (int) rect.getWidth(),
                        (int) rect.getHeight());
                g.setColor(Color.BLACK);


            }
            */
            g.drawString(line, startingX, height);
            height += 25;
            currentRow += 1;
        }
    }

    private void highlighter(String line, Graphics g, Location start, Location end,
                             int startingX, int height){
        FontMetrics fm = g.getFontMetrics();
        String lineBeginning;
        String lineHighlighted;

        if (this.cursorLocation.getRow() == start.getRow()){
            if (this.cursorLocation.getCol() > start.getCol() ){
                //if (true){
                lineBeginning = line.substring(0, start.getCol());
                //System.out.format("start col %d; end col %d\n", start.getCol(), end.getCol());
                lineHighlighted = line.substring(start.getCol(), end.getCol());

            } else {
                //System.out.format("start %d, length %d\n", start.getCol() + 1, line.length());
                //System.out.format("end %d, length %d\n", end.getCol() + 1, line.length());
                if(start.getCol() + 1 >= line.length()){
                    return;
                }
                lineBeginning = line.substring(0, start.getCol()+1);
                if (end.getCol()+1 > line.length()){
                    end.setCol(line.length()-1);
                }
                lineHighlighted = line.substring(start.getCol()+1, end.getCol()+1);
            }
        } else {
            lineBeginning = line.substring(0, start.getCol());
            lineHighlighted = line.substring(start.getCol(), end.getCol());
        }

        Rectangle2D rectBefore = fm.getStringBounds(lineBeginning, g);
        Rectangle2D rect = fm.getStringBounds(lineHighlighted, g);

        g.setColor(Color.YELLOW);
        g.fillRect(startingX + (int) rectBefore.getWidth(),
                height - fm.getAscent(),
                (int) rect.getWidth(),
                (int) rect.getHeight());
        g.setColor(Color.BLACK);
    }


    /*
    * AKCIJE I METODE ZA MENU BAR
    *
    *
     */

    //ACTIONS
    Action Open = new AbstractAction("Open File") {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION){
                openFile(fc.getSelectedFile().getAbsolutePath());
            }
        }
    };

    Action Save = new AbstractAction("Save File") {
        @Override
        public void actionPerformed(ActionEvent e) {
            saveFile();
        }
    };

    Action Exit = new AbstractAction("Exit") {
        @Override
        public void actionPerformed(ActionEvent e) {
            System.exit(0);
        }
    };

    Action Undo = new AbstractAction("Undo") {
        @Override
        public void actionPerformed(ActionEvent e) {
            //...
        }
    };

    Action Redo = new AbstractAction("Redo") {
        @Override
        public void actionPerformed(ActionEvent e) {
            //...
        }
    };

    Action Cut = new AbstractAction("Cut") {
        @Override
        public void actionPerformed(ActionEvent e) {
            //...
        }
    };

    Action Copy = new AbstractAction("Copy") {
        @Override
        public void actionPerformed(ActionEvent e) {
            //...
        }
    };

    Action Paste = new AbstractAction("Paste") {
        @Override
        public void actionPerformed(ActionEvent e) {
            //...
        }
    };

    Action PasteAndTake = new AbstractAction("Paste and Take") {
        @Override
        public void actionPerformed(ActionEvent e) {
            //...
        }
    };

    Action DeleteSelection = new AbstractAction("Delete Selection") {
        @Override
        public void actionPerformed(ActionEvent e) {
            //...
        }
    };

    Action ClearDocument = new AbstractAction("Clear Document") {
        @Override
        public void actionPerformed(ActionEvent e) {
            //...
        }
    };

    Action CursorStart = new AbstractAction("Cursor to Document Start") {
        @Override
        public void actionPerformed(ActionEvent e) {
            //...
        }
    };

    Action CursorEnd = new AbstractAction("Cursor to Document End") {
        @Override
        public void actionPerformed(ActionEvent e) {
            //...
        }
    };


    //METHODS
    public void openFile(String fileName){
        FileReader fr = null;
        try {
            fr = new FileReader(fileName);
            textArea.read(fr, null);
            fr.close();
            setTitle(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveFile(){
        if (fc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION){
            FileWriter fw = null;
            try {
                fw = new FileWriter(fc.getSelectedFile().getAbsoluteFile() + ".txt");
                textArea.write(fw);
                fw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }





    @Override
    public void updateCursorLocation(Location loc) {
        this.cursorLocation = loc;
        String cursorLabelText = String.format("Cursor: row - %d, column - %d",
                loc.getRow(), loc.getCol());
        this.cursorLabel.setText(cursorLabelText);
    }

    @Override
    public void updateText() {
        repaint();
    }

    public static void main(String[] args){
        new TextEditor("HEy!\nWhat's up?\nIs u OK?");

        /*
        System.out.println("HEy!\nWhat's up?");
        String text = "HEy!\nWhat's up?\nIs u OK?";
        String[] lines = text.split("\n");
        for (String line: lines){
            System.out.println(line);
        }
        */
    }
}
