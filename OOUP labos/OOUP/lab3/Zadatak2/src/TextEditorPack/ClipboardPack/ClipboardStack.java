package TextEditorPack.ClipboardPack;

import java.util.*;

public class ClipboardStack {
    private Stack<String> texts;

    public ClipboardStack(){
        this.texts = new Stack<>();
    }

    public void push(List<String> lines){
        String finalLine = "";
        String line;

        Iterator<String> linesIterator = lines.iterator();
        while (linesIterator.hasNext()){
            line = linesIterator.next();
            if (linesIterator.hasNext()){
                finalLine += line + "\n";
            } else {
                finalLine += line;
            }
        }

        this.texts.push(finalLine);
    }

    public List<String> pop(){
        String finalLine = this.texts.pop();
        return Arrays.asList(finalLine.split("\n"));
    }

    public boolean empty(){
        return this.texts.empty();
    }

    public List<String> peek(){
        String finalLine = this.texts.pop();
        this.texts.push(finalLine);
        return Arrays.asList(finalLine.split("\n"));
    }

}
