package TextEditorPack.CursorPack;

import LocationPack.Location;

public interface CursorObserver {
    void updateCursorLocation(Location loc);

}
