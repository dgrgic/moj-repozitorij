package TextEditorPack.CursorPack;

public interface CursorSubject {
    boolean attachCursorObserver(CursorObserver observer);
    boolean dettachCursorObserver(CursorObserver observer);
    void dettachAllCursorObservers();
    void updateCursorObservers();

    boolean moveCursorLeft();
    boolean moveCursorRight();
    boolean moveCursorUp();
    boolean moveCursorDown();
}
