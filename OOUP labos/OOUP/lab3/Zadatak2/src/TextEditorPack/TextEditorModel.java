package TextEditorPack;

import LocationPack.Location;
import LocationPack.LocationRange;
import TextEditorPack.CursorPack.CursorObserver;
import TextEditorPack.CursorPack.CursorSubject;
import TextEditorPack.TextObserverPack.TextObserver;
import TextEditorPack.TextObserverPack.TextSubject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class TextEditorModel implements CursorSubject, TextSubject {
    /*
     *   TextEditorModel - enkapsulira podatke o tekstu
     *                   - polozaju kursora
     *                   - selekciji teksta
     *
     *   Podatkovni clanovi:
     *      - lines - lista redaka
     *      - selectionRange - koordinate pocetka i kraja oznacenog
     *      - cursorLocation - koordinate kursora
     *
     * */

    private List<String> lines;
    private LocationRange selectionRange = null;
    private Location cursorLocation = new Location(0,0);

    private List<CursorObserver> cursorObservers = new ArrayList<>();
    private List<TextObserver> textObservers = new ArrayList<>();


    public Location getCursorLocation() {
        return cursorLocation;
    }

    public TextEditorModel(String text) {
        //razlomiti text u retke
        //pohraniti u lines
        this.lines = new ArrayList<>(Arrays.asList(text.split("\n")));

        for (String line: this.lines){
            System.out.println(line);
        }

    }


    public Iterator allLines(){
        //...
        Iterator<String> iterator = this.lines.iterator();
        return iterator;
    }

    public Iterator linesRange(int index1, int index2){
        //...
        Iterator<String> iterator = this.lines.subList(index1, index2).iterator();
        return iterator;
    }


    //vraca
    public List<String> getSelectedRows(){
        if (selectionRange == null){
            return null;
        }
        Location start = this.selectionRange.getStart();
        Location end = this.selectionRange.getEnd();

        List<String> selectedLines = new ArrayList<>();

        if(start.getRow() == end.getRow()){
            String currentLine = this.lines.get(start.getRow());
            String line = currentLine.substring(start.getCol(), end.getCol());
            selectedLines.add(line);

        } else {
            //first line

            String currentLine = this.lines.get(start.getRow());
            String line = currentLine.substring(start.getCol(), currentLine.length());
            selectedLines.add(line);


            //middle lines
            for (String line2: this.lines.subList(start.getRow() + 1, end.getRow())){
                selectedLines.add(line2);
            }

            //last line
            /*
            currentLine = this.lines.get(end.getRow());
            line = currentLine.substring(0, end.getCol());
            selectedLines.add(line);
            */
            selectedLines.add(this.lines.get(end.getRow()).substring(0, end.getCol()));
        }

        return selectedLines;
    }


    /*
    * CURSOR OBSERVERI
    *
     */
    @Override
    public boolean attachCursorObserver(CursorObserver observer){
        return cursorObservers.add(observer);
    }

    @Override
    public boolean dettachCursorObserver(CursorObserver observer){
        return cursorObservers.remove(observer);
    }

    @Override
    public void dettachAllCursorObservers(){
        cursorObservers.clear();
    }

    @Override
    public void updateCursorObservers(){
        //System.out.println("Obavjestavam sve");
        for (CursorObserver observer: this.cursorObservers){
            observer.updateCursorLocation(this.cursorLocation);
        }
    }

    @Override
    public boolean moveCursorLeft(){
        int currentCol = this.cursorLocation.getCol();
        int currentRow = this.cursorLocation.getRow();
        if (currentCol == 0){
            if (currentRow == 0){
                return false;
            }
            String previousLine = this.lines.get(currentRow - 1);
            int lastCharIndex = previousLine.length();
            this.cursorLocation.setRow(currentRow - 1);
            this.cursorLocation.setCol(lastCharIndex);
            return true;
        }

        this.cursorLocation.setCol(currentCol - 1);
        updateCursorObservers();
        return true;
    }

    @Override
    public boolean moveCursorRight(){
        int currentRow = this.cursorLocation.getRow();
        int currentCol = this.cursorLocation.getCol();
        String currentLine = this.lines.get(currentRow);
        int maxCol = currentLine.length();
        if (currentCol == maxCol){
            int maxRow = this.lines.size();
            if (currentRow == maxRow - 1){
                return false;
            }
            this.cursorLocation.setRow(currentRow + 1);
            this.cursorLocation.setCol(0);
            return true;
        }

        this.cursorLocation.setCol(currentCol + 1);
        updateCursorObservers();
        return true;
    }

    @Override
    public boolean moveCursorUp(){
        int currentRow = this.cursorLocation.getRow();
        int currentCol = this.cursorLocation.getCol();

        if (currentRow == 0){
            return false;
        }

        this.cursorLocation.setRow(currentRow - 1);
        String currentLine = this.lines.get(currentRow - 1);
        int maxCol = currentLine.length();
        if (currentCol > maxCol){
            this.cursorLocation.setCol(maxCol);
        }
        updateCursorObservers();
        return true;

    }

    @Override
    public boolean moveCursorDown(){
        int currentRow = this.cursorLocation.getRow();
        int currentCol = this.cursorLocation.getCol();
        int maxRow = this.lines.size();
        if (currentRow == maxRow - 1){
            return false;
        }

        this.cursorLocation.setRow(currentRow + 1);
        String currentLine = this.lines.get(currentRow + 1);
        int maxCol = currentLine.length();
        if (currentCol > maxCol){
            this.cursorLocation.setCol(maxCol);
        }
        updateCursorObservers();
        return true;
    }

    /*
     * TEXT OBSERVERI
     *
     */
    @Override
    public boolean attachTextObserver(TextObserver observer){
        return textObservers.add(observer);
    }

    @Override
    public boolean dettachTextObserver(TextObserver observer){
        return textObservers.remove(observer);
    }

    @Override
    public void dettachAllTextObservers(){
        textObservers.clear();
    }

    @Override
    public void updateTextObservers(){
        for(TextObserver observer: this.textObservers){
            observer.updateText();
        }
    }

    @Override
    public boolean deleteBefore() {
        int currentRow = this.cursorLocation.getRow();
        int currentCol = this.cursorLocation.getCol();
        String currentLine = this.lines.get(currentRow);
        int maxCol = currentLine.length();
        if (currentCol == 0){
            if (currentRow == 0){
                return false;
            }
            String previousLine = this.lines.get(currentRow-1);
            this.lines.remove(currentRow);
            this.lines.remove(currentRow-1);
            this.lines.add(currentRow-1, previousLine + currentLine);
            this.cursorLocation.setCol(previousLine.length());
            this.cursorLocation.setRow(currentRow-1);
        } else {
            currentLine = currentLine.substring(0, currentCol - 1)
                    + currentLine.substring(currentCol, currentLine.length());

            this.cursorLocation.setCol(currentCol - 1);
            this.lines.remove(currentRow);
            this.lines.add(currentRow, currentLine);
        }

        updateCursorObservers();
        updateTextObservers();
        return true;
    }

    @Override
    public boolean deleteAfter() {
        int currentRow = this.cursorLocation.getRow();
        int currentCol = this.cursorLocation.getCol();
        String currentLine = this.lines.get(currentRow);
        int maxCol = currentLine.length();
        if (currentCol == maxCol){
            int maxRow = this.lines.size();
            if (currentRow == maxRow - 1){
                return false;
            }
            String followingLine = this.lines.get(currentRow+1);
            this.lines.remove(currentRow+1);
            this.lines.remove(currentRow);
            this.lines.add(currentRow, currentLine + followingLine);

        } else {
            currentLine = currentLine.substring(0, currentCol)
                    + currentLine.substring(currentCol + 1,
                    currentLine.length());
            //System.out.println(currentLine);
            this.lines.remove(currentRow);
            this.lines.add(currentRow,currentLine);
            //System.out.println(this.lines[currentRow]);
        }

        updateTextObservers();
        return true;
    }

    /*
    @Override
    public void deleteRange(LocationRange lr) {
        Location start = new Location(lr.getStart().getRow(), lr.getStart().getCol());
        Location end = new Location(lr.getEnd().getRow(), lr.getEnd().getCol());



        int currentRow = 0;
        while (currentRow < this.lines.size()){
            String line = this.lines.get(currentRow);
            if (currentRow == start.getRow()){

                //sve je u jednom redu
                if (currentRow == end.getRow()){
                    line = line.substring(0, start.getCol()) + line.substring(end.getCol(), line.length());
                    this.lines.remove(currentRow);
                    this.lines.add(currentRow, line);
                    break;
                }

                line = line.substring(0, start.getCol());
                this.lines.remove(currentRow);
                this.lines.add(currentRow, line);
                currentRow += 1;
                continue;
            } else if (currentRow == end.getRow()){
                line = line.substring(0, start.getCol());
                this.lines.remove(currentRow);
                this.lines.add(currentRow, line);
                break;
            } else if (currentRow > start.getRow()){
                this.lines.remove(currentRow);
                end.setRow(end.getRow() - 1);
                continue;
            }

        }

        this.cursorLocation = new Location(start.getRow(), start.getCol());
        updateCursorObservers();
        updateTextObservers();
        return;
    }
    */

    //vraca
    @Override
    public void deleteRange(LocationRange lr){
        if (lr == null){
            return;
        }
        Location start = lr.getStart();
        Location end = lr.getEnd();

        List<String> selectedLines = new ArrayList<>();

        if(start.getRow() == end.getRow()){
            String currentLine = this.lines.get(start.getRow());
            String line = currentLine.substring(0, start.getCol()) +
                    currentLine.substring(end.getCol(), currentLine.length());
            this.lines.remove(start.getRow());
            this.lines.add(start.getRow(), line);

        } else {
            //first line
            List<String> linesToAdd = new ArrayList<>();
            String currentLine = this.lines.get(start.getRow());
            String line = currentLine.substring(0, start.getCol());
            //this.lines.remove(start.getRow());
            linesToAdd.add(line);

            /*
            //middle lines
            for (String line2: this.lines.subList(start.getRow() + 1, end.getRow())){
                selectedLines.add(line2);
            }
            */

            //last line
            /*
            currentLine = this.lines.get(end.getRow());
            line = currentLine.substring(0, end.getCol());
            selectedLines.add(line);
            */
            currentLine = this.lines.get(end.getRow());
            linesToAdd.add(currentLine.substring(end.getCol(), currentLine.length()));

            this.lines.subList(start.getRow(), end.getRow() + 1).clear();
            for (int i = 0; i < linesToAdd.size(); i++){
                this.lines.add(start.getRow() + i, linesToAdd.get(i));
            }

        }

        this.cursorLocation = new Location(start.getRow(), start.getCol());
        updateCursorObservers();
        updateTextObservers();
        return;
    }

    @Override
    public LocationRange getSelectionRange() {
        return this.selectionRange;
    }

    @Override
    public void setSelectionRange(LocationRange lr) {
        this.selectionRange = lr;
    }

    public boolean insert(char c){
        int currentRow = this.cursorLocation.getRow();
        int currentCol = this.cursorLocation.getCol();
        String currentLine = this.lines.get(currentRow);
        String character = Character.toString(c);
        currentLine = currentLine.substring(0, currentCol)
                + character.substring(0, 1)
                + currentLine.substring(currentCol,
                currentLine.length());

        this.cursorLocation.setCol(currentCol + 1);
        //System.out.println(currentLine);
        this.lines.remove(currentRow);
        this.lines.add(currentRow,currentLine);
        //System.out.println(this.lines[currentRow]);
        updateCursorObservers();
        updateTextObservers();
        return true;
    }

    public boolean insert(String s){
        int currentRow = this.cursorLocation.getRow();
        int currentCol = this.cursorLocation.getCol();
        String currentLine = this.lines.get(currentRow);
        currentLine = currentLine.substring(0, currentCol)
                + s.substring(0, s.length())
                + currentLine.substring(currentCol,
                currentLine.length());

        this.cursorLocation.setCol(currentCol + s.length());
        //System.out.println(currentLine);
        this.lines.remove(currentRow);
        this.lines.add(currentRow,currentLine);
        //System.out.println(this.lines[currentRow]);
        updateCursorObservers();
        updateTextObservers();
        return true;
    }

    public boolean insert(List<String> lista){
        int currentRow = this.cursorLocation.getRow();
        int currentCol = this.cursorLocation.getCol();
        String line;
        String currentLine;
        String laterLine = "";
        int i = 0;
        Iterator<String> lineIterator = lista.iterator();
        if (lineIterator.hasNext()){
            line = lineIterator.next();
            //one liner
            if (lineIterator.hasNext() == false){
                currentLine = this.lines.get(currentRow);
                currentLine = currentLine.substring(0, currentCol)
                        + line
                        + currentLine.substring(currentCol, currentLine.length());

                this.lines.remove(currentRow);
                this.lines.add(currentRow, currentLine);

                this.cursorLocation = new Location(currentRow, currentCol + line.length());

            }

            //first line
            else {
                currentLine = this.lines.get(currentRow);
                laterLine = currentLine.substring(currentCol, currentLine.length());
                currentLine = currentLine.substring(0, currentCol) + line;

                this.lines.remove(currentRow);
                this.lines.add(currentRow, currentLine);
                i++;
            }
        }


        while(lineIterator.hasNext()){
            line = lineIterator.next();
            if (lineIterator.hasNext()){
                this.lines.add(currentRow + i, line);
                i++;
            } else {
                this.cursorLocation = new Location(currentRow + i, line.length());
                line = line + laterLine;
                this.lines.add(currentRow + i, line);
            }
        }


        updateCursorObservers();
        updateTextObservers();
        return true;
    }

    public boolean enterPressed(){
        int currentRow = this.cursorLocation.getRow();
        int currentCol = this.cursorLocation.getCol();
        String currentLine = this.lines.get(currentRow);
        String subLine1 = currentLine.substring(0, currentCol);
        String subLine2 = currentLine.substring(currentCol, currentLine.length());

        this.lines.remove(currentRow);
        this.lines.add(currentRow, subLine1);
        this.lines.add(currentRow + 1, subLine2);


        this.cursorLocation.setCol(0);
        this.cursorLocation.setRow(currentRow + 1);
        //System.out.println(currentLine);
        //System.out.println(this.lines[currentRow]);
        updateCursorObservers();
        updateTextObservers();
        return true;
    }


}
