package TextEditorPack.MenuPack;

import TextEditorPack.TextEditor;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class MyMenu extends JMenu{

    private TextEditor editor;

    public MyMenu(TextEditor editor) {
        this.editor = editor;

        JMenuBar menuBar = new JMenuBar();
        //setJMenuBar(menuBar);
        JMenu file = new JMenu("File");
        menuBar.add(file);
        JMenu edit = new JMenu("Edit");
        menuBar.add(edit);
        JMenu move = new JMenu("Move");
        menuBar.add(move);

        //ADDING ACTIONS TO FILE MENU ITEMS
        file.add(Open);
        file.add(Save);
        file.addSeparator();
        file.add(Exit);

        //ADDING ACTIONS TO EDIT MENU ITEMS
        edit.add(Undo);
        edit.add(Redo);
        edit.addSeparator();
        edit.add(Cut);
        edit.add(Copy);
        edit.add(Paste);
        edit.add(PasteAndTake);
        edit.addSeparator();
        edit.add(DeleteSelection);
        edit.add(ClearDocument);

        //ADDING ACTIONS TO MOVE MENU ITEMS
        move.add(CursorStart);
        move.add(CursorEnd);
    }

    //ACTIONS
    Action Open = new AbstractAction("Open File") {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (editor.getFc().showOpenDialog(null) == JFileChooser.APPROVE_OPTION){
                openFile(editor.getFc().getSelectedFile().getAbsolutePath());
            }
        }
    };

    Action Save = new AbstractAction("Save File") {
        @Override
        public void actionPerformed(ActionEvent e) {
            saveFile();
        }
    };

    Action Exit = new AbstractAction("Exit") {
        @Override
        public void actionPerformed(ActionEvent e) {
            System.exit(0);
        }
    };

    Action Undo = new AbstractAction("Undo") {
        @Override
        public void actionPerformed(ActionEvent e) {
            //...
        }
    };

    Action Redo = new AbstractAction("Redo") {
        @Override
        public void actionPerformed(ActionEvent e) {
            //...
        }
    };

    Action Cut = new AbstractAction("Cut") {
        @Override
        public void actionPerformed(ActionEvent e) {
            //...
        }
    };

    Action Copy = new AbstractAction("Copy") {
        @Override
        public void actionPerformed(ActionEvent e) {
            //...
        }
    };

    Action Paste = new AbstractAction("Paste") {
        @Override
        public void actionPerformed(ActionEvent e) {
            //...
        }
    };

    Action PasteAndTake = new AbstractAction("Paste and Take") {
        @Override
        public void actionPerformed(ActionEvent e) {
            //...
        }
    };

    Action DeleteSelection = new AbstractAction("Delete Selection") {
        @Override
        public void actionPerformed(ActionEvent e) {
            //...
        }
    };

    Action ClearDocument = new AbstractAction("Clear Document") {
        @Override
        public void actionPerformed(ActionEvent e) {
            //...
        }
    };

    Action CursorStart = new AbstractAction("Cursor to Document Start") {
        @Override
        public void actionPerformed(ActionEvent e) {
            //...
        }
    };

    Action CursorEnd = new AbstractAction("Cursor to Document End") {
        @Override
        public void actionPerformed(ActionEvent e) {
            //...
        }
    };


    //METHODS
    public void openFile(String fileName){
        FileReader fr = null;
        try {
            fr = new FileReader(fileName);
            this.editor.getTextArea().read(fr, null);
            fr.close();
            this.editor.setTitle(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveFile(){
        if (this.editor.getFc().showSaveDialog(null) == JFileChooser.APPROVE_OPTION){
            FileWriter fw = null;
            try {
                fw = new FileWriter(this.editor.getFc().getSelectedFile().getAbsoluteFile() + ".txt");
                this.editor.getTextArea().write(fw);
                fw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
