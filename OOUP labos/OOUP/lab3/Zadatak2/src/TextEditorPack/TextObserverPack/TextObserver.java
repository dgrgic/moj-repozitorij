package TextEditorPack.TextObserverPack;

public interface TextObserver {
    void updateText();
}
