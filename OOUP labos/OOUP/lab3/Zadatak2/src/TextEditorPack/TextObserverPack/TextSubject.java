package TextEditorPack.TextObserverPack;

import LocationPack.LocationRange;

public interface TextSubject {
    boolean attachTextObserver(TextObserver observer);
    boolean dettachTextObserver(TextObserver observer);
    void dettachAllTextObservers();
    void updateTextObservers();

    boolean deleteBefore();
    boolean deleteAfter();
    void deleteRange(LocationRange lr);
    LocationRange getSelectionRange();
    void setSelectionRange(LocationRange lr);
}
