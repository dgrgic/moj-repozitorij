package TextEditorPack.UndoPack;

import java.util.Stack;

public interface EditAction {
    void executeDo();
    void executeUndo();
}
