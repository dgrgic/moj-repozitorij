#include <iostream>
#include <assert.h>
#include <stdlib.h>


  struct Point{
    int x; int y;
  };
  struct Shape{
    enum EType {circle, square, rhomb};
    EType type_;
  };
  struct Circle{
     Shape::EType type_;
     double radius_;
     Point center_;
  };
  struct Square{
     Shape::EType type_;
     double side_;
     Point center_;
  };

  ///*
  struct Rhomb{
     Shape::EType type_;
     double side_;
     double angle_;
     Point center_;
  };
  //*/

  void drawSquare(struct Square*){
    std::cerr <<"in drawSquare\n";
  }
  void drawCircle(struct Circle*){
    std::cerr <<"in drawCircle\n";
  }

  ///*
  void drawRhomb(struct Rhomb*){
    std::cerr <<"in drawRhomb\n";
  }
  //*/
  
  void moveShapes (Shape** shapes, int n, int shift){
    std::cerr <<"##### moveShapes ####\n";
    std::cerr <<" --- shift = ";
    printf(" %d\n", shift);
    for (int i=0; i<n; ++i){
      struct Shape* s = shapes[i];
      switch (s->type_){
      case Shape::square:
      {
        struct Square* sq = (struct Square*)s;
        std::cerr <<"SQUARE: \n";
        std::cerr <<"\t - Before: x,y = ";
        printf(" %d %d\n", sq->center_.x, sq->center_.y);
        //std::cerr << sq->center_ , shift;
        sq->center_.x += shift;
        std::cerr <<"\t - After:  x,y = ";
        printf(" %d %d\n", sq->center_.x, sq->center_.y);
        //std::cerr << sq->center_;
        break;
      }
      case Shape::circle:
      {
        struct Circle* c = (struct Circle*)s;
        std::cerr <<"CIRCLE: \n";
        std::cerr <<"\t - Before: x,y = ";
        printf(" %d %d\n", c->center_.x, c->center_.y);
        //std::cerr << sq->center_ , shift;
        c->center_.y += shift;
        std::cerr <<"\t - After:  x,y = ";
        printf(" %d %d\n", c->center_.x, c->center_.y);
        //std::cerr << sq->center_;
        break;
      }



      // OVAJ DIO TREBA ZAKOMENTIRATI AKO ZELIMO DA PROGRAM PUCA
      ///*
      case Shape::rhomb:
      {
        struct Rhomb* c = (struct Rhomb*)s;
        std::cerr <<"RHOMB: \n";
        std::cerr <<"\t - Before: x,y = ";
        printf(" %d %d\n", c->center_.x, c->center_.y);
        //std::cerr << sq->center_ , shift;
        c->center_.x += shift;
        c->center_.y += shift;
        std::cerr <<"\t - After:  x,y = ";
        printf(" %d %d\n", c->center_.x, c->center_.y);
        //std::cerr << sq->center_;
        break;
      }
      //*/
      default:
        assert(0); 
        exit(0);
      }
    }
  }


  void drawShapes(Shape** shapes, int n){
    std::cerr <<"##### drawShapes ####\n";
    for (int i=0; i<n; ++i){
      struct Shape* s = shapes[i];
      switch (s->type_){
      case Shape::square:
        drawSquare((struct Square*)s);
        break;
      case Shape::circle:
        drawCircle((struct Circle*)s);
        break;
      ///*
      case Shape::rhomb:
        drawRhomb((struct Rhomb*)s);
        break;
      //*/
      default:
        assert(0); 
        exit(0);
      }
    }
  }
  int main(){
    Shape* shapes[5];
    struct Circle* shape;
    struct Square* shap;


    shapes[0]=(Shape*)new Circle();
    shapes[0]->type_=Shape::circle;
    shape = (struct Circle*)shapes[0];
    printf("1: x: %d, y: %d\n", shape->center_.x, shape->center_.y);
    //printf("1: type: %s, x: %d, y: %d\n", shape->type_, shape->center_.x, shape->center_.y);

    shapes[1]=(Shape*)new Square();
    shapes[1]->type_=Shape::square;
    shap = (struct Square*)shapes[1];
    printf("2: x: %d, y: %d\n", shap->center_.x, shap->center_.y);

    shapes[2]=(Shape*)new Square();
    shapes[2]->type_=Shape::square;
    shap = (struct Square*)shapes[2];
    printf("3: x: %d, y: %d\n", shap->center_.x, shap->center_.y);
    //printf("type: %s, x: %d, y: %d\n", shap->type_, shap->center_.x, shap->center_.y);

    shapes[3]=(Shape*)new Circle();
    shapes[3]->type_=Shape::circle;
    shape = (struct Circle*)shapes[3];
    printf("4: x: %d, y: %d\n", shape->center_.x, shape->center_.y);
    //printf("type: %s, x: %d, y: %d\n", shape->type_, shape->center_.x, shape->center_.y);

    shapes[4]=(Shape*)new Rhomb();
    shapes[4]->type_=Shape::rhomb;

    drawShapes(shapes, 5);
    moveShapes(shapes, 5, 17);


    /*
    shapes[4]=(Shape*)new Circle;
    shapes[4]->type_=Shape::circle;
    shape = (struct Circle*)shapes[4];
    printf("4: x: %d, y: %d\n", shape->center_.x, shape->center_.y);
    //printf("1: type: %s, x: %d, y: %d\n", shape->type_, shape->center_.x, shape->center_.y);

    shapes[5]=(Shape*)new Square;
    shapes[5]->type_=Shape::square;
    shap = (struct Square*)shapes[5];
    printf("5: x: %d, y: %d\n", shap->center_.x, shap->center_.y);

    shapes[6]=(Shape*)new Square;
    shapes[6]->type_=Shape::square;
    shap = (struct Square*)shapes[6];
    printf("6: x: %d, y: %d\n", shap->center_.x, shap->center_.y);
    //printf("type: %s, x: %d, y: %d\n", shap->type_, shap->center_.x, shap->center_.y);

    shapes[7]=(Shape*)new Circle;
    shapes[7]->type_=Shape::circle;
    shape = (struct Circle*)shapes[7];
    printf("7: x: %d, y: %d\n", shape->center_.x, shape->center_.y);
    //printf("type: %s, x: %d, y: %d\n", shape->type_, shape->center_.x, shape->center_.y);
    //shapes[4]=(Shape*)new Rhomb;
    //shapes[4]->type_=Shape::rhomb;
    */
  }