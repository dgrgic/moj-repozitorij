import java.util.List;

public interface Subjekt {

    void dodajPromatraca(Promatrac promatrac);
    void ukloniPromatraca(Promatrac promatrac);
    void obavijesti();
    List<Integer> dohvatiStanje();



}
