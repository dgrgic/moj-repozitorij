import java.util.Collections;
import java.util.List;

public class TraziteljMediana implements Promatrac {
    private Subjekt subjekt;

    public TraziteljMediana(Subjekt subjekt) {
        this.subjekt = subjekt;
        this.subjekt.dodajPromatraca(this);
    }

    @Override
    public void azuriraj() {
        List<Integer> brojevi = subjekt.dohvatiStanje();
        Collections.sort(brojevi);

        int m = brojevi.size() / 2;

        System.out.println("medijan: " + brojevi.get(m) + " m = " + m );
        System.out.println(brojevi);

    }
}
