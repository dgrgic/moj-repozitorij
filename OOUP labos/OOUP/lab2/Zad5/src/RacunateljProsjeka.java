import java.util.List;

public class RacunateljProsjeka implements Promatrac {

    private Subjekt subjekt;


    public RacunateljProsjeka(Subjekt subjekt) {
        this.subjekt = subjekt;
        this.subjekt.dodajPromatraca(this);
    }

    @Override
    public void azuriraj() {
        List<Integer> brojevi = subjekt.dohvatiStanje();
        int zbroj = 0;

        for(int i = 0; i < brojevi.size(); i++ ){
            zbroj  += brojevi.get(i);
        }
        System.out.println("prosjek: " + zbroj/brojevi.size());
    }
}
