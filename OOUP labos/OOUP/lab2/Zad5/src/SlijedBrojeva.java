import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class SlijedBrojeva implements Subjekt{

    private List<Integer> brojevi;
    private Izvor izvor;
    private List<Promatrac> promatraci;

    public SlijedBrojeva(Izvor izvor) {
        this.brojevi = new ArrayList<>();
        this.promatraci = new ArrayList<>();
        this.izvor = izvor;
    }

    public void kreni(){
        Integer broj = 0;

        System.out.println("-1 oznacava Kraj. \nMolim Vas zapocnite s unosom: ");

        while( broj != -1){
            broj = izvor.dajBroj();
            System.out.println(" ---- novi broj: " + broj);

            if (broj < -1){
                System.out.println("nesto ste grdno pogrijesili! broj je ispod -1");
                continue;
            }
            if (broj == -1){
                System.out.println("Kraj.");
                break;
            }
            brojevi.add(broj);

            obavijesti();

            try{
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException ex) {
                System.out.println("NONONO - sram te bilo");
            }



        }

        System.out.println("ovo je lista: " + brojevi);

    }


    @Override
    public void dodajPromatraca(Promatrac promatrac) {
        this.promatraci.add(promatrac);

    }

    @Override
    public void ukloniPromatraca(Promatrac promatrac) {
        this.promatraci.remove(promatrac);
    }

    @Override
    public void obavijesti() {
        for(int i = 0; i < this.promatraci.size(); i++ ){
            Promatrac promatrac = this.promatraci.get(i);
            promatrac.azuriraj();
        }
    }

    @Override
    public List<Integer> dohvatiStanje() {
        //saljemo kopiju brojeva
        return new ArrayList<>(this.brojevi);
    }
}
