import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class DatotecniZapisivac implements Promatrac {
    private Subjekt subjekt;
    Path path = Paths.get("K:\\Labs\\OOUP\\lab2\\zad5output.txt");

    public DatotecniZapisivac(Subjekt subjekt) {
        this.subjekt = subjekt;
        this.subjekt.dodajPromatraca(this);
    }

    @Override
    public void azuriraj() {
        List<Integer> brojevi = subjekt.dohvatiStanje();
        List<String> lines = new ArrayList<>();

        for(int i = 0; i < brojevi.size(); i++ ){
            String broj = brojevi.get(i).toString();
            String line = (i+1) + ". broj " + broj;
            lines.add(line);
        }

        try {
            Files.write(path, lines, Charset.forName("UTF-8"));
        } catch (IOException e) {
            System.out.println("Nista od zapisivanja u datoteku :P ");
        }
        System.out.println("Zapisano u datoteku.");
    }
}
