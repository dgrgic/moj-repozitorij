import java.util.List;

public class Zbrajatelj implements Promatrac {

    private Subjekt subjekt;

    public Zbrajatelj(Subjekt subjekt) {
        this.subjekt = subjekt;
        this.subjekt.dodajPromatraca(this);
    }

    @Override
    public void azuriraj() {
        List<Integer> brojevi = subjekt.dohvatiStanje();
        int zbroj = 0;

        for(int i = 0; i < brojevi.size(); i++ ){
            zbroj  += brojevi.get(i);
        }
        System.out.println("zbroj: " + zbroj);
    }
}
