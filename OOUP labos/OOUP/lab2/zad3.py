def mymax(iterable, key = lambda x: x):
  # incijaliziraj maksimalni element i maksimalni ključ
  maxX=maxKey=None

  # obiđi sve elemente
  for x in iterable:
    # ako je key(x) najveći -> ažuriraj max_x i max_key
    if maxKey is None or key(x) > maxKey:
        maxX = x
        maxKey = key(x)

  # vrati rezultat
  return maxX







"""
///////////////////////////////////////////////////////////////////////////
////            Glavni program
///////////////////////////////////////////////////////////////////////////
"""



maxInt = mymax([1, 3, 5, 7, 4, 6, 9, 2, 0])
maxChar = mymax("Suncana strana ulice")
maxString = mymax(["Gle", "malu", "vocku", "poslije", "kise",
                   "Puna", "je", "kapi", "pa", "ih", "njise"])
print("maxInt = ", maxInt)
print("maxChar = ", maxChar)
print("maxString = ", maxString)

##Pronaci najskuplji proizvod u rjecniku D
D={'burek':8, 'buhtla':5, "pizza":10}

maxDict = mymax(D, len)
print("maxDict = ", maxDict)

osobe = [("Martina", "Juric"), ("Dorotea", "Protrka"), ("Marko", "Gulan"),
         ("Katarina", "Cavar")]

maxOsoba = mymax(osobe, lambda x: x[1] + " " + x[0])
print("maxOsoba = ", maxOsoba)


