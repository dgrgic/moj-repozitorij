import ast, re

class Cell:

    def __init__(self, exp, sheet):  #, table2D):
        self.__exp = exp
        self.__value = None 
        self.__observers = set()
        self.__subjects = set()
        self.sheet = sheet

        #self.update()

    def __repr__(self):
        return str(self.__exp) + " -> " + str(self.__value)

    # postavljanje tablice kojoj pripadam
    def setUpSheet(self, sheet):
        self.sheet = sheet
        self.changeExp(self.exp)
        self.changeValue(self.value)

    # mijenanje mog expressiona - po potrebi mijenjanje subjekata na koje gledam
    def changeExp(self, newExp):
        if self.sheet is None:
            print("Jos ne pripadam niti jednoj tablici. Molim pozvati self.setUpSheet(sheet)")
            return
        self.__exp = newExp

        #odjavi se sa ovih koje trenutno promatras
        for s in self.__subjects:
            s.dettach(self)
        self.__subjects = set()

        #promatraj nove celije
        newCells = self.sheet.getrefs(self)
        for c in newCells:
            c.attach(self)
            self.__subjects.add(c)

        self.update()

    # dobavlja moj expression
    def getExp(self):
        return self.__exp

    #mijenjanje vrijednosti moje celije - obavjestavanje mojih promatraca
    def __changeValue(self, newValue):
        if self.sheet is None:
            print("Jos ne pripadam niti jednoj tablici. Molim pozvati self.setUpSheet(sheet)")
            return
##        if self.__subjects:
##            print("Moja vrijednost ovisi o drugima - ne mozemo ju samo tako mijenjati")
##            return
        self.__value = newValue
        #obavijesti sve promatrace o novonastaloj promjeni
        self.notify()

##    #dohvacanje moje vrijednosti 
##    def getValue(self):
##        return self.__value

    #dodavanje novog promatraca
    def attach(self, observer):
        if self.sheet is None:
            print("Jos ne pripadam niti jednoj tablici. Molim pozvati self.setUpSheet(sheet)")
            return
        if observer not in self.__observers:
            self.__observers.add(observer)

    #odjavljivanje nekog promatraca
    def dettach(self, observer):
        if self.sheet is None:
            print("Jos ne pripadam niti jednoj tablici. Molim pozvati self.setUpSheet(sheet)")
            return
        if observer in self.__observers:
            self.__observers.remove(observer)

    # izracunavanje vlastite vrijednosti ponovno (nakon neke promjene)
    def update(self):
        if self.sheet is None:
            print("Jos ne pripadam niti jednoj tablici. Molim pozvati self.setUpSheet(sheet)")
            return
        self.__changeValue( self.sheet.evaluate(self) )
        return

    # dojavljivanje promjene mojim promatracima
    def notify(self):
        if self.sheet is None:
            print("Jos ne pripadam niti jednoj tablici. Molim pozvati self.setUpSheet(sheet)")
            return
        for observer in self.__observers:
            observer.update()
        return

    

    


class Sheet:

    def __init__(self, noRows, noCols, table2D = None):
##        self.2Dpolje = 2Dpolje
        if table2D is None:
            self.table2D = [[ Cell("1", self) for i in range(noCols)] for j in range(noRows)]
        else:
            self.table2D = table2D
        self.nameDict = self.__dictInit()

        self.updateAllCells()

        

    def __repr__(self):
        myself = ""
        for row in self.table2D:
            newRow = str(row) + "\n"
            #print(newRow)
            myself += newRow
        return myself

##    def assignSheetToCells(self):
##        for row in self.table2D

    def updateAllCells(self):
        for row in self.table2D:
            for element in row:
                element.update()
            
    # inicijalizacija rjecnika sa imenima stupaca
    def __dictInit(self):
        noRows = len(self.table2D)
        noCols = len(self.table2D[0])

        letters = ["A","B","C","D","E","F","G","H","I","J","K","L","M",
                   "N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]

        if noCols > len(letters):
            print("DRAMA!!!!!!!!!!!!!!!! Nema dovoljno slova.")
            exit(0)

        d = dict() #dict: name -> (row, column)

        for i in range(noRows):
            for j in range(noCols):
                name = letters[j]+str(i+1)
                name.strip()
                #print(name)
                d[name] = (i,j)
        return d

    # vraca celiju sa mjesta oznacenog sa ref (npr A1)
    def cell(self, ref):
        if ref not in self.nameDict:
            print("DRAMA!!!!!!!!!!!!!!!!!!!!!! Ne postoji polje s takvim imenom")
            return None

        x,y = self.nameDict[ref]

        return self.table2D[x][y]

    # celiji na referenci ref postavlja exp na content
    def set(self, ref, content):
        cellTemp = self.cell(ref)
        cellTemp.changeExp(content)
        x,y = self.nameDict[ref]
        self.table2D[x][y] = cellTemp
        return

    #vraca popis celija na koje se referencira celija cell
    def getrefs(self, cell):
        cells = []

        if "+" in cell.getExp():
            data = cell.getExp().split("+")

            for d in data:
                ref = d.strip()
                cells.append(self.cell(ref))

        return cells

    #evaluacija vrijednosti celije cell
    def evaluate(self, cell):
        value = 0

        cells = self.getrefs(cell)
        
        if cells == []:
            value += int(cell.getExp().strip())
            return value

        else:
            for c in cells:
                value += self.evaluate(c)
            return value

        
  

tablica = Sheet(3,4)
print(tablica)

tablica.set("A2", "B1 + B2")
print("tablica.set(\"A2\", \"B1 + B2\")")
print(tablica)
tablica.set("A3", "A2 + C1")
print("tablica.set(\"A2\", \"B1 + B2\")")
print(tablica)
tablica.set("C1", "5")
print("tablica.set(\"A2\", \"B1 + B2\")")
print(tablica)

    

    
































