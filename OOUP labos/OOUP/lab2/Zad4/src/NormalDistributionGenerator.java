import java.util.OptionalInt;
import java.util.Random;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

//import static java.lang.Math.*;

public class NormalDistributionGenerator implements NumberGenerator {

    private double mean;
    private double variance;
    private int numberOfElements;
    private int count;

    Random r;

    public NormalDistributionGenerator(double mean, double variance,
                                       int numberOfElements) {
        this.mean = mean;
        this.variance = variance;
        this.numberOfElements = numberOfElements;
        this.count = 0;
        this.r = new Random();
    }

    @Override
    public OptionalInt getNumber() {
        if (this.count >= this.numberOfElements){
            return OptionalInt.empty();
        } else {
            OptionalInt numberFinal;
            double numberD = r.nextGaussian();
            numberD = numberD * this.variance + this.mean;
            numberFinal = OptionalInt.of((int) Math.round(numberD));

            this.count += 1;

            return numberFinal;
        }
    }

    @Override
    public boolean hasNext() {
        if (this.count >= this.numberOfElements){
            return FALSE;
        } else {
            return TRUE;
        }
    }
}
