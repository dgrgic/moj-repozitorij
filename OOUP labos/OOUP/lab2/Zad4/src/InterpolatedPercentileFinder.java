import java.util.List;

public class InterpolatedPercentileFinder implements PercentileFinder{
    @Override
    public Integer getPercentile(int percentileNo, List<Integer> sortedList) {
        int sizeOfList = sortedList.size();
        int indV1 = 1, indV2 = 1;
        Integer v1, v2;
        double pv1 = 0, pv2 = 0;


        for (int i = 1; i < sizeOfList; i++){
            pv1 = getPercentileLocation(i, sizeOfList);
            pv2 = getPercentileLocation(i+1, sizeOfList);

            if (pv1 <= percentileNo && pv2 >= percentileNo){
                //System.out.println("tu sam " + pv1 + " " + percentileNo + " " + pv2);
                indV1 = i;
                indV2 = i+1;
                break;
            }
        }

        //System.out.println(sortedList);
        v1 = sortedList.get(indV1-1);
        v2 = sortedList.get(indV2-1);

        return interpolatedValue(v1, v2, pv1, pv2, sizeOfList, percentileNo);
    }

    private double getPercentileLocation(int index, int sizeOfList){
        return  100*(index - 0.5)/sizeOfList ;
    }

    private Integer interpolatedValue(int v1, int v2, double pv1, double pv2, int sizeOfList, int p){

        int retVal;

        retVal = (int) Math.round( v1 + sizeOfList *
                                ( p - pv1 ) *
                                (v2 - v1) / 100.0
                                );

        //System.out.println(" --- ret val: " + retVal);
        return retVal;


    }
}
