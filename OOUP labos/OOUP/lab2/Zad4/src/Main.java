

public class Main {

    public static void main(String[] args){
        //generators
        FibonacciGenerator fibGen = new FibonacciGenerator(10);
        NormalDistributionGenerator norGen = new NormalDistributionGenerator(5, 6, 10);
        SequentialGenerator seqGen = new SequentialGenerator(37, 73, 2);


        //Percentile finders
        ClosestElementPercentile ceFinder = new ClosestElementPercentile();
        InterpolatedPercentileFinder ipFinder = new InterpolatedPercentileFinder();


        //DistributionTester tester = new DistributionTester();
        //tester.printPercentile(fibGen, finder);

        //DistributionTester.printPercentile(fibGen, ceFinder);
        //DistributionTester.printPercentile(norGen, ceFinder);
        //DistributionTester.printPercentile(seqGen, ceFinder);

        //DistributionTester.printPercentile(fibGen, ipFinder);
        DistributionTester.printPercentile(norGen, ipFinder);
        //DistributionTester.printPercentile(seqGen, ipFinder);
    }




}
