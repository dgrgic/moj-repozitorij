import java.util.OptionalInt;

public interface NumberGenerator {

    public OptionalInt getNumber();
    public boolean hasNext();
}
