import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Scanner;

public class DatotecniIzvor implements Izvor {

    Scanner sc;
    Path source;

    public DatotecniIzvor(Path source) {
        this.source = source;
        try {
            this.sc = new Scanner(this.source);
        } catch (IOException e) {
            System.out.println("Nista od citanja, sorry");
        }
    }

    @Override
    public int dajBroj() {
        while (!sc.hasNextInt());
        if(sc.hasNextInt()){
            return sc.nextInt();
        }

        return -1;

    }
}
