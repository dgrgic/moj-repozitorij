import java.util.Scanner;

public class TipkovnickiIzvor implements Izvor {

    Scanner sc;

    public TipkovnickiIzvor() {
        this.sc = new Scanner(System.in);
    }

    @Override
    public int dajBroj() {
        while (!sc.hasNextInt());

        return sc.nextInt();
    }
}
