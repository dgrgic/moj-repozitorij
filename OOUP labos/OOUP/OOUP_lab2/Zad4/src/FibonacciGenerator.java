import java.util.OptionalInt;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

public class FibonacciGenerator implements NumberGenerator{

    private int numberOfElements;
    private int first;
    private int second;
    private int count;


    public FibonacciGenerator(int numberOfElements) {
        this.numberOfElements = numberOfElements;
        this.first = 0;
        this.second = 1;
        this.count = 0;
    }

    @Override
    public OptionalInt getNumber() {
        //System.out.println("count, first, second: " + this.count + " " + this.first + " " + this.second);
        if (this.count >= this.numberOfElements){
            return OptionalInt.empty();
        } else {
            OptionalInt numberFinal;
            int next = this.first + this.second;
            numberFinal = OptionalInt.of(this.second);

            this.first = this.second;
            this.second = next;
            this.count += 1;

            //System.out.println("I'm next: " + next);

            return numberFinal;
        }
    }

    @Override
    public boolean hasNext() {
        //System.out.println("Here");
        if (this.count >= this.numberOfElements){
            return FALSE;
        } else {
            return TRUE;
        }
    }




}
