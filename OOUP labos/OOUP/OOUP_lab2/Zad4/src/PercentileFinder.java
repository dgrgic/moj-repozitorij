import java.util.List;

public interface PercentileFinder {

    Integer getPercentile(int percentileNo, List<Integer> sortedList);
}
