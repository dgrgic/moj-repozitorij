import java.util.ArrayList;
import java.util.Collections;
import java.util.OptionalInt;

public class DistributionTester {


    public static void printPercentile(NumberGenerator numGen, PercentileFinder finder){

        ArrayList<Integer> list = new ArrayList<>();

        while (numGen.hasNext()){
            OptionalInt opt = numGen.getNumber();
            Integer number;
            //System.out.println("--- Iziso je novi opt: " + opt);
            if (opt.isPresent()){
                //System.out.println("------- i postoji! " );
                number = opt.getAsInt();
                list.add(number);
                //System.out.println("------------- to je to: " + number);
            }

        }

        Collections.sort(list);

        System.out.println(list);

        for (int i = 10; i < 100; i += 10){
            Integer percentile = finder.getPercentile(i, list);
            System.out.println(i + ". percentile : " + percentile);
        }

    }


}
