import java.util.OptionalInt;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

public class SequentialGenerator implements NumberGenerator{

    private int lowerBound;
    private int upperBound;
    private int step;
    private int current;

    public SequentialGenerator(int lowerBound, int upperBound, int step) {
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
        this.step = step;
        this.current = this.lowerBound;
    }

    @Override
    public OptionalInt getNumber() {
        if (this.current <= this.upperBound){
            OptionalInt returnValue = OptionalInt.of(this.current);
            this.current += this.step;
            return returnValue;
        } else {
            return OptionalInt.empty();
        }


    }

    @Override
    public boolean hasNext() {
        if (this.current <= this.upperBound){
            return TRUE;
        } else {
            return FALSE;
        }
    }
}
