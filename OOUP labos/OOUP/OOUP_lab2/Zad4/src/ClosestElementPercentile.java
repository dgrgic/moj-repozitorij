import java.util.List;

public class ClosestElementPercentile implements PercentileFinder {


    @Override
    public Integer getPercentile(int percentileNo, List<Integer> sortedList) {
        int sizeOfList = sortedList.size();
        double n_p;
        int element_index;

        n_p = ((double) percentileNo * sizeOfList / 100.0) + 0.5;

        element_index = (int) Math.round(n_p);

        return sortedList.get(element_index - 1 );
    }
}