#include <stdio.h>
#include <string.h>
#include <iostream>

/*funkcija mymax
*	- pronalazi najveci element zadanog polja
*	- primjenjivo na polja elemenata svih tipova
*	- omoguciti rad s raznim vrstama usporedbi
*	- prima pokazivac na kriterijsku funkciju (1 - arg1 > arg2; 0 inace)
*/

template <typename Iterator, typename Predicate>
Iterator mymax(Iterator cur, Iterator last, Predicate pred){
	/*
	std::cout << "mymax\n";
	std::cout << "cur = " << cur << "  last = " << last << "\n";
	*/
	Iterator maxi;
	Iterator temp;

	maxi = cur;
	/*temp = cur;*/

	do {
		/*
		std::cout << "cur = " << cur << "  last = " << last << "\n";
		*/
		if (pred(cur, maxi)){
			maxi = cur;
		}
		++cur;
	} while( cur != last);
	return maxi;
}

/*
* Definirati kriterijske funkcije za usporedbu:
* 	- cijelih brojeva	- gt_int
*	- znakova			- gt_char
*	- znakovnih nizova	- gt_str -> delegirati strcmp
*/

int gt_int(const void* a, const void* b){
	/*
	std::cout << "gt_int\n";
	*/
	int a1, b1;
	a1 = (int) *((int*) a);
	b1 = (int) *((int*) b);

	/*
	std::cout << a1 << " - " << b1 << "\n";
	*/

	return a1 > b1;
}

int gt_char(const void* a, const void* b){
	/*
	std::cout << "gt_char\n";
	*/
	char a1, b1;
	a1 = (char) *((char*) a);
	b1 = (char) *((char*) b);

	return a1 > b1;
}

int gt_str(const void* a, const void* b){
	/*
	std::cout << "gt_str\n";
	*/
	char** a1;
	char** b1;
	a1 = (char**) a;
	b1 = (char**) b;

	return strcmp(*a1, *b1) > 0;
}




int main(){

	/* Polja koja treba sortirati */
	int arr_int[] = { 1, 3, 5, 7, 4, 6, 9, 2, 0};
	char arr_char[]="Suncana strana ulice";
	const char* arr_str[] = {
   		"Gle", "malu", "vocku", "poslije", "kise",
   		"Puna", "je", "kapi", "pa", "ih", "njise"
	};

	const int* maxInt;
	const char* maxChar;
	const char** maxStr;

	/*
	std::cout << "main\n";
	*/

	/* Pozivi funkcija */
	maxInt = mymax( &arr_int[0], &arr_int[sizeof(arr_int) / sizeof(*arr_int)], gt_int);
	std::cout <<"MaxInt = " <<*maxInt <<"\n";
	maxChar = mymax(&arr_char[0], &arr_char[sizeof(arr_char) / sizeof(*arr_char)], gt_char);
	std::cout <<"MaxChar = " <<*maxChar <<"\n";
	maxStr = mymax(&arr_str[0], &arr_str[sizeof(arr_str) / sizeof(*arr_str)], gt_str);
	std::cout <<"MaxStr = " <<*maxStr <<"\n";

}