#include <stdio.h>
#include <string.h>

/*funkcija mymax
*	- pronalazi najveci element zadanog polja
*	- primjenjivo na polja elemenata svih tipova
*	- omoguciti rad s raznim vrstama usporedbi
*	- prima pokazivac na kriterijsku funkciju (1 - arg1 > arg2; 0 inace)
*/

typedef int (*compar)(const void*, const void*);

/*
* base - pokazivac na polje elemenata koje obradjujemo
* nmemb - broj elemenata polja base
* size - velicina 1 elementa polja base
* compar - funkcija koja prima 2 elementa i usporedjuje ih
*/
const void* mymax(const void *base, size_t nmemb, size_t size, compar function){

	int i;
	void* element;
	void* maxElement = (void*) base;

	/*
	printf("nmemb = %d; size = %d; \n", nmemb, size);
	*/

	for (i=0; i<nmemb; i++){
		element = (void*) base+i*size;

		/*
		printf(" - - - %d, element = %d, %d \n", i, element, *((int*)element));
		*/

		if (function(element, maxElement)){
			maxElement = element;
		}

	}

	return maxElement;

}

/*
* Definirati kriterijske funkcije za usporedbu:
* 	- cijelih brojeva	- gt_int
*	- znakova			- gt_char
*	- znakovnih nizova	- gt_str -> delegirati strcmp
*/

int gt_int(const void* a, const void* b){
	int a1, b1;
	a1 = (int) *((int*) a);
	b1 = (int) *((int*) b);

	/*
	printf("Adrese: %d, %d\n Usporedjujem: %d i %d\n", a, b, a1, b1);
	*/

	return a1 > b1;
}

int gt_char(const void* a, const void* b){
	char a1, b1;
	a1 = (char) *((char*) a);
	b1 = (char) *((char*) b);

	return a1 > b1;
}

int gt_str(const void* a, const void* b){
	char** a1;
	char** b1;
	a1 = (char**) a;
	b1 = (char**) b;

	/*
	printf("stringovi: %s, %s\n", *a1, *b1);

	if (strcmp(*a1, *b1) > 0){
		printf("\t veci string = %s, manji string = %s\n", *a1, *b1);
	}
	else {
		printf("\t veci string = %s, manji string = %s\n", *b1, *a1);
	}
	*/

	return strcmp(*a1, *b1) > 0;
}




int main(void){

	/* Polja koja treba sortirati */
	int arr_int[] = { 1, 3, 5, 7, 4, 6, 9, 2, 0 };
	char arr_char[]="Suncana strana ulice";
	const char* arr_str[] = {
   		"Gle", "malu", "vocku", "poslije", "kise",
   		"Puna", "je", "kapi", "pa", "ih", "njise"
	};

	const int* maxInt;
	const char* maxChar;
	const char** maxStr;

	/* Pozivi funkcija */

	/*printf("--- maxInt \n");*/
	maxInt = (const int*) mymax(arr_int, sizeof(arr_int) / sizeof(int), sizeof(int), &gt_int);
	/*printf("--- maxChar \n");*/
	maxChar = (const char*) mymax(arr_char, sizeof(arr_char) / sizeof(char), sizeof(char), &gt_char);
	/*printf("--- maxStr \n");*/
	maxStr = (const char**) mymax(arr_str, sizeof(arr_str) / sizeof(maxStr), sizeof(maxStr), &gt_str);

	printf("MaxInt = %d\n", *maxInt);
	printf("MaxChar = %c\n", *maxChar);
	printf("MaxStr = %s\n", *maxStr);

}