﻿using System;
using System.IO;
using System.Collections.Generic;

namespace Zadatak5
{
    interface ISubjekt
    {
        void AttachObserver(IPromatrac observer);
        void DetachObserver(IPromatrac observer);
        void Notify();
        List<int> getData();
    }

    interface IPromatrac
    {
        void Update();
    }

    interface IIzvor
    {
        int ReadNumber();
    }




    class TipkovnickiIzvor : IIzvor
    {
        public TipkovnickiIzvor()
        {
            Console.WriteLine("Reading numbers from command line" + "\n");
        }

        public int ReadNumber()
        {
            return Int32.Parse(Console.ReadLine());
        }
    }

    class DatotecniIzvor : IIzvor
    {
        private string pathToFile;
        private TextReader sr;

        public DatotecniIzvor(string pathToFile)
        {
            this.pathToFile = pathToFile;
            try
            {
                sr = File.OpenText(this.pathToFile);
                Console.WriteLine("Reading numbers from file" + "\n");
            }
            catch (IOException e)
            {
                Console.WriteLine("Couldn't open file" + "\n");
            }
            
        }

        public int ReadNumber()
        {
            return Int32.Parse(sr.ReadLine());

        }
    }



//zapisati sve elemente iz liste u txt file + timestamp
    class TekstualniPromatrac : IPromatrac
    {
        private string path = "C:\\Users\\Doc\\Desktop\\output.txt";

        private ISubjekt subjekt;        
        private DateTime timestamp;
        private StreamWriter sw;
        List<int> numbers;

        public TekstualniPromatrac(ISubjekt subjekt)
        {
            this.subjekt = subjekt;
            this.subjekt.AttachObserver(this);
        }

        public void Update()
        {
            numbers = subjekt.getData();
            timestamp = DateTime.Now;
            string stringBuilder = "";

            using(sw = new StreamWriter(path, true))
            {
                numbers.ForEach(n =>
                {
                    stringBuilder += (n + " ");
                });
                
                stringBuilder += "-> ";
                stringBuilder += timestamp;
                sw.WriteLine(stringBuilder);
            }

        }
    }

    class PromatracSuma : IPromatrac
    {
        ISubjekt subjekt;
        List<int> numbers;
        int suma;

        public PromatracSuma(ISubjekt subjekt)
        {
            this.subjekt = subjekt;
            this.subjekt.AttachObserver(this);
        }

        public void Update()
        {
            numbers = subjekt.getData();
            suma = 0;
            numbers.ForEach(n =>
            {
                suma += n;
            });
            Console.WriteLine("Sum: " + suma);
        }
    }

    class PromatracProsjek : IPromatrac
    {
        ISubjekt subjekt;
        List<int> numbers;
        int suma;

        public PromatracProsjek(ISubjekt subjekt)
        {
            this.subjekt = subjekt;
            this.subjekt.AttachObserver(this);
        }

        public void Update()
        {
            numbers = subjekt.getData();
            suma = 0;
            numbers.ForEach(n =>
            {
                suma += n;
            });
            Console.WriteLine("Average: " + suma / numbers.Count);
        }
    }

    class PromatracMedijan : IPromatrac
    {
        ISubjekt subjekt;
        List<int> numbers;

        public PromatracMedijan(ISubjekt subjekt)
        {
            this.subjekt = subjekt;
            this.subjekt.AttachObserver(this);
        }

        public void Update()
        {
            numbers = subjekt.getData();
            int size = numbers.Count;
            double median = (double)size / 2 + 0.5;
            Console.WriteLine("Median: " + numbers[(int)Math.Round(median)-1]);
        }
    }




    class SlijedBrojeva : ISubjekt
    {
        private List<int> numbers;
        private List<IPromatrac> observers;
        private IIzvor izvor;

        public SlijedBrojeva(IIzvor izvor)
        {
            this.izvor = izvor;
            this.numbers = new List<int>();
            this.observers = new List<IPromatrac>();
        }

        public void Read()
        {
            int number;

            while(true)
            {
                number = izvor.ReadNumber();

                if (number < -1)
                {
                    Console.WriteLine("Mistake, number can't be negative!!!");
                    continue;
                }

                else if (number == -1){
                    Console.WriteLine("This is the end");
                    break;
                }

                numbers.Add(number);

                Notify();

                System.Threading.Thread.Sleep(1000);
            }
        }

        public void AttachObserver(IPromatrac observer)
        {
            observers.Add(observer);
        }

        public void DetachObserver(IPromatrac observer)
        {
            observers.Remove(observer);
        }

        public void Notify()
        {
            for(int i=0; i<observers.Count; i++)
            {
                IPromatrac observer = observers[i];
                observer.Update();
            }
            Console.WriteLine("");
        }

        public List<int> getData()
        {
            return numbers;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            TipkovnickiIzvor tipIzvor = new TipkovnickiIzvor();
            //DatotecniIzvor datIzvor = new DatotecniIzvor("C:\\Users\\Doc\\Desktop\\izvor.txt");

            SlijedBrojeva slijed = new SlijedBrojeva(tipIzvor);

            TekstualniPromatrac textObserver = new TekstualniPromatrac(slijed);
            PromatracSuma sumObserver = new PromatracSuma(slijed);
            PromatracProsjek averageObserver = new PromatracProsjek(slijed);
            PromatracMedijan medianObserver = new PromatracMedijan(slijed);

            slijed.Read();

        }
    }
}
