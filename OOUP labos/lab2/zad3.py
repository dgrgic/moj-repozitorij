def mymax(iterable, key = lambda x:x):

    max_x = max_key = None
    for x in iterable:
        if max_key is None or key(x) > max_key:
            max_x = x
            max_key = key(x)
    return max_x


# key je ustvari lambda izraz koji preslikava x u odredeni tip za usporedbu!!!


entryInt = [1, 3, 5, 7, 4, 6, 9, 2, 0]
entryChar = "Suncana strana ulice"
entryStr = ["Gle", "malu", "vocku", "poslije", "kise",
  "Puna", "je", "kapi", "pa", "ih", "njise"]

maxInt = mymax(entryInt)
# print("")
maxChar = mymax(entryChar)
# print("")
maxStr = mymax(entryStr)
# print("")

# print(maxInt, maxChar, maxStr)
# print("")

D={'burek':8, 'buhtla':5}

maxDict = mymax(D, lambda x: D.get(x))

# print("Max value in dict:", maxDict)
# print("")

people = [("Dorijan", "Grgic"),("Nika", "Brasnovic"),("Davor", "Micic"),
    ("Matej", "Juric"),("Andrea", "Omicevic")]

# usporeduje stringove prvo po prezimenu pa po imenu (leksikografski poredak)
maxPerson = mymax(people, lambda x: x[1] + " " + x[0])

# print("Latest person:", maxPerson)


lista = [('a', 0.56), ('b', 0.12), ('c', 0.78)]

print(sorted(lista, key=lambda t: -t[1])[:2])
