#include <iostream>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

struct Point{
   int x; int y;
};
struct Shape{
  enum EType {circle, square, rhomb};
  EType type_;
};
struct Circle{
  Shape::EType type_;
  double radius_;
  Point center_;
};
struct Square{
  Shape::EType type_;
  double side_;
  Point center_;
};
struct Rhomb{
  Shape::EType type_;
  double side_;
  Point center_;
};
void drawSquare(struct Square*){
  std::cerr <<"in drawSquare\n";
}
void drawCircle(struct Circle*){
  std::cerr <<"in drawCircle\n";
}
void drawRhomb(struct Rhomb*){
  std::cerr <<"in drawRhomb\n";
}
void drawShapes(Shape** shapes, int n){
  for (int i=0; i<n; ++i){
    struct Shape* s = shapes[i];
    switch (s->type_){
    case Shape::square:
      drawSquare((struct Square*)s);
      break;
    case Shape::circle:
      drawCircle((struct Circle*)s);
      break;
    case Shape::rhomb:
      drawRhomb((struct Rhomb*)s);
      break;
    default:
      assert(0);
      exit(0);
    }
  }
}
void moveShapes(Shape** shapes, int n, int moveIndex){
  for (int i=0; i<n; ++i){
    struct Shape* s = shapes[i];
    switch(s->type_){
    case Shape::square:{
      struct Square* sq = (struct Square*)s;
      std::cout << "MoveShapes square" << '\n';
      printf("Previous x: %d\n", sq->center_.x);
      sq->center_.x += moveIndex;
      printf("New x: %d\n", sq->center_.x);
      break;
    }
    case Shape::circle:{
      struct Circle* circle = (struct Circle*)s;
      std::cout << "MoveShapes circle" << '\n';
      printf("Previous x: %d\n", circle->center_.x);
      circle->center_.x += moveIndex;
      printf("New x: %d\n", circle->center_.x);
      break;
    }
/* Kad ne ažuriram moveShapes onda se poziva ASSERT jer je to default
  radnja kad se ne nađe konkretni shape type
  a assert je funkcija koja može prekinuti program sa određenom porukom
  u slučaju da je izraz koji je primila FALSE */
    case Shape::rhomb:{
      struct Rhomb* rb = (struct Rhomb*)s;
      std::cout << "MoveShapes rhomb" << '\n';
      printf("Previous x: %d\n", rb->center_.x);
      rb->center_.x += moveIndex;
      printf("New x: %d\n", rb->center_.x);
      break;
    }
    default:
      assert(0);
      exit(0);
    }
  }
}
int main(){
  Shape* shapes[5];
  shapes[0]=(Shape*)new Circle;
  shapes[0]->type_=Shape::circle;
  shapes[1]=(Shape*)new Square;
  shapes[1]->type_=Shape::square;
  shapes[2]=(Shape*)new Square;
  shapes[2]->type_=Shape::square;
  shapes[3]=(Shape*)new Circle;
  shapes[3]->type_=Shape::circle;
  shapes[4]=(Shape*)new Rhomb;
  shapes[4]->type_=Shape::rhomb;
  drawShapes(shapes, 5);
  moveShapes(shapes, 5, 2);
}
