#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <iostream>

typedef int (*compar)(const void*, const void*);

int gt_int(const void* a, const void* b){
  return (*(int*)a > *(int*)b);
}

int gt_char(const void* a, const void* b){
  return ( *(char*)a > *(char*)b );
}

int gt_str(const void* a, const void* b){
  char** a1 = (char**) a;
  char** b1 = (char**) b;
  return (strcmp(*a1,*b1) > 0);
}

template <typename Iterator, typename Predicate>
Iterator mymax(Iterator cur, Iterator last, Predicate pred){
  // std::cout << "Pointer-> vrijednost varijable " << *cur << '\n';
  // std::cout << "Adresa varijable" << '\n';
  // std::cout << cur << '\n';
  // std::cout << "Adresa pointera" << '\n';
  // std::cout << &cur << '\n';

  Iterator max = cur;
  // std::cout << "Iterator max " << *max << " " << max << '\n';
  // std::cout << "Curent ++ " << cur << " " << cur+1 << '\n';
  // std::cout << "\n";

  while(cur != last){
    if(pred(cur, max)){
        max = cur;
    }
    cur++;
    // std::cout << cur << "\n";
  }
  return max;
}

int main(){
  int arr_int[] = { 1, 3, 5, 7, 4, 6, 9, 2, 0 };
  char arr_char[]="Suncana strana ulice";
  const char* arr_str[] = {
     "Gle", "malu", "vocku", "poslije", "kise",
     "Puna", "je", "kapi", "pa", "ih", "njise"
  };
  const int* max_int = mymax(&arr_int[0], &arr_int[sizeof(arr_int)/sizeof(*arr_int)-1], &gt_int);
  const char* max_char = mymax(&arr_char[0], &arr_char[sizeof(arr_char)/sizeof(*arr_char)-1], &gt_char);
  const char** max_str = mymax(&arr_str[0], &arr_str[sizeof(arr_str)/sizeof(*arr_str)-1], &gt_str);

  printf("%d je max_int\n", *max_int);
  printf("%c je max_char\n", *max_char);
  printf("%s je max_str\n", *max_str);

  return 0;
}
