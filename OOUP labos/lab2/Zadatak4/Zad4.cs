﻿using System;
using System.Collections.Generic;

namespace Zadatak4
{
    interface INumberGenerator
    {
        List<int> generateNumbers();
    }

    interface IPercentilFinder
    {
        int findPercentil(int p, List<int> listOfNumbers);
    }

    class SequentialNumberGenerator : INumberGenerator
    {
        private int lowerBound;
        private int upperBound;
        private int step;
        private List<int> numbers;

        public SequentialNumberGenerator(int lowerBound, int upperBound, int step)
        {
            this.lowerBound = lowerBound;
            this.upperBound = upperBound;
            this.step = step;
        }

        public List<int> generateNumbers()
        {
            numbers = new List<int>();
            for(int i=lowerBound; i<=upperBound; i += step)
            {
                numbers.Add(i);
            }

            numbers.Sort();
            return numbers;
        }
    }

    class RandomNumberGenerator : INumberGenerator
    {
        private int mean;
        private int variance;
        private int numberOfElements;
        private List<int> numbers;
        private Random random;

        public RandomNumberGenerator(int mean, int variance, int numberOfElements)
        {
            this.mean = mean;
            this.variance = variance;
            this.numberOfElements = numberOfElements;
        }

        public List<int> generateNumbers()
        {
            random = new Random();
            numbers = new List<int>();
            int counter = 0;

            while (counter <= numberOfElements)
            {
                int randomNumber = random.Next();
                numbers.Add(randomNumber * mean + variance);
                counter++;
            }

            numbers.Sort();
            return numbers;
        }
    }

    class FibonacciNumberGenerator : INumberGenerator
    {
        private int numberOfElements;
        private List<int> numbers;

        public FibonacciNumberGenerator(int number)
        {
            this.numberOfElements = number;
        }

        public List<int> generateNumbers()
        {
            numbers = new List<int>();
            int counter = 0;

            int current = 0;
            int next = 1;
            int newNumber;

            numbers.Add(current);
            numbers.Add(next);

            while (counter <= numberOfElements)
            {
                newNumber = current + next;
                numbers.Add(newNumber);

                current = next;
                next = newNumber;

                counter++;
            }

            numbers.Sort();
            return numbers;
        }
    }

    class ClosestPercentile : IPercentilFinder
    {
        public int findPercentil(int p, List<int> listOfNumbers)
        {
            int sizeOfList = listOfNumbers.Count;
            double n_p = ((double)p * sizeOfList / 100) + 0.5;
            int index = (int)Math.Round(n_p);
            return listOfNumbers[index-1];
        }
    }

    class InterpolatedPercentile : IPercentilFinder
    {
        public int findPercentil(int p, List<int> listOfNumbers)
        {
            int sizeOfNumbers = listOfNumbers.Count;
            int v1;
            int v2;
            int pv1;
            int pv2;
            int v = 0;

            for(int i=1; i<sizeOfNumbers; i++)
            {
                v1 = listOfNumbers[i-1];
                v2 = listOfNumbers[i];

                pv1 = calculatePercentilIndex(i, sizeOfNumbers);
                pv2 = calculatePercentilIndex(i+1, sizeOfNumbers);

                if (pv1 < p && p < pv2)
                {
                    v = calculatePercentil(v1, v2, pv1, p, sizeOfNumbers);
                    break;
                }
                else if (p <= pv1)
                {
                    v = v1;
                    break;
                }
                else if (p >= pv2)
                {
                    v = v2;
                }
            }
            return v;
        }

        private int calculatePercentil(int v1, int v2, int pv1, int p, int size)
        {
            double v = v1 + ((double)size * (p - pv1) * (v2 - v1) / 100);
            return (int)Math.Round(v);
        }

        private int calculatePercentilIndex(int index, int size)
        {
            double per = 100 * (index - 0.5) / size;
            return (int)Math.Round(per);
        }
    }

    class DistributionTester
    {
        public static void printPercentil(INumberGenerator numGen,  IPercentilFinder perFind)
        {
            List<int> numbers = numGen.generateNumbers();

            for(int i=10; i<100; i += 10)
            {
                int percentile = perFind.findPercentil(i, numbers);
                Console.WriteLine(i + "ti percentil: " + percentile);
            }
            Console.WriteLine("");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            SequentialNumberGenerator seq = new SequentialNumberGenerator(2,10,3);
            FibonacciNumberGenerator fib = new FibonacciNumberGenerator(12);
            RandomNumberGenerator random = new RandomNumberGenerator(2, 5, 10);

            ClosestPercentile cper = new ClosestPercentile();
            InterpolatedPercentile iper = new InterpolatedPercentile();

            DistributionTester.printPercentil(seq, cper);
            DistributionTester.printPercentil(seq, iper);
            DistributionTester.printPercentil(fib, cper);
            DistributionTester.printPercentil(fib, iper);
            DistributionTester.printPercentil(random, cper);
            DistributionTester.printPercentil(random, iper);
        }
    }
}
