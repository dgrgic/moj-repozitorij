#include <stdio.h>
#include <stdlib.h>
#include "string.h"
typedef int (*compar)(const void*, const void*);

int gt_int(const void* a, const void* b){
  return (*(int*)a > *(int*)b);
}

int gt_char(const void* a, const void* b){
  return ( *(char*)a > *(char*)b );
}

int gt_str(const void* a, const void* b){
  char** a1 = (char**) a;
  char** b1 = (char**) b;
  return (strcmp(*a1,*b1) > 0);
}
/* NBP se ostvaruje delegiranjem posla metodi preko pokazivača
  Da bi nadograđivali ovaj kod, potrebno je dodati metodu te u main-u
  prilagoditi metodu */
const void* mymax(void* base, size_t nitems, size_t size, compar function){
  int i;
  void* element;
  void* max = base;
  for(i=0; i<nitems; i++){
      element = base+i*size;
      if(function(element,max)){
          max=element;
      }
  }
  return max;
}

int main(){
  int arr_int[] = { 1, 3, 5, 7, 4, 6, 9, 2, 0 };
  char arr_char[]="Suncana strana ulice";
  const char* arr_str[] = {
     "Gle", "malu", "vocku", "poslije", "kise",
     "Puna", "je", "kapi", "pa", "ih", "njise"
  };
  const int* max_int;
  const char* max_char;
  const char** max_str;

  max_int = (const int*) mymax(arr_int, sizeof(arr_int) / sizeof(int), sizeof(int), &gt_int);
  printf("%d je max_int\n", *max_int);

  max_char = (const char*) mymax(arr_char, sizeof(arr_char) / sizeof(char), sizeof(char), &gt_char);
  printf("%c je max_char\n", *max_char);

  max_str = (const char**) mymax(arr_str, sizeof(arr_str) / sizeof(max_str), sizeof(max_str), &gt_str);
  printf("%s je max_str\n", *max_str);

  return 0;
}
