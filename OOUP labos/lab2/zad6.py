import string

Letters = list(string.ascii_uppercase)
Numbers = list(string.digits)
Numbers.remove("0")


class Cell:

    # cell je ujedno i subject i observer

    def __init__(self, exp, sheet):
        self.exp = exp
        self.sheet = sheet
        self.value = 0

        self.observers = set()
        self.subjects = set()

    def __str__(self):
        return str(self.exp) + ":=" + str(self.value)

    def set_value(self):
        return self.sheet.evaluate(self)

    def get_exp(self):
        return self.exp

    def attach(self, cell):
        if cell not in self.observers:
            self.observers.add(cell)

    def detach(self, cell):
        self.observers.remove(cell)

    def notify(self):
        # print("Notifying observers for " + str(self))
        for o in self.observers:
            # print(str(o))
            o.update()

    def update(self):
        # print("Updating observer " + str(self))
        cells = self.sheet.getrefs(self)
        for c in cells:
            if c in self.observers:
                raise RuntimeError
        self.value = self.set_value()
        self.notify()

    # ovdje sada trebam obavijestiti svoje pretplatnike(self.observers) o promjeni
    # mijenja subjekte radi novog izraza te obavjestava svoje promatrace o promjeni sebe
    def change_exp(self, content):

        # print("Changing exp for cell " + str(self) + " to " + content)
        self.exp = content
        # print(self.exp, self.value)

        self.value = self.set_value()
        # print(self.exp, self.value)

        # makni sve subjekte jer ne mogu provjeriti prethodni value pa usporediti
        # jel novi exp vec postojeci subjekt ili nije

        # print("Current subjects for " + str(self) + " are " + str(len(self.subjects)))
        for sub in self.subjects:
            sub.dettach(self)
            self.subjects.remove(sub)
        # print("New subjects for " + str(self) + " are " + str(len(self.subjects)))

        # getrefs vraca listu subjekata za trenutni cell
        cells = self.sheet.getrefs(self)
        # print("New subjects for " + str(self) + " are " + str(len(self.subjects)))

        # za trenutni cell dodat svaki subjekt iz liste
        # dodatno za svaki subjekt iz liste attachat trenutni cell
        for c in cells:
            c.attach(self)
            self.subjects.add(c)

        self.notify()


class Sheet:

    def __init__(self, rows, columns):

        self.table = [[Cell("0", self) for j in range(columns)] for i in range(rows)]
        self.dictionary = make_dictionary(rows, columns)

    def __str__(self):
        sheet_row = ""
        for row in self.table:
            for cell in row:
                sheet_row += str(cell) + " "
            sheet_row += "\n"
        return sheet_row

    # vraca cell od zadanog imena
    def cell(self, ref):
        dictionary = self.dictionary

        if ref in dictionary.keys():
            x, y = dictionary[ref]

            return self.table[x][y]
        else:
            print("Nema me, {0}", ref)
        return None

    # nade cell na mjestu ref i njemu update-a exp na content
    def set(self, ref, content):
        dictionary = self.dictionary
        # print("Setting content " + content + " to cell " + ref)
        if ref in dictionary.keys():
            x, y = dictionary[ref]
            self.table[x][y].change_exp(content)
        else:
            print("There is no ref{0} in dictionary", ref)

    # vraca listu cellova koje referencira cell u svojem value izrazu
    # vraca listu subjekata za trenutni cell
    def getrefs(self, cell):
        expression = cell.get_exp()
        new_cells = []

        if '+' in str(expression):
            ref_cells = expression.split('+')
            for c in ref_cells:
                x, y = self.dictionary[c.strip()]
                new_cells.append(self.table[x][y])

        #     for n in new_cells:
        #         print(str(n))
        # else:
        #     print("It's not an expression")

        return new_cells

    # evaluira numericku vrijednost zadanog cell-a
    # treba dobit refs od tog cella i za svaki pozvat evaluate da bi pronasao value
    # koji stavlja u varijablu te ju vraca
    def evaluate(self, cell):
        value = 0
        cells = self.getrefs(cell)

        if not cells:
            value += int(cell.exp.strip())
            return value
        else:
            for c in cells:
                value += self.evaluate(c)
            return value


def make_dictionary(rows, columns):

    dictionary = dict()

    for i in range(rows):
        for j in range(columns):
            key = Letters[i] + Numbers[j]
            dictionary[key] = (i, j)

    return dictionary


s = Sheet(5, 5)
s.set("A1", "2")
s.set("A2", "5")
s.set("A3", "A1+A2")
s.set("C3", "8")
print(s)
print("")

s.set("A1", "4")
s.set("A4", "A1+A3")
print(s)
print("")

s.set("B4", "A3+A4")
print(s)
print("")

s.set("A1", "3")
print(s)
print("")

s.set("A2", "7")
s.set("C1", "C3+A2")
print(s)
print("")

s.set("C3", "5")
print(s)
