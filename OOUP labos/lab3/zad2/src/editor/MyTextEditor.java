package editor;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;

import javax.swing.JComponent;

public class MyTextEditor extends JComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int width;
	private int height;
	
	public MyTextEditor(int width, int height) {
		this.width = width;
		this.height = height;
	}
	
	public void paint(Graphics graphics) {
		graphics.setColor(Color.red);
		graphics.drawLine(10, 10, 20, 10);
		graphics.drawLine(10, 10, 10, 20);
		graphics.setColor(Color.green);
		graphics.drawString("Lorem ipsum dolor sit amet", 10, 40);
		graphics.drawString("Lorem ipsum dolor sit amet", 10, 50);
	}

	
}
