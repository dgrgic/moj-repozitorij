package editor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import action.EditAction;
import location.Location;
import location.LocationRange;
import observer.CursorObserver;
import observer.TextObserver;

public class TextEditorModel {

	private static int height = 22;
	private static int width = 12;

	public List<String> lines;
	private LocationRange selectionRange;
	private Location cursorLocation;

	private List<CursorObserver> cursorObservers;
	private List<TextObserver> textObservers;

	public TextEditorModel(String pocTekst) {
		this.cursorObservers = new ArrayList<>();
		this.textObservers = new ArrayList<>();
		this.lines = new ArrayList<>(Arrays.asList(pocTekst.split("\n")));
		this.cursorLocation = new Location(0, 0);
	}

	public Location getCursorLocation(){
		return this.cursorLocation;
	}

	public void attachCursorObserver(CursorObserver c) {
		this.cursorObservers.add(c);
	}

	public void detachCursorObserver(CursorObserver c) {
		this.cursorObservers.remove(c);
	}

	public void attachTextObserver(TextObserver t) {
		this.textObservers.add(t);
	}

	public void dettachTextObserver(TextObserver t) {
		this.textObservers.remove(t);
	}

	public void notifyCursorObservers() {
		for (CursorObserver c : cursorObservers) {
			c.updateCursorLocation(cursorLocation);
		}
	}

	public void notifyTextObservers(){
		for (TextObserver t : this.textObservers){
			t.updateText();
		}
	}

	public void home() {
		if(this.cursorLocation.getColumn() != 0) {
			this.cursorLocation.setColumn(0);
			this.notifyCursorObservers();
			this.notifyTextObservers();
		}
	}
	
	public void end() {
		int col = this.cursorLocation.getColumn();
		int row = this.cursorLocation.getRow();
		String line = this.lines.get(row / height);
		
		if(col != line.length()) {
			this.cursorLocation.setColumn(line.length() * height);
			this.notifyCursorObservers();
			this.notifyTextObservers();
		}
	}
	
	public boolean moveCursorLeft() {
		System.out.println("\nLeft");

		int column = this.cursorLocation.getColumn();
		int row = this.cursorLocation.getRow();

		System.out.println("Trenutni: " + this.cursorLocation.toString());

		if (row == 0) {
			if (column == 0) {
				return false;
			}
			this.cursorLocation.setColumn(column - width);
			System.out.println("Nova pozicija: " + this.cursorLocation.toString());

			this.notifyCursorObservers();
			return true;
		} else {
			if (column == 0) {
				int previousLine = this.lines.get(row / height - 1).length();

				this.cursorLocation.setColumn(previousLine * width - 5);
				this.cursorLocation.setRow(row - height);
				System.out.println(row / height + ". redak, nova pozicija: " + this.cursorLocation.toString());
				this.notifyCursorObservers();
				return true;
			} else if (column > 0) {
				this.cursorLocation.setColumn(column - width);
				this.notifyCursorObservers();
				return true;
			} else {
				return false;
			}
		}
	}

	public boolean moveCursorRight() {
		System.out.println("\nRight");

		int column = this.cursorLocation.getColumn();
		int row = this.cursorLocation.getRow();

		System.out.println("Koordinate: " + this.cursorLocation.toString());

		int len = this.lines.get(row / height).length();
		int linesLen = this.lines.size();

		System.out.println("Duzina trenutnog retka: " + len);
		System.out.println("Broj redaka: " + linesLen);

		if (row / height == linesLen - 1) {
			if (column / width == len ) {
				return false;
			}
			this.cursorLocation.setColumn(column + width);
			this.notifyCursorObservers();
			return true;
		} else {
			if (column / width == len ) {
				this.cursorLocation.setColumn(0);
				this.cursorLocation.setRow(row + height);
				this.notifyCursorObservers();
				return true;
			} else {
				this.cursorLocation.setColumn(column + width);
				this.notifyCursorObservers();
				return true;
			}
		}
	}

	public boolean moveCursorUp() {
		System.out.println("\nUp");

		int row = this.cursorLocation.getRow();
		System.out.println("Koordinate: " + this.cursorLocation.toString());

		if (row == 0) {
			return false;
		} else {
			this.cursorLocation.setRow(row - height);
			System.out.println("Novi polozaj: " + this.cursorLocation.toString());
			this.notifyCursorObservers();
			return true;
		}
	}

	// mogucnosti:
	// 1) u zadnjem sam retku -> nema dalje
	// 2) nisam u zadnjem retku -> spusti se dolje
	public boolean moveCursorDown() {
		System.out.println("\nDown");

		int column = this.cursorLocation.getColumn();
		int row = this.cursorLocation.getRow();
		int lineLen = this.lines.get(row / height).length();
		int linesLen = this.lines.size();

		System.out.println("Koordinate: " + this.cursorLocation.toString());
		System.out.println("Broj redaka: " + linesLen);

		// ako je zadnji redak
		if (row / height == linesLen - 1) {
			return false;
		} else {
			if (column / width + 5 == lineLen - 1) {
				int len = this.lines.get(row / height + 1).length();
				this.cursorLocation.setRow(row + height);
				this.cursorLocation.setColumn(len * width - 5);
				System.out.println("Novi polozaj: " + this.cursorLocation.toString());
				this.notifyCursorObservers();
				return true;
			} else {
				this.cursorLocation.setRow(row + height);
				System.out.println("Novi polozaj: " + this.cursorLocation.toString());
				this.notifyCursorObservers();
				return true;
			}
		}
	}

//	backspace
	public boolean deleteBefore() {
		System.out.println("\nBackspace");
		int row = this.cursorLocation.getRow();
		int column = this.cursorLocation.getColumn();

		String line = this.lines.get(row/height);

		if(column == 0) {
			return false;
		}

		String leftLine = line.substring(0, column/width-1);
		String rightLine = line.substring(column/width);

		this.lines.remove(row/height);
		this.lines.add(row/height, leftLine + rightLine);

		this.cursorLocation.setColumn(column - width);

		this.notifyCursorObservers();
		this.notifyTextObservers();
		return true;
	}

//	delete
	public boolean deleteAfter() {
		System.out.println("\nDelete");
		int column = this.cursorLocation.getColumn();
		int row = this.cursorLocation.getRow();
		String line = this.lines.get(row/height);

		if(column/width == line.length()){
			return false;
		}

		String left = line.substring(0, column/width);
		String right = line.substring(column/width+1);

		System.out.println(left + right);

		this.lines.remove(row/height);
		this.lines.add(row/height, left + right);

		this.notifyCursorObservers();
		this.notifyTextObservers();
		return true;
	}

	public boolean deleteRange(LocationRange lr) {
		System.out.println("Deletam range");

		Location start = lr.getStart();
		Location end = lr.getEnd();

		int startRow = start.getRow();
		int startCol = start.getColumn();

		int endRow = end.getRow();
		int endCol = end.getColumn();

		if(endRow != startRow){
//			više redaka treba brisati
			System.out.println("Brisem vise redaka: " + startRow + ", " + endRow);

			if(endRow > startRow){
				String left = "";
				String right = "";

				for(int i=startRow/height; i<=endRow/height; i++){
					if(i == startRow/height){
						String line = lines.get(i);
						left = line.substring(0, startCol/width);
					}
					else if (i == endRow/height){
						String line = lines.get(i);
						right = line.substring(endCol/width);
					}
					else {
						lines.remove(i);
					}
				}

				this.lines.remove(endRow/height);
				this.lines.remove(startRow/height);

				this.lines.add(startRow/height, left + right);

				this.cursorLocation.setColumn(startCol);
				this.cursorLocation.setRow(startRow);

				this.notifyCursorObservers();
				this.notifyTextObservers();
				return true;

			} else {
				String left = "";
				String right = "";

				for(int i=endRow/height; i<=startRow/height; i++){
					if(i == startRow/height){
						String line = lines.get(i);
						right = line.substring(startCol/width);
					}
					else if (i == endRow/height){
						String line = lines.get(i);
						left = line.substring(0, endCol/width);
					}
					else {
						lines.remove(i);
					}
				}

				this.lines.remove(startRow/height);
				this.lines.remove(endRow/height);
				this.lines.add(endRow/height, left + right);

				this.cursorLocation.setColumn(endCol);
				this.cursorLocation.setRow(endRow);

				this.notifyCursorObservers();
				this.notifyTextObservers();

				return true;
			}
		} else {
//			jedan redak treba brisati
			System.out.println("Brisem jedan redak");
			String row = this.lines.get(endRow/height);
			String left = "";
			String right = "";

			if(startCol > endCol){
				left = row.substring(0, endCol/width);
				right = row.substring(startCol/width);
				this.lines.remove(endRow/height);
				this.lines.add(endRow/height, left + right);
				this.cursorLocation.setColumn(endCol);
			} else {
				left = row.substring(0, startCol/width);
				right = row.substring(endCol/width);
				this.lines.remove(endRow/height);
				this.lines.add(endRow/height, left + right);
				this.cursorLocation.setColumn(startCol);
			}

			this.notifyCursorObservers();
			this.notifyTextObservers();
			return true;
		}
	}

	public LocationRange getSelectionRange() {
		return this.selectionRange;
	}

	public void setSelectionRange(LocationRange lt) {
		this.selectionRange = lt;
	}

	public boolean insert(char c){

		int row = this.cursorLocation.getRow() / height;
		int column = this.cursorLocation.getColumn() / width;

		String line = this.lines.get(row);
		String left = line.substring(0, column);
		String right = line.substring(column);

		if(this.getSelectionRange() != null){
			System.out.println("Postoji highlight");
			this.deleteRange(this.getSelectionRange());
		}
		System.out.println("Ne postoji highlight");
		if(c == '\n'){
			this.lines.remove(row);
			this.lines.add(row, left + c);
			this.lines.add(row+1, right);
			this.cursorLocation.setColumn(0);
			this.cursorLocation.setRow(row*height + height);
		}
		else {
			this.lines.remove(row);
			this.lines.add(row, left + c + right);
			this.cursorLocation.setColumn(column*width + width);
		}

		this.notifyCursorObservers();
		this.notifyTextObservers();
		return true;
	}

	public boolean insert(String text){
		List<String> newText = Arrays.asList(text.split("\n"));
		int row = this.cursorLocation.getRow() / height;
		int column = this.cursorLocation.getColumn() / width;
		int len = 0;

		if(newText.size() == 1){
			String newLine = newText.get(0);
			len = newLine.length();
			String line = this.lines.get(row);
			String left = line.substring(0, column);
			String right = line.substring(column);

			this.lines.remove(row);
			this.lines.add(row, left + newLine + right);

			this.cursorLocation.setColumn(column*width + len*width);

			this.notifyCursorObservers();
			this.notifyTextObservers();
			return true;
		}
		return true;
	}

	public Iterator<String> allLines() {
		return new Iterator<String>() {

			private int count = 0;

			@Override
			public boolean hasNext() {
				return count < lines.size();
			}

			@Override
			public String next() {
				return lines.get(count++);
			}
		};
	}

	public Iterator<String> linesRange(int index1, int index2) {
		return new Iterator<String>() {

			private int count = index1 - 1;

			@Override
			public boolean hasNext() {
				return count < index2 - 1;
			}

			@Override
			public String next() {
				String line = lines.get(count++);
				return line;
			}
		};
	}

}
