package editor;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Iterator;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;

import clipboard.ClipboardStack;
import location.Location;
import location.LocationRange;
import manager.UndoManager;
import observer.CursorObserver;
import observer.TextObserver;
//import sun.nio.cs.ext.MacHebrew;

public class TextEditor extends JFrame implements CursorObserver, TextObserver {

	private static final long serialVersionUID = 1L;
	private TextEditorModel editorModel;
	private Location cursorLocation;
	private Editor editor;
	private Location selectionStart;

	public TextEditor() {
		super("Text editor by doc");
		TextEditorModel tem = new TextEditorModel("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\n"
				+ "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\n"
				+ "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\n"
				+ "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");
		Editor editor = new Editor(this);
		ClipboardStack clipboard = new ClipboardStack();
		UndoManager undoManager = UndoManager.getInstance();

		this.editor = editor;
		add(editor);
		editor.setSize(2000, 600);
		editor.setVisible(true);
		
		this.editorModel = tem;
		this.editorModel.attachCursorObserver(this);
		this.editorModel.attachTextObserver(this);
		this.cursorLocation = new Location(0, 0);
		
		setLocation(10,300);
		setSize(2000, 600);
		setLayout(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		
		addKeyListener(new KeyListener() {

			private boolean shift = false;
			private boolean ctrl = false;
			private LocationRange highlightRange = new LocationRange(new Location(0,0), new Location(0,0));
			private Location highlightStart = new Location(0, 0);

			@Override
			public void keyTyped(KeyEvent e) {
				return;
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_SHIFT){
					shift = false;
//					editorModel.setSelectionRange(null);
					selectionStart = null;
				}

				else if(e.getKeyCode() == KeyEvent.VK_CONTROL){
					ctrl = false;
				}
			}
			
			@Override
			public void keyPressed(KeyEvent e) {

				if(e.getKeyCode() == KeyEvent.VK_SHIFT){
					highlightStart.setRow(cursorLocation.getRow());
					highlightStart.setColumn(cursorLocation.getColumn());
					highlightRange.setStart(highlightStart);
					shift = true;

				}

				else if(e.getKeyCode() == KeyEvent.VK_CONTROL){
					System.out.println("Drzim CTRL");
					ctrl = true;
				}
				
				else if(e.getKeyCode() == KeyEvent.VK_HOME) {
					editorModel.home();
				}
				
				else if(e.getKeyCode() == KeyEvent.VK_END) {
					editorModel.end();
				}

				else if(e.getKeyCode() == KeyEvent.VK_KP_LEFT || e.getKeyCode() == KeyEvent.VK_LEFT) {
					if(shift) {
						if(editorModel.moveCursorLeft()){
							System.out.println("SHIFT lijevo:" + highlightStart);
							highlightRange.setEnd(cursorLocation);
							editorModel.setSelectionRange(highlightRange);
							editor.repaint();
						}
					}
					else {
						if(editorModel.moveCursorLeft()){
							highlightRange.setEnd(highlightStart);
							editor.repaint();
						}
					}
				}
				else if(e.getKeyCode() == KeyEvent.VK_KP_RIGHT || e.getKeyCode() == KeyEvent.VK_RIGHT) {
					if(shift){
						if(editorModel.moveCursorRight()){
							System.out.println("SHIFT desno: " + highlightStart);
							highlightRange.setEnd(cursorLocation);
							editorModel.setSelectionRange(highlightRange);
							editor.repaint();
						}
					}
					else {
						if(editorModel.moveCursorRight()) {
							highlightRange.setEnd(highlightStart);
							editor.repaint();
						}
					}
				}
				else if(e.getKeyCode() == KeyEvent.VK_KP_UP || e.getKeyCode() == KeyEvent.VK_UP) {
					if(shift){
						if(editorModel.moveCursorUp()) {
							System.out.println("Shift up");
							highlightRange.setEnd(cursorLocation);
							editorModel.setSelectionRange(highlightRange);
							editor.repaint();
						}
					}
					else {
						if(editorModel.moveCursorUp()){
							highlightRange.setEnd(highlightStart);
							editor.repaint();
						}
					}
				}
				else if(e.getKeyCode() == KeyEvent.VK_KP_DOWN || e.getKeyCode() == KeyEvent.VK_DOWN) {
					if(shift){
						if(editorModel.moveCursorDown()){
							System.out.println("Shift down");
							highlightRange.setEnd(cursorLocation);
							editorModel.setSelectionRange(highlightRange);
							editor.repaint();
						}
					}
					else{
						if(editorModel.moveCursorDown()) {
							highlightRange.setEnd(highlightStart);
							editor.repaint();
						}
					}
				}

				else if(e.getKeyCode() == KeyEvent.VK_BACK_SPACE){
					if(editorModel.getSelectionRange() != null){
						System.out.println("Backspace, " + editorModel.getSelectionRange().getStart().toString() + ", " + editorModel.getSelectionRange().getEnd().toString());
						editorModel.deleteRange(editorModel.getSelectionRange());
//						highlightRange.setEnd(cursorLocation);
						editorModel.setSelectionRange(null);

					} else {
						System.out.println("Ne postoji range");
						editorModel.deleteBefore();
					}
				}

				else if(e.getKeyCode() == KeyEvent.VK_DELETE){
					if(editorModel.getSelectionRange() != null){
						System.out.println("Backspace, " + editorModel.getSelectionRange().getStart().toString() + ", " + editorModel.getSelectionRange().getEnd().toString());
						editorModel.deleteRange(editorModel.getSelectionRange());
//						highlightRange.setEnd(cursorLocation);
						editorModel.setSelectionRange(null);

					} else {
						editorModel.deleteAfter();
					}
				}

//				za abecedu, brojeve, znakove
				else if(e.getKeyChar() >= 32 && e.getKeyChar() <= 126){
					if(editorModel.insert(e.getKeyChar())){
						System.out.println("Unesen znak:" + e.getKeyChar() + " " + e.getKeyCode());

					}
				}

//				enter
				else if(e.getKeyCode() == KeyEvent.VK_ENTER){
					if(editorModel.insert('\n')){
						System.out.println("Unesen znak:" + e.getKeyChar() + " " + e.getKeyCode());
					}
				}

//				ctrl + c (copy)
				else if(e.getKeyCode() == KeyEvent.VK_C){
					if(ctrl){
						if(editorModel.getSelectionRange() != null){
							Location start = editorModel.getSelectionRange().getStart();
							Location end = editorModel.getSelectionRange().getEnd();
							String text = "";

//							vise redaka je highlightano
							if(start.getRow() != end.getRow()){
								System.out.println("\nVise redaka je oznaceno za kopiranje");
								int begin = start.getRow() / 22;
								int exit = end.getRow() / 22;

								for(int i=begin; i<=exit; i++){
									if(i==begin){
										String line = editorModel.lines.get(i);
										text += line.substring(start.getColumn() / 12);
									}
									else if(i==exit){
										String line = editorModel.lines.get(i);
										text += line.substring(0, end.getColumn() / 12);
									}
									else {
										text += editorModel.lines.get(i);
									}
								}

								System.out.println("Tekst za pushanje: " + text);
							}
//							samo je jedan redak oznacen
							else {
								System.out.println("Jedan redak je oznacen za kopiranje");
								text += editorModel.lines.get(start.getRow() / 22).substring(start.getColumn() / 12, end.getColumn() / 12);
								System.out.println("Tekst za pushanje: " + text);
							}

							clipboard.push(text);
						}

						else{
							System.out.println("Nista od kopiranja brate");
						}
					}
				}

//				ctrl + x (cut)
				else if(e.getKeyCode() == KeyEvent.VK_X){
					if(ctrl){
						if(editorModel.getSelectionRange() != null){
							Location start = editorModel.getSelectionRange().getStart();
							Location end = editorModel.getSelectionRange().getEnd();
							String text = "";

//							vise redaka je highlightano
							if(start.getRow() != end.getRow()){
								System.out.println("\nVise redaka je oznaceno za kopiranje");
								int begin = start.getRow() / 22;
								int exit = end.getRow() / 22;

								for(int i=begin; i<=exit; i++){
									if(i==begin){
										String line = editorModel.lines.get(i);
										text += line.substring(start.getColumn() / 12);
									}
									else if(i==exit){
										String line = editorModel.lines.get(i);
										text += line.substring(0, end.getColumn() / 12);
									}
									else {
										text += editorModel.lines.get(i);
									}
								}

								System.out.println("Tekst za pushanje: " + text);
							}
//							samo je jedan redak oznacen
							else {
								System.out.println("Jedan redak je oznacen za kopiranje");
								text += editorModel.lines.get(start.getRow() / 22).substring(start.getColumn() / 12, end.getColumn() / 12);
								System.out.println("Tekst za pushanje: " + text);
							}

							editorModel.deleteRange(editorModel.getSelectionRange());
							clipboard.push(text);
						}

						else{
							System.out.println("Nista od kopiranja brate");
						}
					}
				}

//				ctrl + v (paste)
				else if(e.getKeyCode() == KeyEvent.VK_V){
					if(ctrl){
						String text = "";
						if(shift){
							if(!clipboard.isEmpty()){
								text = clipboard.pop();
								System.out.println("Tekst za pisanje: " + text);

							}
							else {
								System.out.println("Nema nista za pisanje");
							}
						}
						else {
							if(!clipboard.isEmpty()){
								text = clipboard.peek();
								System.out.println("Tekst za pisanje: " + text);
							}
							else{
								System.out.println("Nema nista za pisanje");
							}
						}

						editorModel.insert(text);
					}
				}

			}
		});
	}

	@Override
	public void updateCursorLocation(Location loc) {
		this.cursorLocation.setColumn(loc.getColumn());
		this.cursorLocation.setRow(loc.getRow());
	}

	@Override
	public void updateText() {
		this.editor.repaint();
	}

	private class Editor extends JPanel {
		
		private static final long serialVersionUID = 1L;
		private TextEditor textEditor;
		public Editor(TextEditor te) {
			super();
			this.textEditor = te;
		}
		
		public void paint(Graphics g) {
			super.paint(g);
			Iterator<String> lines = editorModel.allLines();
//			Iterator<String> lines = editorModel.linesRange(1, 2);
			Location cursor = this.textEditor.cursorLocation;
			LocationRange highlightRange = this.textEditor.editorModel.getSelectionRange();

			g.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 20));
			int height = 0;
			int diff = g.getFont().getSize() + 2;

			while(lines.hasNext()) {
				String line = lines.next();
				g.drawString(line, 0, height+=diff);
			}
			
			g.drawLine(cursor.getColumn(), cursor.getRow(), cursor.getColumn(), cursor.getRow() + diff);

			if(highlightRange != null){
				this.highlight(g, highlightRange, diff);
			}
		}

		private void highlight(Graphics g, LocationRange highlighter, int height){
			Location start = highlighter.getStart();
			Location end = highlighter.getEnd();
			int width = Math.abs(start.getColumn() - end.getColumn());
			int heightRow = end.getRow() / height + height;

			g.setColor(Color.gray);
			System.out.println("Highlightam od " + start + " do " + end);

			int endRow = end.getRow() / height;
			int startRow = start.getRow() / height;
			int startCol = start.getColumn();
			int endCol = end.getColumn();

			switch(this.checkOrientation(start, end)){
				case "desno":
					System.out.println("idem udesno");
					g.drawRect(start.getColumn(), start.getRow(), width, heightRow);
					g.fillRect(start.getColumn(), start.getRow(), width, heightRow);
					break;

				case "lijevo":
					System.out.println("idem ulijevo");
					g.drawRect(end.getColumn(), end.getRow(), width, heightRow);
					g.fillRect(end.getColumn(), end.getRow(), width, heightRow);
					break;

				case "dolje":
					System.out.println("idem dolje");
					System.out.println("Retci: " + startRow + ", " + endRow);

					for(int i=startRow; i<=endRow; i++){
						if(i == startRow){
							int line = editorModel.lines.get(i).length();
							g.drawRect(startCol, i*height, line * 12 - startCol, i + height);
							g.fillRect(startCol, i*height, line * 12 - startCol, i + height);
						}
						else if (i == endRow){
							g.drawRect(0, i*height, endCol, i + height);
							g.fillRect(0, i*height, endCol, i + height);
						}
						else {
							int lineLen = editorModel.lines.get(i).length();
							g.drawRect(0, i*height, lineLen * 12, i + height);
							g.fillRect(0, i*height, lineLen * 12, i + height);
						}
					}
					break;

				case "gore":
					System.out.println("Idem gore");
					System.out.println("Retci: " + startRow + ", " + endRow);

					for(int i=startRow; i>=endRow; i--){
						if(i == startRow){
							g.drawRect(0, i*height, startCol, i + height);
							g.fillRect(0, i*height, startCol, i + height);
						}
						else if (i == endRow){
							int line = editorModel.lines.get(i).length();
							g.drawRect(endCol, i*height, line * 12 - endCol, i + height);
							g.fillRect(endCol, i*height, line * 12 - endCol, i + height);
						}
						else {
							int lineLen = editorModel.lines.get(i).length();
							g.drawRect(0, i*height, lineLen * 12, i + height);
							g.fillRect(0, i*height, lineLen * 12, i + height);
						}
					}
			}
		}

		private String checkOrientation(Location start, Location end){
			String action = "";
			if(start.getColumn() < end.getColumn() && start.getRow() == end.getRow()){
				action = "desno";
			}
			else if(start.getColumn() > end.getColumn() && start.getRow() == end.getRow()){
				action = "lijevo";
			}
			else if(start.getRow() < end.getRow()){
				action = "dolje";
			}
			else if(start.getRow() > end.getRow()){
				action = "gore";
			}
			return action;
		}
	}
}
