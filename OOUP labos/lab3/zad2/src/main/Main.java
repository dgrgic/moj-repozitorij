package main;

import javax.swing.SwingUtilities;

import editor.TextEditor;

public class Main {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				initGui();
			}
			
		});
	}
	
	public static void initGui() {
		TextEditor editor = new TextEditor();
	}
}
