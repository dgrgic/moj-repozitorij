package main;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import editor.MyTextEditor;

public class Zad21 {

	public static void main(String[] args) {
//		SwingUtilities.invokeLater(null);		
		init();
	}
	
	public static void init() {
		JFrame frame = new JFrame();
		MyTextEditor editor = new MyTextEditor(300,200);
		
		frame.setLocation(500, 50);
		frame.setSize(300, 200);
		frame.setTitle("Zadatak 2.1!");
		frame.setDefaultCloseOperation(
			WindowConstants.DISPOSE_ON_CLOSE
		);
		frame.setVisible(true);
		frame.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				System.out.println(e.getKeyCode() + " is typed.");
			}

			@Override
			public void keyPressed(KeyEvent e) {
				System.out.println(e.getKeyCode() + " is pressed.");
				frame.setVisible(false);
				frame.dispose();
			}

			@Override
			public void keyReleased(KeyEvent e) {
				System.out.println(e.getKeyCode() + " is released.");	
			}
			
		});
		
		frame.add(editor);
	}
}
