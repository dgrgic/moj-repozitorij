package clipboard;

import observer.ClipboardObserver;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class ClipboardStack {

    private Stack<String> texts;
    private List<ClipboardObserver> observers;

    public ClipboardStack(){
        this.texts = new Stack<>();
        this.observers = new ArrayList<>();
    }

    public void attach(ClipboardObserver c){
        this.observers.add(c);
    }

    public void detach(ClipboardObserver c){
        this.observers.remove(c);
    }

    public void notifyObservers(){
        for(ClipboardObserver c: this.observers){
            c.updateClipboard();
        }
    }

    public void clear(){
        this.texts.clear();
    }

    public void push(String text){
        this.texts.push(text);
    }

    public String pop(){
        return this.texts.pop();
    }

    public boolean isEmpty(){
        return this.texts.isEmpty();
    }

    public String peek(){
        return this.texts.peek();
    }
}
