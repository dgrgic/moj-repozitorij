package observer;

public interface ClipboardObserver {
    boolean updateClipboard();
}
