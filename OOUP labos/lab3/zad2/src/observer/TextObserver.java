package observer;

public interface TextObserver {

	void updateText();
}
