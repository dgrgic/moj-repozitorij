package observer;

import location.Location;

public interface CursorObserver {

	void updateCursorLocation(Location loc);
}
