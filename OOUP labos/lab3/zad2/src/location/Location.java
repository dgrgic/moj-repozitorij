package location;

public class Location {

	private int column; // x
	private int row; // y

	public Location(int x, int y) {
		this.column = x;
		this.row = y;
	}

	public void setColumn(int x) {
		this.column = x;
	}

	public void setRow(int y) {
		this.row = y;
	}

	public int getColumn() {
		return this.column;
	}

	public int getRow() {
		return this.row;
	}

	@Override
	public String toString() {
		return "column = " + this.column + ", row = " + this.row;
	}
}
