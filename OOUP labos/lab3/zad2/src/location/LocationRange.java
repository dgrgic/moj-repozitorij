package location;

public class LocationRange {

	private Location start;
	private Location end;
	
	public LocationRange() {
		this.start = null;
		this.end = null;
	}
	
	public LocationRange(Location start, Location end) {
		this.start = start;
		this.end = end;
	}

	public Location getStart() {
		return start;
	}

	public void setStart(Location start) {
		this.start = start;
	}

	public Location getEnd() {
		return end;
	}

	public void setEnd(Location end) {
		this.end = end;
	}
	
}
