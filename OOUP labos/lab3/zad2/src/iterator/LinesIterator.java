package iterator;

public interface LinesIterator<T> {

	boolean hasNext();
	T next();
}
