package manager;

import action.EditAction;

import java.util.Stack;

public class UndoManager {

    private Stack<EditAction> undoStack = new Stack<>();
    private Stack<EditAction> redoStack = new Stack<>();
//    singleton
    private static UndoManager instance = new UndoManager();

    private UndoManager(){ }

    public static UndoManager getInstance(){
        return instance;
    }

    public void undo(){
        EditAction action = this.undoStack.pop();
        this.redoStack.push(action);
        action.executeUndo();
    }

    public void push(EditAction c){
        this.redoStack.clear();
        this.undoStack.push(c);
    }
}
