package action;

public interface EditAction {
    void executeDo();
    void executeUndo();
}
