package model.Animal;

import java.lang.reflect.Constructor;

public class AnimalFactory {

	@SuppressWarnings("unchecked")
	public static Animal newInstance(String kind, String name) {
		try {
			Class<Animal> clazz = null;
			clazz = (Class<Animal>)Class.forName("model.plugins." + kind);
			Constructor<?> ctr = clazz.getConstructor(String.class);
			Animal animal = (Animal)ctr.newInstance(name);
			return animal;
		
		} catch(Exception e) {
			e.printStackTrace();
		}

		return null;
	}
	
}
