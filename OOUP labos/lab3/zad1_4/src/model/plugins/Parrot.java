package model.plugins;

import model.Animal.Animal;

public class Parrot extends Animal {

	private String parrotName;
	
	
	public Parrot(String name) {
		super();
		this.parrotName = name;
	}

	@Override
	public String name() {
		return this.parrotName;
	}

	@Override
	public String greet() {
		return "Sto mu gromova!";
	}

	@Override
	public String menu() {
		return "brazilske orahe";
	}
	
}
