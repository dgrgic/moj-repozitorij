package model.plugins;

import model.Animal.Animal;

public class Tiger extends Animal {

	private String tigerName;
	
	
	public Tiger(String tigerName) {
		super();
		this.tigerName = tigerName;
	}

	@Override
	public String name() {
		return this.tigerName;
	}

	@Override
	public String greet() {
		return "Mijau!";
	}

	@Override
	public String menu() {
		return "mlako mlijeko";
	}

}
