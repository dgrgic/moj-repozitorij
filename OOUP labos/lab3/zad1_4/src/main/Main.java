package main;

import model.Animal.Animal;
import model.Animal.AnimalFactory;

public class Main {

	public static void main(String[] args) {
		
		Animal p1 = AnimalFactory.newInstance("Parrot", "Modrobradi");
		Animal p2 = AnimalFactory.newInstance("Tiger", "Straško");
		
		if(p1==null || p2==null) {
			return;
		}
		
		p1.animalPrintGreeting();
		p2.animalPrintGreeting();
		
		p1.animalPrintMenu();
		p2.animalPrintMenu();
	}
	
}
