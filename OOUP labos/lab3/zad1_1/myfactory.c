#include "myfactory.h"
#include <dlfcn.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char const* makenameforhandle(char const* s1, char const* s2){
  char *result = malloc(strlen(s1) + strlen(s2) + 1); // +1 for the null-terminator
  // in real code you would check for errors in malloc here
  strcpy(result, s1);
  strcat(result, s2);
  return result;
}

// pokazivac na funkciju koja ne prima nista, a vraca neki string
// koristim za animalPrintGreeting i animalPrintMenu
typedef char const* (*PTRFUN)();

typedef struct{
  PTRFUN* vtable;
  // vtable entries:
  // 0: char const* name(void* this);
  // 1: char const* greet();
  // 2: char const* menu();
} Animal;

// dlsym vraca metodu create, a metoda create prima string(ime) i vraca instancu tipa Animal*
typedef Animal* (*CRFUN)(char const*);

void* myfactory(char const* libname, char const* ctorarg){
  // sad tu trebam pomocu dlopen otvoriti library libname
  // onda iz tog libraryja pozvati funkciju create sa ctorarg

  void* handle;
  CRFUN create_animal;
  char const* realname;
  char* error;

  realname = makenameforhandle("./", libname);
  realname = makenameforhandle(realname, ".so");

  handle = dlopen(realname, RTLD_LAZY);
  if(!handle){
    fputs(dlerror(), stderr);
    exit(1);
  }

  create_animal = dlsym(handle, "create");
  if ((error = dlerror()) != NULL)  {
    fputs(error, stderr);
    exit(1);
  }

  Animal* animal = create_animal(ctorarg);

  return animal;

  dlclose(handle);

}
