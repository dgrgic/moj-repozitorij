#include <stdio.h>
#include <stdlib.h>
#include "myfactory.h"

typedef char const* (*PTRFUN)();

typedef struct{
  PTRFUN* vtable;
  // vtable entries:
  // 0: char const* name(void* this);
  // 1: char const* greet();
  // 2: char const* menu();
} Animal;

void animalPrintGreeting(Animal* this){
  printf("%s pozdravlja: %s\n", this->vtable[0](this), this->vtable[1]());
}

void animalPrintMenu(Animal* this){
  printf("%s voli: %s\n", this->vtable[0](this), this->vtable[2]());
}

int main(){
  Animal* p1=(Animal*)myfactory("parrot", "Modrobradi");
  Animal* p2=(Animal*)myfactory("tiger", "Straško");
  if (!p1 || !p2){
    printf("Creation of plug-in objects failed.\n");
    exit(1);
  }

  animalPrintGreeting(p1);//"Sto mu gromova!"
  animalPrintGreeting(p2);//"Mijau!"

  animalPrintMenu(p1);//"brazilske orahe"
  animalPrintMenu(p2);//"mlako mlijeko"

  free(p1); free(p2);
}
