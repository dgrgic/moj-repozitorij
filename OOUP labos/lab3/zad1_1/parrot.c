#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef char const* (*PTRFUN)();

// treba mi struktura parrot radi imena
// također ovo je library za stvaranje konkretnog Animal-a tj Parrot
typedef struct {
  PTRFUN* vtable;
  char const* name;
} Parrot;

typedef struct {
	PTRFUN* vtable;
	// vtable entries:
	// 0: char const* name(void* this);
	// 1: char const* greet();
	// 2: char const* menu();
} Animal;


char const* name(void* this){
  return ((Parrot*)this)->name;
}

char const* greet(){
  return "Sto mu gromova!";
}

char const* menu(){
  return "brazilske orahe";
}

PTRFUN parrotfunctions[3] = {
  (PTRFUN) name,
  (PTRFUN) greet,
  (PTRFUN) menu
};

void constructParrot(Parrot* p, char const* name){
    p->vtable = parrotfunctions;
    p->name = name;
}

void* create(char const* name){
  Parrot* p = malloc(sizeof(Parrot));
  constructParrot(p, name);
  return p;
}
