#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef char const* (*PTRFUN)();

// treba mi struktura parrot radi imena
// također ovo je library za stvaranje konkretnog Animal-a tj Tiger
typedef struct {
  PTRFUN* vtable;
  char const* name;
} Tiger;

typedef struct {
	PTRFUN* vtable;
	// vtable entries:
	// 0: char const* name(void* this);
	// 1: char const* greet();
	// 2: char const* menu();
} Animal;


char const* name(void* this){
  return ((Tiger*)this)->name;
}

char const* greet(){
  return "Mijau!";
}

char const* menu(){
  return "mlako mlijeko";
}

PTRFUN tigerfunctions[3] = {
  (PTRFUN) name,
  (PTRFUN) greet,
  (PTRFUN) menu
};

void constructTiger(Tiger* t, char const* name){
    t->vtable = tigerfunctions;
    t->name = name;
}

void* create(char const* name){
  Tiger* t = malloc(sizeof(Tiger));
  constructTiger(t, name);
  return t;
}
