import time
import random
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

neighbours = {
    'l': 0 if rank == size - 1 else rank + 1,
    'r': size - 1 if rank == 0 else rank - 1
}

forks = {
    'l': 'd' if rank < size - 1 else ' ',
    'r': 'd' if rank == 0 else ' '
}

# listener dict is used to mimic Iprobe function
listener = {}
for side in "lr":
    listener[side] = comm.irecv(source=neighbours[side])

# requests is a set of memorized actions for each philosopher
requests = set()


# philosopher either recieves a fork or sends a fork
def do_something(side, action):

    # if someone needs a fork
    if action == "getfork":
        if forks[side] == 'd':
            comm.isend('c', dest=neighbours[side])
            forks[side] = ' '
            print('\t' * rank + f"sent {side} " + f"[{forks['l']},{forks['r']}]")

        # if there is no fork or fork is clean, memorize request
        else:
            requests.add(side)

    # if recieved clean fork
    elif action == "c":
        forks[side] = action
        print('\t' * rank + f"got {side} " + f"[{forks['l']},{forks['r']}]")


def check_incoming_fork_requests():
    # first serve memorized requests from neighbours
    for r in requests:
        do_something(r, "getfork")
    requests.clear()

    # then check if someone sent you a clean fork
    for side in "lr":
        flag, data = listener[side].test()
        if flag:
            do_something(side, data)
            # update listener
            listener[side] = comm.irecv(source=neighbours[side])


def think():
    seconds = random.randint(2, 10)
    print('\t' * rank + "think" + f"[{forks['l']},{forks['r']}]")
    for i in range(seconds):
        # while thinking check for requests and serve memorized requests if there are any
        check_incoming_fork_requests()
        time.sleep(0.5)


def get_forks():
    while forks['l'] == ' ' or forks['r'] == ' ':
        for side in "lr":
            if forks[side] == ' ':
                comm.isend("getfork", dest=neighbours[side])
                print('\t' * rank + f"req {side} " + f"[{forks['l']},{forks['r']}]")

        # while still waiting for forks, check for requests and serve memorized requests if there are any
        check_incoming_fork_requests()
        time.sleep(1)


def eat():
    print('\t' * rank + "eating" + f"[{forks['l']},{forks['r']}]")
    time.sleep(3)
    # after eating, all forks are dirty
    for key in forks:
        forks[key] = 'd'
    print('\t' * rank + "finished" + f"[{forks['l']},{forks['r']}]")


while True:
    think()
    get_forks()
    eat()
