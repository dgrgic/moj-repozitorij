﻿using SwimmingSchool.Helpers;
using SwimmingSchool.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Windows.Forms;
using Microsoft.EntityFrameworkCore.Internal;

namespace SwimmingSchool
{
    public partial class Registracija : Form
    {
        public Registracija()
        {
            InitializeComponent();
        }
        
        private void btnRegistriraj_Click(object sender, EventArgs e)
        {
            String ime = tbIme.Text;
            String prezime = tbPrezime.Text;
            String email = tbEmail.Text;
            String korisnickoIme = tbKorisnickoIme.Text;
            String lozinka = tbLozinka.Text;
            String potvrdaLozinke = tbPotvrdi.Text;
            Boolean trener = rbtnTrener.Checked;
            Boolean roditelj = rbtnRoditelj.Checked;

            // sad pozivam validator da provjeri sve

            using (var db = new SwimmingSchoolDbContext())
            {

                var uloge = db.Uloga.ToList();
                Console.WriteLine("Broj uloga:" + uloge.Count);

                db.Korisnik.Add(new Korisnik()
                {
                    Ime = ime,
                    Prezime = prezime,
                    Email = email,
                    KorisnickoIme = korisnickoIme,
                    Lozinka = lozinka,
                    IdUloga = 1L
                });

                db.SaveChanges();
            }
        }

        private void rbtnTrener_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            Console.WriteLine("Trener " + rb.Checked);
        }

        private void rbtnRoditelj_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            Console.WriteLine("Roditelj " + rb.Checked);
        }
    }
}
