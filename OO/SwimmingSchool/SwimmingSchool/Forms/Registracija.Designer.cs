﻿namespace SwimmingSchool
{
    partial class Registracija
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbIme = new System.Windows.Forms.Label();
            this.lbPrezime = new System.Windows.Forms.Label();
            this.lbEmail = new System.Windows.Forms.Label();
            this.lbKorisnickoIme = new System.Windows.Forms.Label();
            this.tbKorisnickoIme = new System.Windows.Forms.TextBox();
            this.tbIme = new System.Windows.Forms.TextBox();
            this.tbEmail = new System.Windows.Forms.TextBox();
            this.tbPrezime = new System.Windows.Forms.TextBox();
            this.rbtnTrener = new System.Windows.Forms.RadioButton();
            this.btnRegistriraj = new System.Windows.Forms.Button();
            this.tbPotvrdi = new System.Windows.Forms.TextBox();
            this.tbLozinka = new System.Windows.Forms.TextBox();
            this.lbPotvrdi = new System.Windows.Forms.Label();
            this.lbLozinka = new System.Windows.Forms.Label();
            this.gbUloga = new System.Windows.Forms.GroupBox();
            this.rbtnRoditelj = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.gbUloga.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbIme);
            this.groupBox1.Controls.Add(this.lbPrezime);
            this.groupBox1.Controls.Add(this.lbEmail);
            this.groupBox1.Controls.Add(this.lbKorisnickoIme);
            this.groupBox1.Controls.Add(this.tbKorisnickoIme);
            this.groupBox1.Controls.Add(this.tbIme);
            this.groupBox1.Controls.Add(this.tbEmail);
            this.groupBox1.Controls.Add(this.tbPrezime);
            this.groupBox1.Location = new System.Drawing.Point(103, 73);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(299, 196);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Osobni podaci";
            // 
            // lbIme
            // 
            this.lbIme.AutoSize = true;
            this.lbIme.Location = new System.Drawing.Point(27, 46);
            this.lbIme.Name = "lbIme";
            this.lbIme.Size = new System.Drawing.Size(30, 17);
            this.lbIme.TabIndex = 16;
            this.lbIme.Text = "Ime";
            // 
            // lbPrezime
            // 
            this.lbPrezime.AutoSize = true;
            this.lbPrezime.Location = new System.Drawing.Point(27, 86);
            this.lbPrezime.Name = "lbPrezime";
            this.lbPrezime.Size = new System.Drawing.Size(59, 17);
            this.lbPrezime.TabIndex = 17;
            this.lbPrezime.Text = "Prezime";
            // 
            // lbEmail
            // 
            this.lbEmail.AutoSize = true;
            this.lbEmail.Location = new System.Drawing.Point(27, 123);
            this.lbEmail.Name = "lbEmail";
            this.lbEmail.Size = new System.Drawing.Size(42, 17);
            this.lbEmail.TabIndex = 18;
            this.lbEmail.Text = "Email";
            // 
            // lbKorisnickoIme
            // 
            this.lbKorisnickoIme.AutoSize = true;
            this.lbKorisnickoIme.Location = new System.Drawing.Point(27, 161);
            this.lbKorisnickoIme.Name = "lbKorisnickoIme";
            this.lbKorisnickoIme.Size = new System.Drawing.Size(99, 17);
            this.lbKorisnickoIme.TabIndex = 19;
            this.lbKorisnickoIme.Text = "Korisnicko ime";
            // 
            // tbKorisnickoIme
            // 
            this.tbKorisnickoIme.Location = new System.Drawing.Point(178, 157);
            this.tbKorisnickoIme.Name = "tbKorisnickoIme";
            this.tbKorisnickoIme.Size = new System.Drawing.Size(100, 22);
            this.tbKorisnickoIme.TabIndex = 26;
            // 
            // tbIme
            // 
            this.tbIme.Location = new System.Drawing.Point(178, 41);
            this.tbIme.Name = "tbIme";
            this.tbIme.Size = new System.Drawing.Size(100, 22);
            this.tbIme.TabIndex = 23;
            // 
            // tbEmail
            // 
            this.tbEmail.Location = new System.Drawing.Point(178, 118);
            this.tbEmail.Name = "tbEmail";
            this.tbEmail.Size = new System.Drawing.Size(100, 22);
            this.tbEmail.TabIndex = 25;
            // 
            // tbPrezime
            // 
            this.tbPrezime.Location = new System.Drawing.Point(178, 81);
            this.tbPrezime.Name = "tbPrezime";
            this.tbPrezime.Size = new System.Drawing.Size(100, 22);
            this.tbPrezime.TabIndex = 24;
            // 
            // rbtnTrener
            // 
            this.rbtnTrener.AutoSize = true;
            this.rbtnTrener.Location = new System.Drawing.Point(30, 62);
            this.rbtnTrener.Name = "rbtnTrener";
            this.rbtnTrener.Size = new System.Drawing.Size(72, 21);
            this.rbtnTrener.TabIndex = 0;
            this.rbtnTrener.TabStop = true;
            this.rbtnTrener.Text = "Trener";
            this.rbtnTrener.UseVisualStyleBackColor = true;
            this.rbtnTrener.CheckedChanged += new System.EventHandler(this.rbtnTrener_CheckedChanged);
            // 
            // btnRegistriraj
            // 
            this.btnRegistriraj.Location = new System.Drawing.Point(466, 291);
            this.btnRegistriraj.Name = "btnRegistriraj";
            this.btnRegistriraj.Size = new System.Drawing.Size(311, 124);
            this.btnRegistriraj.TabIndex = 29;
            this.btnRegistriraj.Text = "Registriraj";
            this.btnRegistriraj.UseVisualStyleBackColor = true;
            this.btnRegistriraj.Click += new System.EventHandler(this.btnRegistriraj_Click);
            // 
            // tbPotvrdi
            // 
            this.tbPotvrdi.Location = new System.Drawing.Point(169, 70);
            this.tbPotvrdi.Name = "tbPotvrdi";
            this.tbPotvrdi.Size = new System.Drawing.Size(100, 22);
            this.tbPotvrdi.TabIndex = 28;
            this.tbPotvrdi.UseSystemPasswordChar = true;
            // 
            // tbLozinka
            // 
            this.tbLozinka.Location = new System.Drawing.Point(169, 36);
            this.tbLozinka.Name = "tbLozinka";
            this.tbLozinka.Size = new System.Drawing.Size(100, 22);
            this.tbLozinka.TabIndex = 27;
            this.tbLozinka.UseSystemPasswordChar = true;
            // 
            // lbPotvrdi
            // 
            this.lbPotvrdi.AutoSize = true;
            this.lbPotvrdi.Location = new System.Drawing.Point(6, 75);
            this.lbPotvrdi.Name = "lbPotvrdi";
            this.lbPotvrdi.Size = new System.Drawing.Size(100, 17);
            this.lbPotvrdi.TabIndex = 22;
            this.lbPotvrdi.Text = "Potvrdi lozinku";
            // 
            // lbLozinka
            // 
            this.lbLozinka.AutoSize = true;
            this.lbLozinka.Location = new System.Drawing.Point(6, 41);
            this.lbLozinka.Name = "lbLozinka";
            this.lbLozinka.Size = new System.Drawing.Size(57, 17);
            this.lbLozinka.TabIndex = 21;
            this.lbLozinka.Text = "Lozinka";
            // 
            // gbUloga
            // 
            this.gbUloga.Controls.Add(this.rbtnRoditelj);
            this.gbUloga.Controls.Add(this.rbtnTrener);
            this.gbUloga.Location = new System.Drawing.Point(103, 291);
            this.gbUloga.Name = "gbUloga";
            this.gbUloga.Size = new System.Drawing.Size(299, 124);
            this.gbUloga.TabIndex = 30;
            this.gbUloga.TabStop = false;
            this.gbUloga.Text = "Uloga";
            // 
            // rbtnRoditelj
            // 
            this.rbtnRoditelj.AutoSize = true;
            this.rbtnRoditelj.Location = new System.Drawing.Point(178, 62);
            this.rbtnRoditelj.Name = "rbtnRoditelj";
            this.rbtnRoditelj.Size = new System.Drawing.Size(76, 21);
            this.rbtnRoditelj.TabIndex = 1;
            this.rbtnRoditelj.TabStop = true;
            this.rbtnRoditelj.Text = "Roditelj";
            this.rbtnRoditelj.UseVisualStyleBackColor = true;
            this.rbtnRoditelj.CheckedChanged += new System.EventHandler(this.rbtnRoditelj_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lbLozinka);
            this.groupBox3.Controls.Add(this.lbPotvrdi);
            this.groupBox3.Controls.Add(this.tbLozinka);
            this.groupBox3.Controls.Add(this.tbPotvrdi);
            this.groupBox3.Location = new System.Drawing.Point(460, 73);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(317, 134);
            this.groupBox3.TabIndex = 31;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Lozinka";
            // 
            // Registracija
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(887, 479);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.gbUloga);
            this.Controls.Add(this.btnRegistriraj);
            this.Controls.Add(this.groupBox1);
            this.Name = "Registracija";
            this.Text = "Registracija";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbUloga.ResumeLayout(false);
            this.gbUloga.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lbIme;
        private System.Windows.Forms.Label lbPrezime;
        private System.Windows.Forms.Label lbEmail;
        private System.Windows.Forms.Label lbKorisnickoIme;
        private System.Windows.Forms.TextBox tbKorisnickoIme;
        private System.Windows.Forms.TextBox tbIme;
        private System.Windows.Forms.TextBox tbEmail;
        private System.Windows.Forms.TextBox tbPrezime;
        private System.Windows.Forms.RadioButton rbtnTrener;
        private System.Windows.Forms.Button btnRegistriraj;
        private System.Windows.Forms.TextBox tbPotvrdi;
        private System.Windows.Forms.TextBox tbLozinka;
        private System.Windows.Forms.Label lbPotvrdi;
        private System.Windows.Forms.Label lbLozinka;
        private System.Windows.Forms.GroupBox gbUloga;
        private System.Windows.Forms.RadioButton rbtnRoditelj;
        private System.Windows.Forms.GroupBox groupBox3;
    }
}

