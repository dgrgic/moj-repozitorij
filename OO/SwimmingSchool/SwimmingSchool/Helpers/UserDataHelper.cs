﻿namespace SwimmingSchool.Helpers
{
    static class UserDataHelper
    {
        private static int? _userId;

        public static void SetUserId(int? id)
        {
            _userId = id;
        }

        public static int? GetUserId()
        {
            return _userId;
        }
    }
}
