﻿namespace SwimmingSchool.Models
{
    public class Obavijest
    {
        public long Id { get; set; }
        public string Naslov { get; set; }
        public string Tijelo { get; set; }
        public string Datum { get; set; }
        public long IdKorisnik { get; set; }

        public Korisnik Korisnik { get; set; }
    }
}
