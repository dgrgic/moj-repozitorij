﻿namespace SwimmingSchool.Models
{
    public class KorisnikTrening
    {
        public long Id { get; set; }
        public long IdKorisnik { get; set; }
        public long IdTrening { get; set; }

        public Korisnik Korisnik { get; set; }
        public Trening Trening { get; set; }
    }
}
