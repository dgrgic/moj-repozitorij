﻿using System.Collections.Generic;

namespace SwimmingSchool.Models
{
    public class Trening
    {
        public Trening()
        {
            KorisnikTrening = new HashSet<KorisnikTrening>();
        }

        public long Id { get; set; }
        public string Datum { get; set; }
        public long Odradeno { get; set; }
        public long IdPlivac { get; set; }

        public Plivac Plivac { get; set; }
        public ICollection<KorisnikTrening> KorisnikTrening { get; set; }
    }
}
