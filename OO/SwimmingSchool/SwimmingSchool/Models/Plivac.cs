﻿using System.Collections.Generic;

namespace SwimmingSchool.Models
{
    public class Plivac
    {
        public Plivac()
        {
            KorisnikPlivac = new HashSet<KorisnikPlivac>();
            Trening = new HashSet<Trening>();
        }

        public long Id { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public long IdGrupa { get; set; }

        public Grupa Grupa { get; set; }
        public ICollection<KorisnikPlivac> KorisnikPlivac { get; set; }
        public ICollection<Trening> Trening { get; set; }
    }
}
