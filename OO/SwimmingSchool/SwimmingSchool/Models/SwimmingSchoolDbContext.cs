﻿using Microsoft.EntityFrameworkCore;

namespace SwimmingSchool.Models
{
    public class SwimmingSchoolDbContext : DbContext
    {
        public SwimmingSchoolDbContext()
        {
        }

        public SwimmingSchoolDbContext(DbContextOptions<SwimmingSchoolDbContext> options)
            : base(options)
        {
        }

        public DbSet<Grupa> Grupa { get; set; }
        public DbSet<Korisnik> Korisnik { get; set; }
        public DbSet<KorisnikPlivac> KorisnikPlivac { get; set; }
        public DbSet<KorisnikTrening> KorisnikTrening { get; set; }
        public DbSet<Obavijest> Obavijest { get; set; }
        public DbSet<Plivac> Plivac { get; set; }
        public DbSet<Trening> Trening { get; set; }
        public DbSet<Uloga> Uloga { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlite("DataSource=|DataDirectory|SwimmingSchoolDb.db");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Grupa>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasColumnType("varchar(30)");
            });

            modelBuilder.Entity<Korisnik>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnType("varchar(30)");

                entity.Property(e => e.Ime)
                    .IsRequired()
                    .HasColumnType("varchar(30)");

                entity.Property(e => e.KorisnickoIme)
                    .IsRequired()
                    .HasColumnType("varchar(30)");

                entity.Property(e => e.Lozinka)
                    .IsRequired()
                    .HasColumnType("varchar(30)");

                entity.Property(e => e.Prezime)
                    .IsRequired()
                    .HasColumnType("varchar(30)");

                entity.HasOne(d => d.Uloga)
                    .WithMany(p => p.Korisnik)
                    .HasForeignKey(d => d.IdUloga)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<KorisnikPlivac>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.IdKorisnik).HasColumnName("idKorisnik");

                entity.Property(e => e.IdPlivac).HasColumnName("idPlivac");

                entity.HasOne(d => d.Korisnik)
                    .WithMany(p => p.KorisnikPlivac)
                    .HasForeignKey(d => d.IdKorisnik)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Plivac)
                    .WithMany(p => p.KorisnikPlivac)
                    .HasForeignKey(d => d.IdPlivac)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<KorisnikTrening>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.IdKorisnik).HasColumnName("idKorisnik");

                entity.Property(e => e.IdTrening).HasColumnName("idTrening");

                entity.HasOne(d => d.Korisnik)
                    .WithMany(p => p.KorisnikTrening)
                    .HasForeignKey(d => d.IdKorisnik)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Trening)
                    .WithMany(p => p.KorisnikTrening)
                    .HasForeignKey(d => d.IdTrening)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Obavijest>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Datum)
                    .IsRequired()
                    .HasColumnType("varchar(30)");

                entity.Property(e => e.Naslov)
                    .IsRequired()
                    .HasColumnType("varchar(30)");

                entity.Property(e => e.Tijelo)
                    .IsRequired()
                    .HasColumnType("varchar(70)");

                entity.HasOne(d => d.Korisnik)
                    .WithMany(p => p.Obavijest)
                    .HasForeignKey(d => d.IdKorisnik)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Obavijest_Korisnik");
            });

            modelBuilder.Entity<Plivac>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Ime)
                    .IsRequired()
                    .HasColumnType("varchar(30)");

                entity.Property(e => e.Prezime)
                    .IsRequired()
                    .HasColumnType("varchar(30)");

                entity.HasOne(d => d.Grupa)
                    .WithMany(p => p.Plivac)
                    .HasForeignKey(d => d.IdGrupa)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Trening>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Datum)
                    .IsRequired()
                    .HasColumnType("varchar(30)");

                entity.HasOne(d => d.Plivac)
                    .WithMany(p => p.Trening)
                    .HasForeignKey(d => d.IdPlivac)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Uloga>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasColumnType("varchar(30)");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        void OnModelCreatingPartial(ModelBuilder modelBuilder)
        {
            //throw new System.NotImplementedException();
        }
    }
}
