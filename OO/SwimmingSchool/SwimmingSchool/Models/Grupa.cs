﻿using System;
using System.Collections.Generic;

namespace SwimmingSchool.Models
{
    public class Grupa
    {
        public Grupa()
        {
            Plivac = new HashSet<Plivac>();
        }

        public long Id { get; set; }
        public string Naziv { get; set; }

        public ICollection<Plivac> Plivac { get; set; }
    }
}
