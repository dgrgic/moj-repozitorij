﻿namespace SwimmingSchool.Models
{
    public class KorisnikPlivac
    {
        public long Id { get; set; }
        public long IdKorisnik { get; set; }
        public long IdPlivac { get; set; }
        public long Potvrdeno { get; set; }

        public Korisnik Korisnik { get; set; }
        public Plivac Plivac { get; set; }
    }
}
