﻿using System.Collections.Generic;

namespace SwimmingSchool.Models
{
    public class Korisnik
    {
        public Korisnik()
        {
            KorisnikPlivac = new HashSet<KorisnikPlivac>();
            KorisnikTrening = new HashSet<KorisnikTrening>();
            Obavijest = new HashSet<Obavijest>();
        }

        public long Id { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string Email { get; set; }
        public string KorisnickoIme { get; set; }
        public long IdUloga { get; set; }
        public string Lozinka { get; set; }
        public long Potvrden { get; set; }

        public Uloga Uloga { get; set; }
        public ICollection<KorisnikPlivac> KorisnikPlivac { get; set; }
        public ICollection<KorisnikTrening> KorisnikTrening { get; set; }
        public ICollection<Obavijest> Obavijest { get; set; }
    }
}
