﻿using System.Collections.Generic;

namespace SwimmingSchool.Models
{
    public class Uloga
    {
        public Uloga()
        {
            Korisnik = new HashSet<Korisnik>();
        }

        public long Id { get; set; }
        public string Naziv { get; set; }

        public ICollection<Korisnik> Korisnik { get; set; }
    }
}
