﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PrvaDomacaZadaca_Kalkulator
{
    public class Factory
    {
        public static ICalculator CreateCalculator()
        {
            // vratiti kalkulator
            return new Kalkulator();
        }
    }

    public class Util
    {
        internal static double SolveEquation(List<object> equation)
        {
            foreach(var o in equation)
            {
                Console.Write(o + ", ");
            }
            double sum = 0.0;
            // ovdje racunam sve uneseno
            if (equation.Count == 0)
            {
                return sum;
            }
            
            else if(equation.Count == 2)
            {
                string action = equation[equation.Count - 1].ToString();
                double nmbr = 0.0;
                switch (action)
                {
                    case "+":
                        nmbr = Double.Parse(equation[0].ToString());
                        sum = nmbr + nmbr;
                        break;

                    case "-":
                        nmbr = Double.Parse(equation[0].ToString());
                        sum = nmbr + nmbr;
                        break;

                    case "*":
                        nmbr = Double.Parse(equation[0].ToString());
                        sum = nmbr + nmbr;
                        break;

                    case "/":
                        nmbr = Double.Parse(equation[0].ToString());
                        sum = nmbr + nmbr;
                        break;
                }
            }
            else
            {
                for (int i = 0; i < equation.Count; i++)
                {
                    if (equation[i].ToString() == "+")
                    {
                        if (i + 1 <= equation.Count - 1)
                        {
                            sum = sum + Double.Parse(equation[i + 1].ToString());
                            equation.Remove(equation[i + 1]);
                        }
                    }
                    else if (equation[i].ToString() == "-")
                    {
                        if (i + 1 <= equation.Count - 1)
                        {
                            sum = sum - Double.Parse(equation[i + 1].ToString());
                            equation.Remove(equation[i + 1]);
                        }
                    }
                    else if (equation[i].ToString() == "*")
                    {
                        if (i + 1 <= equation.Count - 1)
                        {
                            sum = sum * Double.Parse(equation[i + 1].ToString());
                            equation.Remove(equation[i + 1]);
                        }
                    }
                    else if (equation[i].ToString() == "/")
                    {
                        if (i + 1 <= equation.Count - 1)
                        {
                            sum = sum / Double.Parse(equation[i + 1].ToString());
                            equation.Remove(equation[i + 1]);
                        }
                    }
                    else
                    {
                        sum = Double.Parse(equation[i].ToString());
                    }
                }
            }

            return Math.Round(sum, 9);
        }

        internal static double SolveInverse(string screen)
        {
            return Math.Round(1 / Double.Parse(screen), 9);
        }

        internal static double SolveRoot(string screen)
        {
            // racunam korijen prethodno unesenog broja
            return Math.Round(Math.Sqrt(Double.Parse(screen)), 9);
        }

        internal static double SolveQuadrat(string screen)
        {
            // racunam kvadrat prethodno unesenog broja
            return Math.Round(Double.Parse(screen) * Double.Parse(screen), 9);
        }

        internal static double SolveTangens(string screen)
        {
            // racunam tangens prethodno unesenog broja
            return Math.Round(Math.Tan(Double.Parse(screen)), 9);
        }

        internal static double SolveCosinus(string screen)
        {
            // racunam kosinus prethodno unesenog broja
            return Math.Round(Math.Cos(Double.Parse(screen)), 9);
        }

        internal static double SolveSinus(string screen)
        {
            // racunam sinus prethodno unesenog broja
            return Math.Round(Math.Sin(Double.Parse(screen)), 9);
        }

        internal static string fillDisplay(List<char> screen)
        {
            StringBuilder display = new StringBuilder();
            screen.ForEach(e =>
            {
                display.Append(e);
            });

            return display.ToString();
        }

        // zovem samo prije operacija (binarne, unarne)
        internal static List<char> CheckComma(List<char> screen)
        {
            if (screen.Contains(','))
            {
                if(screen[screen.Count-1] == ',')
                {
                    screen.Remove(screen[screen.Count - 1]);
                    return screen;
                }
                else
                {
                    for(int i=screen.Count-1; i>screen.IndexOf(','); i--)
                    {
                        if(screen[i] == '0')
                        {
                            screen.Remove(screen[i]);
                        }
                        else
                        {
                            break;
                        }
                    }

                    if (screen[screen.Count - 1] == ',')
                    {
                        screen.Remove(screen[screen.Count - 1]);
                    }

                    return screen;

                }
            }
            return screen;
        }

        internal static List<char> AddMinus(List<char> screen)
        {
            screen.Add('-');
            for(int i=screen.Count-1; i>0; i--)
            {
                char tmp = screen[i];
                screen[i] = screen[i - 1];
                screen[i - 1] = tmp;
            }

            return screen;
        }

        internal static string StoreMultiple(double number, string screen)
        {
            StringBuilder sb = new StringBuilder();
            foreach(char c in number.ToString())
            {
                sb.Append(c);
            }
            foreach (char c in screen)
            {
                sb.Append(c);
            }
            return sb.ToString();
        }

        internal static List<object> MultipleOperators(List<object> equation, string action)
        {
            string last = equation[equation.Count - 1].ToString();
            if (last == "+" || last == "-" || last == "*" || last == "/")
            {
                equation[equation.Count - 1] = action;
            }
            return equation;
        }

        //internal static List<object> BinaryUnary(List<object> equation, char inPressedDigit)
        //{
        //    switch (inPressedDigit)
        //    {
        //        case 'M':
        //            string previous = equation[equation.Count - 2].ToString();

        //    }
        //}
    }

    public class Kalkulator:ICalculator
    {
        private List<char> screen; // kako se unose podaci

        private String display; // lista rezultata, prikazuje se samo zadnji (unos + M, S, K, T, Q, R, I)
                                      // rezultat se racuna kod unosa binarnog (+, -, *, /) i sprema

        // treba
        private List<object> equation; // cijela jednadzba koju util rjesava (nakon =)
        private double numberInMemory; // broj u memoriji, nakon P gleda se cijeli rezultat od pocetka

        public Kalkulator()
        {
            screen = new List<char>();
            screen.Add('0');
            display = Util.fillDisplay(screen);
            equation = new List<object>();
            numberInMemory = 0.0;
        }

        public void Press(char inPressedDigit)
        {
            // ovdje primam char iz konzole i ubacujem u listu znakova (max 10 znakova, inace se zaokruzuje)
            if (screen.Count > 10)
            {
                // vise od 10 znamenki
                if(screen.Contains(',') || screen.Contains('-'))
                {
                    switch(inPressedDigit)
                    {
                        case 'M':
                        screen = Util.CheckComma(screen);
                        screen = Util.AddMinus(screen);
                        display = Util.fillDisplay(screen);
                        break;

                        case 'P':
                            // spremi u memoriju broj
                            screen = Util.CheckComma(screen);
                            display = Util.fillDisplay(screen);
                            if (numberInMemory != 0.0)
                            {
                                display = Util.StoreMultiple(numberInMemory, display);
                                numberInMemory = Double.Parse(display);
                            }
                            else
                            {
                                numberInMemory = Double.Parse(display);
                            }
                            screen.Clear();
                            break;

                        case 'G':
                            // dohvati broj iz memorije
                            display = numberInMemory.ToString();
                            break;

                        case 'C':
                            // unesen je C (clear) ne brise se memorija niti prethodni rezultati
                            screen.Clear();
                            screen.Add('0');
                            display = Util.fillDisplay(screen);
                            break;

                        case 'O':
                            // unesen je O (off) brise sve
                            screen.Clear();
                            screen.Add('0');
                            numberInMemory = 0;
                            display = Util.fillDisplay(screen);
                            equation.Clear();
                            // mozda jos neka provjera (akcija)
                            break;

                        case 'I':
                            // inverz izraza
                            // ovdje radim nesto krivo
                            if (screen.Count == 1 && screen[0] == '0')
                            {
                                display = "-E-";
                                break;
                            }
                            // uvijek pozivam checkComma
                            screen = Util.CheckComma(screen);
                            display = Util.fillDisplay(screen);
                            double number = Util.SolveInverse(display);
                            display = number.ToString();
                            screen.Clear();
                            foreach (char c in display)
                            {
                                screen.Add(c);
                            }
                            break;

                        case 'R':
                            // korjenovanje izraza
                            screen = Util.CheckComma(screen);
                            display = Util.fillDisplay(screen);
                            number = Util.SolveRoot(display);
                            display = number.ToString();
                            screen.Clear();
                            foreach (char c in display)
                            {
                                screen.Add(c);
                            }
                            break;

                        case 'Q':
                            // kvadriranje izraza
                            screen = Util.CheckComma(screen);
                            display = Util.fillDisplay(screen);
                            number = Util.SolveQuadrat(display);
                            display = number.ToString();
                            screen.Clear();
                            foreach (char c in display)
                            {
                                screen.Add(c);
                            }
                            break;

                        case 'T':
                            // tangens izraza
                            screen = Util.CheckComma(screen);
                            display = Util.fillDisplay(screen);
                            number = Util.SolveTangens(display);
                            display = number.ToString();
                            screen.Clear();
                            foreach (char c in display)
                            {
                                screen.Add(c);
                            }
                            break;

                        case 'K':
                            // kosinus izraza
                            screen = Util.CheckComma(screen);
                            display = Util.fillDisplay(screen);
                            number = Util.SolveCosinus(display);
                            display = number.ToString();
                            screen.Clear();
                            foreach (char c in display)
                            {
                                screen.Add(c);
                            }
                            break;

                        case 'S':
                            // sinus izraza
                            screen = Util.CheckComma(screen);
                            display = Util.fillDisplay(screen);
                            number = Util.SolveSinus(display);
                            display = number.ToString();
                            screen.Clear();
                            foreach (char c in display)
                            {
                                screen.Add(c);
                            }
                            break;

                        case '+':
                            screen = Util.CheckComma(screen);
                            display = Util.fillDisplay(screen);
                            equation.Add(Double.Parse(display));
                            equation.Add(inPressedDigit);
                            screen.Clear();
                            break;

                        case '-':
                            screen = Util.CheckComma(screen);
                            display = Util.fillDisplay(screen);
                            equation.Add(Double.Parse(display));
                            equation.Add(inPressedDigit);
                            screen.Clear();
                            break;

                        case '*':
                            screen = Util.CheckComma(screen);
                            display = Util.fillDisplay(screen);
                            equation.Add(Double.Parse(display));
                            equation.Add(inPressedDigit);
                            screen.Clear();
                            break;

                        case '/':
                            screen = Util.CheckComma(screen);
                            display = Util.fillDisplay(screen);
                            equation.Add(Double.Parse(display));
                            equation.Add(inPressedDigit);
                            screen.Clear();
                            break;


                        case '=':
                            screen = Util.CheckComma(screen);
                            display = Util.fillDisplay(screen);

                            equation.Add(Double.Parse(display));
                            screen.Clear();
                            display = Util.SolveEquation(equation).ToString();
                            if (display.Length > 10 && !display.Contains(",") && !display.Contains("-"))
                            {
                                display = "-E-";
                            }
                            break;
                    }
                }
            }
            
            else
            {
                // nema 10 znamenki
                if(inPressedDigit >= '1' && inPressedDigit <= '9')
                {
                    if (screen.Count == 1 && screen[0] == '0')
                    {
                        screen.Clear();
                        screen.Add(inPressedDigit);
                        display = Util.fillDisplay(screen);
                    }
                    else
                    {
                        screen.Add(inPressedDigit);
                        display = Util.fillDisplay(screen);
                    }
                    
                }
                else
                {
                    switch (inPressedDigit)
                    {
                        case '+':
                            if (screen.Count == 0)
                            {
                                equation = Util.MultipleOperators(equation, "+");
                                break;
                            }
                            screen = Util.CheckComma(screen);
                            display = Util.fillDisplay(screen);
                            equation.Add(Double.Parse(display));
                            equation.Add(inPressedDigit);
                            screen.Clear();
                            break;

                        case '-':
                            if (screen.Count == 0)
                            {
                                equation = Util.MultipleOperators(equation, "-");
                                break;
                            }
                            screen = Util.CheckComma(screen);
                            display = Util.fillDisplay(screen);
                            equation.Add(Double.Parse(display));
                            equation.Add(inPressedDigit);
                            screen.Clear();
                            break;

                        case '*':
                            if (screen.Count == 0)
                            {
                                equation = Util.MultipleOperators(equation, "*");
                                break;
                            }
                            screen = Util.CheckComma(screen);
                            display = Util.fillDisplay(screen);
                            equation.Add(Double.Parse(display));
                            equation.Add(inPressedDigit);
                            screen.Clear();
                            break;

                        case '/':
                            if (screen.Count == 0)
                            {
                                equation = Util.MultipleOperators(equation, "/");
                                break;
                            }
                            screen = Util.CheckComma(screen);
                            display = Util.fillDisplay(screen);
                            equation.Add(Double.Parse(display));
                            equation.Add(inPressedDigit);
                            screen.Clear();
                            break;

                        case '=':
                            // unesen je "=" -> izracunaj
                            screen = Util.CheckComma(screen); // moguci problem
                            display = Util.fillDisplay(screen);
                            if(screen.Count == 0)
                            {
                                display = Util.SolveEquation(equation).ToString();
                                screen.Clear();

                            }
                            else
                            {
                                Console.WriteLine("Velicina " + equation.Count);
                                equation.Add(Double.Parse(display));
                                screen.Clear();
                                display = Util.SolveEquation(equation).ToString();
                                if (display.Length > 10 && !display.Contains(",") && !display.Contains("-"))
                                {
                                    display = "-E-";
                                }

                            }
                            break;

                        case 'P':
                            // spremi u memoriju broj
                            screen = Util.CheckComma(screen);
                            display = Util.fillDisplay(screen);
                            if (numberInMemory != 0.0)
                            {
                                display = Util.StoreMultiple(numberInMemory, display);
                                numberInMemory = Double.Parse(display);
                            }
                            else
                            {
                                numberInMemory = Double.Parse(display);
                            }
                            screen.Clear();
                            break;

                        case 'G':
                            // dohvati broj iz memorije
                            display = numberInMemory.ToString();
                            break;

                        case 'C':
                            // unesen je C (clear) ne brise se memorija niti prethodni rezultati
                            screen.Clear();
                            screen.Add('0');
                            display = Util.fillDisplay(screen);
                            break;

                        case 'O':
                            // unesen je O (off) brise sve
                            screen.Clear();
                            screen.Add('0');
                            numberInMemory = 0;
                            display = Util.fillDisplay(screen);
                            equation.Clear();
                            // mozda jos neka provjera (akcija)
                            break;

                        case 'I':
                            // inverz izraza
                            // ovdje radim nesto krivo
                            if(screen.Count == 1 && screen[0] == '0')
                            {
                                display = "-E-";
                                break;
                            }
                            else if (screen.Count == 0)
                            {
                                double nmbr = Util.SolveInverse(display);
                                display = nmbr.ToString();
                                
                                break;
                            }

                            screen = Util.CheckComma(screen);
                            display = Util.fillDisplay(screen);
                            double number = Util.SolveInverse(display);
                            display = number.ToString();
                            screen.Clear();
                            foreach(char c in display)
                            {
                                screen.Add(c);
                            }
                            break;

                        case 'R':
                            // korjenovanje izraza
                            if (screen.Count == 1 && screen[0] == '0')
                            {
                                display = "-E-";
                                break;
                            }
                            else if (screen.Count == 0)
                            {
                                double nmbr = Util.SolveRoot(display);
                                display = nmbr.ToString();
                                break;
                            }
                            screen = Util.CheckComma(screen);
                            display = Util.fillDisplay(screen);
                            number = Util.SolveRoot(display);
                            display = number.ToString();
                            screen.Clear();
                            foreach (char c in display)
                            {
                                screen.Add(c);
                            }
                            break;

                        case 'Q':
                            // kvadriranje izraza
                            if (screen.Count == 1 && screen[0] == '0')
                            {
                                display = "-E-";
                                break;
                            }
                            else if (screen.Count == 0)
                            {
                                double nmbr = Util.SolveRoot(display);
                                display = nmbr.ToString();
                                break;
                            }
                            screen = Util.CheckComma(screen);
                            display = Util.fillDisplay(screen);
                            number = Util.SolveQuadrat(display);
                            display = number.ToString();
                            screen.Clear();
                            foreach (char c in display)
                            {
                                screen.Add(c);
                            }
                            break;

                        case 'T':
                            // tangens izraza
                            if (screen.Count == 1 && screen[0] == '0')
                            {
                                display = "-E-";
                                break;
                            }
                            else if (screen.Count == 0)
                            {
                                double nmbr = Util.SolveRoot(display);
                                display = nmbr.ToString();
                                break;
                            }
                            screen = Util.CheckComma(screen);
                            display = Util.fillDisplay(screen);
                            number = Util.SolveTangens(display);
                            display = number.ToString();
                            screen.Clear();
                            foreach (char c in display)
                            {
                                screen.Add(c);
                            }
                            break;

                        case 'K':
                            // kosinus izraza
                            if (screen.Count == 1 && screen[0] == '0')
                            {
                                display = "-E-";
                                break;
                            }
                            else if (screen.Count == 0)
                            {
                                double nmbr = Util.SolveRoot(display);
                                display = nmbr.ToString();
                                break;
                            }
                            screen = Util.CheckComma(screen);
                            display = Util.fillDisplay(screen);
                            number = Util.SolveCosinus(display);
                            display = number.ToString();
                            screen.Clear();
                            foreach (char c in display)
                            {
                                screen.Add(c);
                            }
                            break;

                        case 'S':
                            // sinus izraza
                            if (screen.Count == 1 && screen[0] == '0')
                            {
                                display = "-E-";
                                break;
                            }
                            else if (screen.Count == 0)
                            {
                                double nmbr = Util.SolveRoot(display);
                                display = nmbr.ToString();
                                break;
                            }
                            screen = Util.CheckComma(screen);
                            display = Util.fillDisplay(screen);
                            number = Util.SolveSinus(display);
                            display = number.ToString();
                            screen.Clear();
                            foreach (char c in display)
                            {
                                screen.Add(c);
                            }
                            break;

                        case 'M':
                            // mijenjaj predznak
                            screen = Util.CheckComma(screen);
                            screen = Util.AddMinus(screen);
                            display = Util.fillDisplay(screen);
                            break;

                        case ',':
                            screen.Add(',');
                            display = Util.fillDisplay(screen);
                            break;

                    }
                }
            }
        }

        public string GetCurrentDisplayState()
        {
            return display;
        }

    }

}
