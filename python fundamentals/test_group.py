from api.model.dbmodel.user import Role
from api.tests.resource import UnitTestBase
from api.views import group as group_view


class TestGroup(UnitTestBase):
    admin_email = 'test_admin@email.com'
    auditor_email = 'test_auditor@email.com'

    def test_add_group(self):
        login_data = self.login_user(self.admin_email, 'password',
                                     role_name=Role.ROLE_ADMIN)
        security_token = login_data.get('security_token')

        resp = self.app.post_json(group_view.groups.path,
                                  {
                                      "name": "Grupa 1",
                                      "risk_methodology": 1,
                                      "risk_calculation": 1,
                                      "likelihood_impact_formula": 2,
                                      "contributing_risk_formula": 2,
                                      "organization_id": 1
                                  },
                                  headers={
                                      'Securitytoken': str(security_token)
                                  }, status=200)

        assert resp.json['status'] == 'OK'
