def faktoriel(n):
  if n == 1:
      return n
  else:
      return n * faktoriel(n-1)

def reverse(s):
  if len(s) == 1:
    return s
  else:
    return s[-1:] + reverse(s[:-1])

def fib(n):
  if n <= 1:
    return n
  else:
    return fib(n-1) + fib(n-2)

print("Unesi koji faktorijel zelis")
n = int(input())
print(faktoriel(n))

print("Unesi rijec koju zelis da ti okrenem")
s = str(input())
print(reverse(s))

print("Unesi duljinu fibonacijevog niza")
n = int(input())
for i in range(n):
  print(fib(i))
