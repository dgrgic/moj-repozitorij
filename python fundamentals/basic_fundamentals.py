class Klasa:
    def __init__(self, value):
        self.value = value
    def __repr__(self):
        return str(self.value)

class Derived(Klasa):
    def __init__(self, value, kljuc):
        super().__init__(value)
        self.kljuc = kljuc
    def __repr__(self):
        return '{0} ima kljuc {1}'.format(self.value, self.kljuc)

k = Klasa(12)
print(k)

k.__setattr__('value', 25)
print(k)

k.value = 33
print(k)

value = k.value
print('Dobiven value:', value)

value = k.__getattribute__('value')
print('Dobiven value preko builtina:', value)

d = Derived(50, 'key')
print('Derived', d)
