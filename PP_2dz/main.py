from mpi4py import MPI
from board import Board
import time, math

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

max_depth = 4

# evaluate best move for cpu
def evaluate(current_board, last_player, last_column, depth):
    board_to_evaluate = Board(current_board.copy_board())
    winner = board_to_evaluate.find_four(last_column)
    total = 0
    all_lose, all_win = True, True
    moves = 0

    # if cpu is winner return 1
    if winner == board_to_evaluate.cpu:
        return 1
    # if player is winner return -1
    if winner == board_to_evaluate.player:
        return -1
    # if we reached the end return 0
    if depth == 0:
        return 0

    if last_player == board_to_evaluate.cpu:
        next_player = board_to_evaluate.player
    else:
        next_player = board_to_evaluate.cpu

    for i in range(board_to_evaluate.width):
        if board_to_evaluate.move_legal(i):
            moves += 1
            board_to_evaluate.new_move(i, next_player)
            result = evaluate(board_to_evaluate, next_player, i, depth - 1)
            board_to_evaluate.undo_move(i)

            if result > -1:
                all_lose = False
            if result != 1:
                all_win = False
            if result == 1 and next_player == board_to_evaluate.cpu:
                return 1
            if result == -1 and next_player == board_to_evaluate.player:
                return -1

            total += result

    if all_win:
        return 1
    if all_lose:
        return -1

    return total / moves


# get single task for worker
def get_task(tasks, board):
    for i in range(board.width):
        for j in range(board.height):
            if (i, j) not in tasks.keys():
                tasks[(i, j)] = 0.0
                return i, j
    return -1, -1


if rank == 0:
    # i'm master
    board = Board()
    results = 0

    # instantiate tasks dictionary
    tasks = {}

    # check if there is a winner already
    for i in range(board.width):
        winner = board.find_four(i)
        if winner != 0:
            board.show_board()
            print(f"Winner is {winner}")
            break

    # if there is no winner, continue
    player_column = int(input("Choose column to put a coin: "))
    board.new_move(player_column, board.player)

    while True:

        # check if there is a winner already
        for i in range(board.width):
            winner = board.find_four(i)
            if winner != 0:
                board.show_board()
                print(f"Winner is {winner}")
                break

        # start measure time
        start_time = time.time()

        # send workers copy of board
        for i in range(1, size):
            # print(f"[M] Send to {i} a copy of board")
            comm.send({"type": "BOARD", "board": board.copy_board()}, dest=i)

        end = False

        while not end:

            # get message
            status = MPI.Status()
            message = comm.recv(source=MPI.ANY_SOURCE, status=status)
            sender_id = message["process_id"]

            # print(f"[M] Got message {message}")

            # if message is a type of task request
            if message["type"] == "TASK_REQUEST":
                # send task
                # print(f"[M] message is task request for process {sender_id}")
                task = get_task(tasks, board)
                # print(f"[M] Send task {task}")
                cpu_move, player_move = task[0], task[1]
                if cpu_move == -1:
                    continue
                # print(f"[M] Send to {sender_id} task ({cpu_move},{player_move})")
                comm.send({"type": "TASK", "cpu_move": cpu_move, "player_move": player_move}, dest=sender_id)

            # or message is a type of result
            elif message["type"] == "RESULT":
                # calculate result
                # print(f"[M] Got result {message}")
                results += 1
                cpu_move = message["cpu_move"]
                player_move = message["player_move"]
                tasks[(cpu_move, player_move)] = message["evaluation"]

                if results == math.pow(board.width, 2):
                    # print("Rezultata je 49")
                    end = True
                    results = 0

        # send workers break signal so they can receive a copy of new board
        for i in range(1, size):
            comm.send({"type": "BREAK"}, dest=i)

        # calculate probabilities
        column_results = [0.0 for i in range(board.width)]
        for i in range(board.width):
            for j in range(board.height):
                column_results[i] += tasks[(i, j)] / board.width

        # end measure time
        stop_time = time.time()
        tasks.clear()
        # print("Taskovi", tasks)

        string_builder = ""
        for cr in column_results:
            string_builder += str(round(cr, 3)) + " "

        # get the best move for cpu and make that move
        best_move_cpu = column_results.index(max(column_results))
        # print(f"Board move cpu {best_move_cpu}")
        board.new_move(best_move_cpu, board.cpu)
        print(string_builder)
        board.show_board()
        # print(f"Elapsed time: {stop_time - start_time}")

        winner = board.find_four(best_move_cpu)
        if winner == board.cpu:
            print("Winner is CPU")
            break

        # player makes a move
        player_column = int(input("\nChoose a column to put a coin: "))
        board.new_move(player_column, board.player)

else:
    # i'm worker
    board_copy = Board()
    end = False

    while not end:

        # get message
        message = comm.recv(source=0)
        # print(f"[W] Recieved from master {message}")

        # got copy of the board
        if message["type"] == "BOARD":
            board_copy = Board(message["board"])

        # begin work
        while True:
            # send request for task
            comm.send({"type": "TASK_REQUEST", "process_id": rank}, dest=0)
            task_message = comm.recv(source=0)

            if task_message["type"] == "BREAK":
                break

            elif task_message["type"] == "TASK":
                # print(f"[W] Got task {task_message}")
                cpu_move = task_message["cpu_move"]
                player_move = task_message["player_move"]
                # print(f"[W] Task ({cpu_move},{player_move})")

                # first make a move for cpu
                board_copy.new_move(cpu_move, board_copy.cpu)
                # print(f"\n[W] Made cpu move ({cpu_move})")
                # board_copy.show_board()
                # print("")

                # if cpu is the winner, notify master
                winner = board_copy.find_four(cpu_move)
                # print(f"[W] Winner? {winner}")

                if winner == board_copy.cpu:
                    comm.send({"type": "RESULT",
                               "process_id": rank,
                               "cpu_move": cpu_move,
                               "player_move": player_move,
                               "evaluation": 1}, dest=0)
                    board_copy.undo_move(cpu_move)
                    # print("Winner is CPU")
                    # end = True
                    continue

                # then make a move for player
                board_copy.new_move(player_move, board_copy.player)
                # print(f"\n[W] Made player move ({player_move})")
                # board_copy.show_board()
                # print("")

                # if player is the winner, notify master
                winner = board_copy.find_four(player_move)
                # print(f"[W] Winner? {winner}")
                if winner == board_copy.player:
                    comm.send({"type": "RESULT",
                               "process_id": rank,
                               "cpu_move": cpu_move,
                               "player_move": player_move,
                               "evaluation": -1}, dest=0)
                    board_copy.undo_move(player_move)
                    board_copy.undo_move(cpu_move)
                    # print("Winner is PLAYER")
                    # end = True
                    continue

                # print(f"[w {rank}] time for evaluation")
                # there is no winner, need to evaluate cpu's move and send evaluation results to master
                evaluation = evaluate(board_copy, board_copy.player, player_move, max_depth)
                board_copy.undo_move(player_move)
                board_copy.undo_move(cpu_move)
                comm.send({"type": "RESULT",
                           "process_id": rank,
                           "cpu_move": cpu_move,
                           "player_move": player_move,
                           "evaluation": evaluation}, dest=0)
