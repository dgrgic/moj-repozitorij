class Board:
    width = 7
    height = 7
    cpu = "C"
    player = "P"

    def __init__(self, copy_board=None):
        self.board = [['=' for j in range(self.height)] for i in range(self.width)]
        if copy_board:
            i, j = 0, 0
            for c in copy_board:
                self.board[i][j] = c
                if j == self.height - 1:
                    i += 1
                    j = 0
                else:
                    j += 1

    # print board to console
    def show_board(self):
        for i in range(self.height):
            table_row = ""
            for j in range(self.width):
                table_row += self.board[j][i] + " "
            print(table_row)

    # copy board
    def copy_board(self):
        new_board = ""
        for i in range(self.width):
            for j in range(self.height):
                new_board += self.board[i][j]
        return new_board

    # check is move is legal
    def move_legal(self, column):
        if column < 0 or column > 6 or self.board[column][0] != "=":
            return False
        return True

    """
        insert new coin to last empty space (row) in given column
        :param column column for inserting new coin
        :param player indicator (pc/player)
    """

    def new_move(self, column, player):
        if not self.move_legal(column):
            print("Invalid move")
        else:
            row = self.height - 1
            while row != 0:
                if self.board[column][row] != "=":
                    # print(f"Postoji novcic {row},{column}")
                    row -= 1
                    continue
                else:
                    # print(f"Ne postoji novcic {row},{column}")
                    self.board[column][row] = player
                    break

    # delete last move in given column
    def undo_move(self, column):
        for i in range(self.height):
            if self.board[column][i] != "=":
                self.board[column][i] = "="
                break

    # finds connected four coins for either player with given last column
    def find_four(self, last_column):
        # print("\nFinding four")
        last_row = 0
        last_player = ""

        for i in range(self.height):
            if self.board[last_column][i] != "=":
                last_row = i
                last_player = self.board[last_column][last_row]
                break

        # print(f"Last row {last_row},last column {last_column}, last_player {last_player}\n")
        # check column
        winner = self.check_column(last_column, last_player)
        # print(f"Winner column {winner}")
        if winner != 0:
            return winner

        # check row
        winner = self.check_row(last_row, last_player)
        # print(f"Winner row {winner}")
        if winner != 0:
            return winner

        # check left diagonal
        winner = self.check_left_diagonal(last_column, last_row, last_player)
        # print(f"Winner ld {winner}")
        if winner != 0:
            return winner

        # check right diagonal
        winner = self.check_right_diagonal(last_column, last_row, last_player)
        # print(f"Winner rd {winner}")
        if winner != 0:
            return winner

        return 0

    # check if there are four coins in given column
    def check_column(self, last_column, last_player):
        # print("Check column\n")
        count = 0
        for i in range(self.height - 1, 2, -1):
            # print(self.board[last_column][i])
            if self.board[last_column][i] == last_player:
                count += 1
            else:
                count = 0
        # print("Count je", count)
        if count == 4:
            return last_player
        return 0

    # check if there are four coins in given row
    def check_row(self, last_row, last_player):
        # print("Check row\n")
        count = 0
        for i in range(self.width):
            # print(self.board[i][last_row])
            if self.board[i][last_row] == last_player:
                count += 1
                if count == 4:
                    return last_player
            else:
                count = 0
        # print("Count je", count)
        return 0

    # check if there are four coins in left diagonal
    def check_left_diagonal(self, last_column, last_row, last_player):
        count = 0
        first_row = last_row
        first_column = last_column

        # check edge cases
        for i in range(self.height):
            if last_column - i < 0 or last_row - i < 0:
                break
            first_column = last_column - i
            first_row = last_row - i

        for i in range(self.height):
            # check edge cases
            if first_column + i >= self.width or first_row + i >= self.height:
                break
            if self.board[first_column + i][first_row + i] == last_player:
                count += 1
            else:
                count = 0

        if count == 4:
            return last_player
        return 0

    # check if there are four coins in left diagonal
    def check_right_diagonal(self, last_column, last_row, last_player):
        count = 0
        first_row = last_row
        first_column = last_column

        # check edge cases
        for i in range(self.height):
            if last_column + i >= self.width or last_row - i < 0:
                break
            first_column = last_column + i
            first_row = last_row - i

        for i in range(self.height):
            # check edge cases
            if first_column - i < 0 or first_row + i >= self.height:
                break
            if self.board[first_column - i][first_row + i] == last_player:
                count += 1
            else:
                count = 0

        if count == 4:
            return last_player
        return 0
