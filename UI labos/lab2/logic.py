import util
import functools

class Labels:
    """
    Labels describing the WumpusWorld
    """
    WUMPUS = 'w'
    TELEPORTER = 't'
    POISON = 'p'
    SAFE = 'o'

    """
    Some sets for simpler checks
    >>> if literal.label in Labels.DEADLY:
    >>>     # Don't go there!!!
    """
    DEADLY = set([WUMPUS, POISON])
    WTP = set([WUMPUS, POISON, TELEPORTER])

    UNIQUE = set([WUMPUS, POISON, TELEPORTER, SAFE])

    POISON_FUMES = 'b'
    TELEPORTER_GLOW = 'g'
    WUMPUS_STENCH = 's'

    INDICATORS = set([POISON_FUMES, TELEPORTER_GLOW, WUMPUS_STENCH])


def stateWeight(state):
    """
    To ensure consistency in exploring states, they will be sorted
    according to a simple linear combination.
    The maps will never be
    larger than 20x20, and therefore this weighting will be consistent.
    """
    x, y = state
    return 20*x + y


@functools.total_ordering
class Literal:
    """
    A literal is an atom or its negation
    In this case, a literal represents if a certain state (x,y) is or is not
    the location of GhostWumpus, or the poisoned pills.
    """

    def __init__(self, label, state, negative=False):
        """
        Set all values. Notice that the state is remembered twice - you
        can use whichever representation suits you better.
        """
        x,y = state

        self.x = x
        self.y = y
        self.state = state

        self.negative = negative
        self.label = label

    def __key(self):
        """
        Return a unique key representing the literal at a given point
        """
        return (self.x, self.y, self.negative, self.label)

    def __hash__(self):
        """
        Return the hash value - this operator overloads the hash(object) function.
        """
        return hash(self.__key())

    def __eq__(first, second):
        """
        Check for equality - this operator overloads '=='
        """
        return first.__key() == second.__key()

    def __lt__(self, other):
        """
        Less than check
        by using @functools decorator, this is enough to infer ordering
        """
        return stateWeight(self.state) < stateWeight(other.state)

    def __str__(self):
        """
        Overloading the str() operator - convert the object to a string
        """
        if self.negative: return '~' + self.label
        return self.label

    def __repr__(self):
        """
        Object representation, in this case a string
        """
        return self.__str__()

    def copy(self):
        """
        Return a copy of the current literal
        """
        return Literal(self.label, self.state, self.negative)

    def negate(self):
        """
        Return a new Literal containing the negation of the current one
        """
        return Literal(self.label, self.state, not self.negative)

    def isDeadly(self):
        """
        Check if a literal represents a deadly state
        """
        return self.label in Labels.DEADLY

    def isWTP(self):
        """
        Check if a literal represents GhostWumpus, the Teleporter or
        a poisoned pill
        """
        return self.label in Labels.WTP

    def isSafe(self):
        """
        Check if a literal represents a safe spot
        """
        return self.label == Labels.SAFE

    def isTeleporter(self):
        """
        Check if a literal represents the teleporter
        """
        return self.label == Labels.TELEPORTER


class Clause:
    """
    A disjunction of finitely many unique literals.
    The Clauses have to be in the CNF so that resolution can be applied to them. The code
    was written assuming that the clauses are in CNF, and will not work otherwise.

    A sample of instantiating a clause (~B v C):

    >>> premise = Clause(set([Literal('b', (0, 0), True), Literal('c', (0, 0), False)]))

    or; written more clearly
    >>> LiteralNotB = Literal('b', (0, 0), True)
    >>> LiteralC = Literal('c', (0, 0), False)

    >>> premise = Clause(set([[LiteralNotB, LiteralC]]))
    """

    def __init__(self, literals):
        """
        The constructor for a clause. The clause assumes that the data passed
        is an iterable (e.g., list, set), or a single literal in case of a unit clause.
        In case of unit clauses, the Literal is wrapped in a list to be safely passed to
        the set.
        """
        if not type(literals) == set and not type(literals) == list:
            self.literals = set([literals])
        else:
            self.literals = set(literals)

    def isResolveableWith(self, otherClause):
        """
        Check if a literal from the clause is resolveable by another clause -
        if the other clause contains a negation of one of the literals.
        e.g., (~A) and (A v ~B) are examples of two clauses containing opposite literals
        """
        for literal in self.literals:
            if literal.negate() in otherClause.literals:
                return True
        return False

    def isRedundant(self, otherClauses):
        """
        Check if a clause is a subset of another clause.
        """
        for clause in otherClauses:
            if self == clause: continue
            if clause.literals.issubset(self.literals):
                return True
        return False

    def negateAll(self):
        """
        Negate all the literals in the clause to be used
        as the supporting set for resolution.
        """
        negations = set()
        for literal in self.literals:
            clause = Clause(literal.negate())
            negations.add(clause)
        return negations

    def __str__(self):
        """
        Overloading the str() operator - convert the object to a string
        """
        return ' V '.join([str(literal) for literal in self.literals])

    def __repr__(self):
        """
        The representation of the object
        """
        return self.__str__()

    def __key(self):
        """
        Return a unique key representing the literal at a given point
        """
        return tuple(sorted(list(self.literals)))

    def __hash__(self):
        """
        Return the hash value - this operator overloads the hash(object) function.
        """
        return hash(self.__key())

    def __eq__(first, second):
        """
        Check for equality - this operator overloads '=='
        """
        return first.__key() == second.__key()


def resolution(clauses, goal):
    """
    Implement refutation resolution.

    The pseudocode for the algorithm of refutation resolution can be found
    in the slides. The implementation here assumes you will use set of support
    and simplification strategies. We urge you to go through the slides and
    carefully design the code before implementing.
    """
    resolvedPairs = set()
    # setOfSupport predstavlja klauzule dobivene negacijom cilja
    # znaci sos sadrzi klauzule nastale negacijom cilja
    pairs = set()
    setOfSupport = goal.negateAll()

    print "Entry clauses and goals:", clauses, goal
    print "Supporting clauses made from negation of goal:", setOfSupport
#  mozda provjeriti selectClauses(clauses,clauses) al nisam siguran
    while(True):
        pairs = selectClauses(clauses, setOfSupport, resolvedPairs)
        if len(pairs) == 0:
            print "Time for same in same sos"
            pairs = selectClauses(setOfSupport, setOfSupport, resolvedPairs)
            if len(pairs) == 0:
                return False
        for pair_clauses in pairs:
            print "Pair of clauses to resolve: ", pair_clauses
            if pair_clauses not in resolvedPairs:
                resolvedPairs.add(pair_clauses)
            resolvent = resolvePair(pair_clauses[0], pair_clauses[1])

            if len(resolvent) == 0:
                print "NIL!!!"
                return True
            print "Not nil :(", resolvent
            if (resolvent in clauses) or (resolvent in setOfSupport):
                return False
            print "Adding resolvent", resolvent, "to Sos", setOfSupport
            print "Previous setOfSupport", setOfSupport
            for res in resolvent:
                setOfSupport.add(res)
            print "New setOfSupport:", setOfSupport
            if setOfSupport in clauses:
                return False
            removeRedundant(clauses, setOfSupport)
        print "Empty clauses"
    """
    ####################################
    ###                              ###
    ###        YOUR CODE HERE        ###
    ###                              ###
    ####################################
    """

def removeRedundant(clauses, setOfSupport):

    # ili obrnuto nisam siguran
    redundant_clauses = set()
    for clause in clauses:
        if clause.isRedundant(setOfSupport):
            redundant_clauses.add(clause)

    for r_clause in redundant_clauses:
        if r_clause in clauses:
            clauses.remove(r_clause)

    """
    Remove redundant clauses (clauses that are subsets of other clauses)
    from the aforementioned sets.
    Be careful not to do the operation in-place as you will modify the
    original sets. (why?)
    ####################################
    ###                              ###
    ###        YOUR CODE HERE        ###
    ###                              ###
    ####################################
    """
    pass

def resolvePair(firstClause, secondClause):

    print "Resolving:", firstClause,"and", secondClause
# first clause je ulazni clause, a second clause je sos
# resolventa je novi clauses

    resolvents = set()
    resolvent_clause = set()
    literals_to_remove = set()

    for lit_one in firstClause.literals:
        for lit_two in secondClause.literals:
            if (lit_one.label == lit_two.label) and (lit_one.negative != lit_two.negative) and (lit_one.state == lit_two.state):
                print "Jesu", lit_one, lit_two
                literals_to_remove.add(lit_one.label)
                print "Remove literal:", lit_one
                # resolvents.append(())
                 # ovdje sada izraditi logiku do kraja
                 # potrebno je provjeriti dal postoje literali u jednom i drugom clauseu
                 # neka dobra logika

    print "Literals to remove:", literals_to_remove
    for literal in firstClause.literals:
        print "FirstClause.literals:", firstClause.literals
        if literal.label in literals_to_remove:
            print "Literal", literal, "should be removed"
            continue
        resolvents.add(literal)
    print "After first clause:", resolvents
    for literal in secondClause.literals:
        if literal.label in literals_to_remove:
            continue
        resolvents.add(literal)
    print "After second clause:", resolvents
    if len(resolvents) != 0:
        resolvent_clause.add(Clause(resolvents))

    print "Rezolvente", resolvent_clause
    return resolvent_clause
    """
    Resolve a pair of clauses.
    ####################################
    ###                              ###
    ###        YOUR CODE HERE        ###
    ###                              ###
    ####################################
    """
    pass

def selectClauses(clauses, setOfSupport, resolvedPairs):
    # ova metoda bira klauzule koje su komplementarne
    clauses_to_resolve = set()

    for clause in clauses:
        for sos in setOfSupport:
            if(clause.isResolveableWith(sos)):
                if (clause, sos) not in resolvedPairs:
                    clauses_to_resolve.add((clause, sos))

    print "Clauses to resolve:", clauses_to_resolve

    return clauses_to_resolve
    """
    Select pairs of clauses to resolve.
    ####################################
    ###                              ###
    ###        YOUR CODE HERE        ###
    ###                              ###
    ####################################
    """
    pass

def testResolution():
    """
    A sample of a resolution problem that should return True.
    You should come up with your own tests in order to validate your code.
    """
    premise1 = Clause(set([Literal('a', (0, 0), True), Literal('b', (0, 0), False)]))
    premise2 = Clause(set([Literal('b', (0, 0), True), Literal('c', (0, 0), False)]))
    premise3 = Clause(Literal('a', (0,0)))
    goal = Clause(Literal('c', (0,0)))

    print resolution(set([premise1, premise2, premise3]), goal)
    print ""

def testResolution1():
    premise1 = Clause(set([Literal('t', (0, 0), True), Literal('u', (0, 0), False)]))
    premise2 = Clause(set([Literal('t', (0, 0), False), Literal('a', (0, 0), False)]))
    premise3 = Clause(set([Literal('a', (0, 0), True), Literal('u', (0, 0), True)]))
    goal = Clause(set([Literal('t', (0,0), True), Literal('a', (0,0), True)]))
    print resolution(set([premise1, premise2, premise3]), goal)
    print ""

def testResolution2():
    premise1 = Clause(set([Literal('f', (0,0), False)]))
    goal = Clause(set([Literal('f', (0,0), False), Literal('g', (0,0), False)]))
    print resolution(set([premise1]), goal)
    print ""

def testResolution3():
    premise1 = Clause(set([Literal('p1', (0,0), True), Literal('p2', (0,0), False)]))
    premise2 = Clause(set([Literal('p2', (0,0), True)]))
    premise3 = Clause(set([Literal('p3', (0,0), True), Literal('p5', (0,0), False)]))
    premise4 = Clause(set([Literal('p6', (0,0), True), Literal('p5', (0,0), True)]))
    premise5 = Clause(set([Literal('p1', (0,0), False), Literal('p3', (0,0), False)]))
    premise6 = Clause(set([Literal('p1', (0,0), False), Literal('p4', (0,0), False)]))
    goal = Clause(set([Literal('p4', (0,0), False)]))

    print resolution(set([premise1, premise2, premise3, premise4, premise5, premise6]), goal)
if __name__ == '__main__':
    """
    The main function - if you run logic.py from the command line by
    >>> python logic.py

    this is the starting point of the code which will run.
    """
    testResolution()
    testResolution1()
    testResolution2()
    testResolution3()
