
"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util
from logic import *

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
        state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
        actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def miniWumpusSearch(problem):
    """
    A sample pass through the miniWumpus layout. Your solution will not contain
    just three steps! Optimality is not the concern here.
    """
    from game import Directions
    e = Directions.EAST
    n = Directions.NORTH
    return  [e, n, n]

def logicBasedSearch(problem):
    """

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:

    print "Start:", problem.getStartState()
    print "Is the start a goal?", problem.isGoalState(problem.getStartState())
    print "Start's successors:", problem.getSuccessors(problem.getStartState())

    print "Does the Wumpus's stench reach my spot?",
               \ problem.isWumpusClose(problem.getStartState())

    print "Can I sense the chemicals from the pills?",
               \ problem.isPoisonCapsuleClose(problem.getStartState())

    print "Can I see the glow from the teleporter?",
               \ problem.isTeleporterClose(problem.getStartState())

    (the slash '\\' is used to combine commands spanning through multiple lines -
    you should remove it if you convert the commands to a single line)

    Feel free to create and use as many helper functions as you want.

    A couple of hints:
        * Use the getSuccessors method, not only when you are looking for states
        you can transition into. In case you want to resolve if a poisoned pill is
        at a certain state, it might be easy to check if you can sense the chemicals
        on all cells surrounding the state.
        * Memorize information, often and thoroughly. Dictionaries are your friends and
        states (tuples) can be used as keys.
        * Keep track of the states you visit in order. You do NOT need to remember the
        tranisitions - simply pass the visited states to the 'reconstructPath' method
        in the search problem. Check logicAgents.py and search.py for implementation.
    """
    # array in order to keep the ordering
    visitedStates = []
    startState = problem.getStartState()

    # ovdje skupljam znanje
    # kljuc je tuple (x,y), a value je lista tuplova sa boolean vrijednostima
    # npr (1,1) : [(S, !S),(C, !C),(G, !G)]
    # ili je bolje imat (1,1) : {'s': True, 'c': False, 'g': False, }
    inteligence = {}

    # forbiden = set()
    danger = set()
    safe = set()
    premises = set()

    open = util.Stack();
    print "Start:", startState
    print "Is the start a goal?", problem.isGoalState(problem.getStartState())
    print "Start's successors:", problem.getSuccessors(problem.getStartState())
    print "Visited states:", visitedStates

    initialState = problem.getStartState()
    open.push(problem.getStartState())

# depthFirstSearch()

    while not open.isEmpty():
        currentState = open.pop()
        print ""
        print "Current state", currentState

        if currentState in visitedStates:
            print "Currentstate je u visited"
            continue

        visitedStates.append(currentState)
        print "Visited after adding current", visitedStates
        if problem.isGoalState(currentState):
            moves = problem.reconstructPath(visitedStates)
            print "Goal!!!"
            print moves
            return moves

        print "Nije goal", currentState

        # skupljam podatke za trenutnu poziciju
        getKnowledge(currentState, inteligence, problem)

        for key in inteligence.keys():
            literals = inteligence[key]
            for lit in literals:
                premises.add(Clause(set([lit])))

        print "Premises:", premises
        print "Got knowledge for", currentState, ":", inteligence[currentState]

        #  provjeravam successore ovisno o trenutnom stanju
        checkFields(currentState, inteligence, problem, visitedStates, danger, safe, premises)

        # ne postoji danger polje u nekom od successora za trenutno stanje,
        # sva polja su sigurna
        # dodajem ih u open jer na njih smijem stati
        # if(len(danger) == 0):

        # postoji danger polje u nekom od successora za trenutno stanje
        # potrebno je odrediti koje je od tih polja duh ili kapsula ovisno o znanju
        # tako da ih spremim u forbiden jer na to polje ne smijem stati
        # else:
        if(len(danger) != 0):
            print "Postoji danger!!!"
            print premises
            for d in danger:
                Wgoal = Clause(set([Literal('w', d, False)]))
                Pgoal = Clause(set([Literal('p', d, False)]))
                if(resolution(premises, Wgoal)):
                    print "Wumpus je na poziciji:", d
                    inteligence[d] = set([Literal('w', d, False)])
                    if d in safe:
                        safe.remove(d)
                elif(resolution(premises, Pgoal)):
                    print "Poison je na poziciji:", d
                    inteligence[d] = set([Literal('p', d, False)])
                    if d in safe:
                        safe.remove(d)


        print inteligence
        print "After", currentState, "danger:", danger, "and safe:", safe

        if len(safe) == 0:
            moves = problem.reconstructPath(visitedStates)
            return moves
        else:
            for safe_spot in safe:
                if safe_spot not in visitedStates:
                    open.push(safe_spot)


        # print "Checked successors for", currentState, ":", inteligence

        # print inteligence

        # for key in inteligence.keys():
        #     if key not in visitedStates:
        #         if key not in forbiden:
        #             open.push(key)


        # inace provjeri za svaki successor dal je wumpus,capsule,teleporter ili free ovisno o dobivenom znanju
        # na isti nacin kao getKnowledge, samo ce se zvati drukcije


# state je pozicija
def getKnowledge(state, inteligence, problem):

    print "Getting knowledge for:", state

    literals = set()

    if(problem.isWumpusClose(state)):
        print "Na poziciji", state, "osjetim smrad Wumpusa"
        literals.add(Literal('s', state, False))

    else:
        literals.add(Literal('s', state, True))

    if(problem.isPoisonCapsuleClose(state)):
        print "Na poziciji", state, "osjetim kemikalije Kapsule"
        literals.add(Literal('c', state, False))

    else:
        literals.add(Literal('c', state, True))

    if(problem.isTeleporterClose(state)):
        print "Na poziciji", state, "osjetim sjaj Teleportera"
        literals.add(Literal('g', state, False))

    else:
        literals.add(Literal('g', state, True))

    print literals

    inteligence[state] = literals

def checkFields(state, inteligence, problem, visited, danger, safe, premises):

    print "Checking successors for:", state

    # set literala koji opisuje trenutnu lokaciju
    literals = inteligence[state]

    # check = set()

    successors = problem.getSuccessors(state)
    print "Successors for:", state, "are:", successors

    # ako je na trenutnom stanju smrad od wumpusa ili kemikalija Kapsule
    # dodaj successor u danger jer je na nekom od njih wumpus ili kapsula

    for literal in literals:
        print "Literal:", literal

        if literal.label == 's' and literal.negative == False:
            print "Its stench at:", state
            lits = set()
            lits.add(Literal('s', state, True))
            lits.add(Literal('s', state, False))
            for suc in successors:
                suc_state = suc[0]
                # ako sam ga vec posjetio znaci da sam i saznao neko znanje
                if suc_state in visited:
                    continue
                elif suc_state in safe:
                    lits.add(Literal('w', suc_state, True))
                else:
                    lits.add(Literal('w', suc_state, False))
            clause = Clause(lits)
            premises.add(clause)
            print "Clause:", clause
            makeSucDanger(successors, visited, danger)

        elif literal.label == 'c' and literal.negative == False:
            print "Its chemicals at:", state
            lits = set()
            lits.add(Literal('c', state, True))
            lits.add(Literal('c', state, False))
            for suc in successors:
                suc_state = suc[0]
                if suc_state in visited:
                    continue
                elif suc_state in safe:
                    lits.add(Literal('p', suc_state, True))
                else:
                    lits.add(Literal('p', suc_state, False))
            clause = Clause(lits)
            premises.add(clause)
            print "Clause:", clause
            makeSucDanger(successors, visited, danger)

        elif literal.label == 'g' and literal.negative == False:
            print "Its glow at:", state
            lits = set()
            lits.add(Literal('g', state, True))
            lits.add(Literal('g', state, False))
            for suc in successors:
                suc_state = suc[0]
                if (suc_state in visited) or (suc_state in danger):
                    continue
                lits.add(Literal('t', suc_state, False))
            clause = Clause(lits)
            premises.add(clause)
            print "Clause:", clause
            makeSucSafe(successors, visited, safe)

        else:
            print "Nothing at:", state
            makeSucSafe(successors, visited, safe)
            safe.add(state)

    # if(Literal('s', state, False) in literals):
    #     print "Currently standing on stench of wumpus", state
    #     for suc in successors:
    #         suc_state = suc[0]
    #         print "Succ:", suc[0][0], suc[0][1], "...", suc[0]
    #         if suc_state in visited:
    #             continue
    #         if(problem.isWumpus(suc_state)):
    #             print "Na mjestu:", suc_state, " je wumpus"
    #             check.add(Literal('w', (suc_state[0], suc_state[1]), False))
    #             inteligence[suc_state] = check
    #             forbiden.add(suc_state)
    #         else:
    #             check.add(Literal('w', (suc_state[0], suc_state[1]), True))
    #             inteligence[suc_state] = check
    #
    # elif(Literal('s', state, True) in literals):
    #     print "Currently NOT standing on stench of wumpus", state
    #     getSuccessorsKnowledge(successors, inteligence, problem, visited)
    #
    # elif(Literal('c', state, False) in literals):
    #     print "Currently standing on chemicals of capsule", state
    #     for suc in successors:
    #         suc_state = suc[0]
    #         if suc_state in visited:
    #             continue
    #         if(problem.isPoisonCapsule(suc_state)):
    #             print "Na mjestu:", suc_state, " je kapsula"
    #             check.add(Literal('p', (suc_state[0], suc_state[1]), False))
    #             inteligence[suc_state] = check
    #             forbiden.add(suc_state)
    #         else:
    #             check.add(Literal('p', (suc_state[0], suc_state[1]), True))
    #             inteligence[suc_state] = check
    #
    # elif(Literal('c', state, True) in literals):
    #     print "Currently NOT standing on the chemicals of a capsule", state
    #     getSuccessorsKnowledge(successors, inteligence, problem, visited)
    #
    # elif(Literal('g', state, False) in literals):
    #     print "Currently standing on glow of teleporter", state
    #     for suc in successors:
    #         suc_state = suc[0]
    #         if suc_state in visited:
    #             continue
    #         if(problem.isTeleporter(suc_state)):
    #             print "Na mjestu:", suc_state, " je Teleporter"
    #             check.add(Literal('t', (suc_state[0], suc_state[1]), False))
    #             inteligence[suc_state] = check
    #         else:
    #             check.add(Literal('t', (suc_state[0], suc_state[1]), True))
    #             inteligence[suc_state] = check
    #
    # elif(Literal('g', state, True) in literals):
    #     print "Currently NOT standing on the glow of a teleporter", state
    #     getSuccessorsKnowledge(successors, inteligence, problem, visited)
    #
    # else:
    #     print "Currently standing on safe spot", state
    #     getSuccessorsKnowledge(successors, inteligence, problem, visited)
    #

# def getSuccessorsKnowledge(successors, inteligence, problem, visited):
#     for suc in successors:
#         suc_state = suc[0]
#         print "Succ:", suc[0][0], suc[0][1], "...", suc[0]
#         if suc_state in visited:
#             continue
#         getKnowledge(suc_state, inteligence, problem)

def makeSucDanger(successors, visited, danger):
    for suc in successors:
        suc_state = suc[0]
        if suc_state in visited:
            continue
        danger.add(suc_state)

def makeSucSafe(successors, visited, safe):
    for suc in successors:
        suc_state = suc[0]
        if suc_state in visited:
            continue
        safe.add(suc_state)

# Abbreviations
lbs = logicBasedSearch
