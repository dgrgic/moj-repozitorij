import util
import math

class NaiveBayesClassifier(object):
    """
    See the project description for the specifications of the Naive Bayes classifier.

    Note that the variable 'datum' in this code refers to a counter of features
    (not to a raw samples.Datum).
    """
    def __init__(self, legalLabels, smoothing=0, logTransform=False, featureValues=util.Counter()):
        self.legalLabels = legalLabels
        self.type = "naivebayes"
        self.k = int(smoothing) # this is the smoothing parameter, ** use it in your train method **
        self.logTransform = logTransform
        self.featureValues = featureValues # empty if there is no smoothing

    def fit(self, trainingData, trainingLabels):
        """
        Trains the classifier by collecting counts over the training data, and
        stores the smoothed estimates so that they can be used to classify.

        trainingData is a list of feature dictionaries.  The corresponding
        label lists contain the correct label for each instance.

        To get the list of all possible features or labels, use self.features and self.legalLabels.
        """

        self.features = trainingData[0].keys() # the names of the features in the dataset

        self.prior = util.Counter() # probability over labels
        self.conditionalProb = util.Counter() # Conditional probability of feature feat for a given class having value v
                                      # HINT: could be indexed by (feat, label, value)

        # TODO:
        # construct (and store) the normalized smoothed priors and conditional probabilities

        dataset = len(trainingData) #ukupan broj podataka

        if self.k > 0:
            for feat in self.features:
                self.featureValues[feat] = set();

        for label in trainingLabels:
            self.prior[label] += 1.0

        # sad trebam iterirati po parovima (znacajka, hipoteza) i povecavati broj pojavljivanja

        for i in range(0, len(trainingData)):
            for data in trainingData[i]:
                if self.k > 0:
                    self.featureValues[data].add(trainingData[i][data])
                self.conditionalProb[(data, trainingData[i][data], trainingLabels[i])] += 1.0


        # sad ovdje racunam izglednosti na nacin -> broj pojavljivanja podataka sa svim tim vrijednostima / broj pojavljivanja hipoteze
        # potrebno je izgladiti na nacin (count + self.k)/(countPrior + self.k * legalLabels)
        for prob in self.conditionalProb:
            count = self.conditionalProb[prob]
            if self.k > 0:
                self.conditionalProb[prob] = float(count + self.k)/float(self.prior[prob[2]] + self.k * len(self.featureValues[prob[0]]))
            else:
                self.conditionalProb[prob] = float(count)/float(self.prior[prob[2]])


        for label in self.prior:
            count = self.prior[label]
            self.prior[label] = float(count)/float(dataset)

        "*** YOUR CODE HERE ***"

    def predict(self, testData):
        """
        Classify the data based on the posterior distribution over labels.

        You shouldn't modify this method.
        """

        guesses = []
        self.posteriors = [] # posterior probabilities are stored for later data analysis.

        for instance in testData:
            if self.logTransform:
                posterior = self.calculateLogJointProbabilities(instance)
            else:
                posterior = self.calculateJointProbabilities(instance)

            guesses.append(posterior.argMax())
            self.posteriors.append(posterior)
        return guesses


    def calculateJointProbabilities(self, instance):
        """
        Returns the joint distribution over legal labels and the instance.
        Each probability should be stored in the joint counter, e.g.
        Joint[3] = <Estimate of ( P(Label = 3, instance) )>

        To get the list of all possible features or labels, use self.features and
        self.legalLabels.
        """
        joint = util.Counter()

        "*** YOUR CODE HERE ***"

        for label in self.legalLabels:
            # calculate the joint probabilities for each class
            product = float(1)

            for feat in instance:
                if (feat, instance[feat], label) in self.conditionalProb:
                    product *= float(self.conditionalProb[(feat, instance[feat], label)])

            joint[label] = product * float(self.prior[label])

        return joint


    def calculateLogJointProbabilities(self, instance):
        """
        Returns the log-joint distribution over legal labels and the instance.
        Each log-probability should be stored in the log-joint counter, e.g.
        logJoint[3] = <Estimate of log( P(Label = 3, instance) )>

        To get the list of all possible features or labels, use self.features and
        self.legalLabels.
        """
        logJoint = util.Counter()

        "*** YOUR CODE HERE ***"

        for label in self.legalLabels:
            product = float(1)

            for feat in instance:
                if (feat, instance[feat], label) in self.conditionalProb:
                    product *= float(self.conditionalProb[(feat, instance[feat], label)])

            logJoint[label] = math.log(product * float(self.prior[label]))

        return logJoint
