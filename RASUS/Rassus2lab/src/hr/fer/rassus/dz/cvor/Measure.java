package hr.fer.rassus.dz.cvor;

public class Measure {

	private String value;
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Double getScalar() {
		return scalar;
	}

	public void setScalar(Double scalar) {
		this.scalar = scalar;
	}

	public String getVector() {
		return vector;
	}

	public void setVector(String vector) {
		this.vector = vector;
	}

	private Double scalar;
	private String vector;
	
	public Measure(String value, Double scalar, String vector) {
		this.value = value;
		this.scalar = scalar;
		this.vector = vector;
	}
	
	public String toString() {
		return value + " " + scalar.toString() + " " + vector;
	}
}
