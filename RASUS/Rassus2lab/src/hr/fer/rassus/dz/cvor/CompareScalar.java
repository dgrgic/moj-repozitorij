package hr.fer.rassus.dz.cvor;

import java.util.Comparator;

public class CompareScalar implements Comparator<Measure>{

	@Override
	public int compare(Measure o1, Measure o2) {
		return o1.getScalar().compareTo(o2.getScalar());
	}
	
}