package hr.fer.rassus.dz.cvor;

import java.util.ArrayList;
import java.util.List;

public class Util{

	public static double averageValue(List<String> measures) {
		int sum = 0;
		int len = measures.size();
		
		for(String pom: measures) {
			sum += Integer.valueOf(pom.split(" ")[0]);
		}
		
		return (double)sum/len;
	}
	
	public static List<String> sortByScalarValue(List<String> measures){
		List<Measure> helpList = new ArrayList<Measure>();
		for(String m: measures) {
			String help[] = m.split(" ");
			Measure measure = new Measure(help[0], Double.parseDouble(help[1]), help[2]);
			helpList.add(measure);
		}
		helpList.sort(new CompareScalar());
		measures.clear();
		for(Measure m: helpList) {
			measures.add(m.toString());
		}
		return measures;
	}
	
	public static List<String> sortByVectorValue(List<String> measures){
		List<Measure> helpList = new ArrayList<Measure>();
		for(String m: measures) {
			String help[] = m.split(" ");
			Measure measure = new Measure(help[0], Double.parseDouble(help[1]), help[2]);
			helpList.add(measure);
		}
		helpList.sort(new CompareVector());
		measures.clear();
		for(Measure m: helpList) {
			measures.add(m.toString());
		}
		return measures;
	}
	
	
	
}
