package hr.fer.rassus.dz.cvor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import hr.fer.rassus.dz.client.StupidUDPClient;
import hr.fer.rassus.dz.server.StupidUDPServer;

public class Node {

	private int vector[] = {0, 0, 0, 0}; // vektorska tablica
	private int position = 0; // pozicija cvora u tablici ovisno o dobivenom portu
	
	private List<Integer> nodes; // static configuration of nodes in network (list of ports)
	private List<String> measurements;
	private EmulatedSystemClock clock;
	private int serverPort;
	
	public Node() {
		nodes = new ArrayList<>();
		measurements = new ArrayList<>();
		clock = new EmulatedSystemClock();
		fillNodeAddress();
		readMeasurements();
	}
	
	private void fillNodeAddress() {
		try {
			BufferedReader file =  new BufferedReader(new InputStreamReader(
					new FileInputStream(
							new File("adrese.txt"))));
			String line = file.readLine();
			String[] addresses = line.split(",");
			for(String pom: addresses) {
				nodes.add(Integer.parseInt(pom));
			}
			file.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		int randomIndex = (int) (Math.random() * (3 - 0 + 1)) + 0; // random index to choose server port
		int possiblePort = nodes.get(randomIndex); // possible server port
		
		position = randomIndex;
//		System.out.println("Dobio sam random index " + position);
		
		serverPort = possiblePort;
	}
	
	private void readMeasurements() {
		try {
			BufferedReader file =  new BufferedReader(new InputStreamReader(
					new FileInputStream(
							new File("mjerenja.txt"))));
			String line;
			while((line = file.readLine()) != null) {
				if(line.charAt(0) == 'T') {
					continue;
				}
				measurements.add(line.split(",")[3]);
			}
			file.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private String randomMeasurement(long start) {
		long endTime = System.currentTimeMillis();
		long activeTime = (long) ((endTime - start) / 1000F);
		int index = Math.floorMod(activeTime, 100) + 2;
		
		String scalarValue = String.valueOf(clock.currentTimeMillis());
		StringBuffer vectorValue = new StringBuffer();
		StringBuffer send = new StringBuffer();
		
//		System.out.println("\nStvaram vector\n");
		for(int v: vector) {
//			System.out.println("Duljina je: " + String.valueOf(v).length());
			vectorValue.append(String.valueOf(v).trim() + ",");
		}
		
		vectorValue.deleteCharAt(vectorValue.length()-1);
		
		String measurement = measurements.get(index-1);
		
		send.append(measurement);
		send.append(" ");
		send.append(scalarValue);
		send.append(" ");
		send.append(vectorValue);
		
		return send.toString();
	}
	
	private void checkServerMeasure(String serverMeasure) {
		String[] values = serverMeasure.split(" ")[2].split(",");
//		System.out.println("\nChekiram dobiveni vektor " + values.length);
		
		for(int i=0; i<values.length; i++) {
			
//			System.out.println("Vrijednost na mjestu " + i + " je " + values[i] + " a duljina je " + values[i].length());
			int value = 0;
			
			try {
				value = Integer.valueOf(values[i].trim());	
			}
			catch(NumberFormatException e) {
				System.out.println("Cannot parse " + values[i] + " to int");
				
			}
			
			if(value > vector[i]) {
				vector[i] = value;
			}
		}
	}
	
	private void printMeasures(List<String> myMeasures, List<String> serverMeasures) {
		//podatak koji saljem je tipa (mjerenje, vektorska, skalarna)
		//printam : sortirane po vektorskom, skalarnom i prosjek svih mjerenja (mojih i tudih)
		
//		System.out.println("Duljina mojih: " + myMeasures.size() + "\n");
//		System.out.println("Duljina drugih: " + serverMeasures.size() + "\n");
		
		List<String> allMeasures = new ArrayList<String>();
		allMeasures.addAll(myMeasures);
		allMeasures.addAll(serverMeasures);
		
		Double average = Util.averageValue(allMeasures);
		System.out.println("Srednja vrijednost za cvor " + serverPort + " iznosi " + average);
		
		System.out.println("Mjerenja sortirana po skalarnoj oznaci:");
		allMeasures = Util.sortByScalarValue(allMeasures);
		for(String s: allMeasures) {
			System.out.println(s);
		}
		
		allMeasures = Util.sortByVectorValue(allMeasures);
		System.out.println("Mjerenja sortirana po vektorskoj oznaci:");
		for(String s: allMeasures) {
			System.out.println(s);
		}
		System.out.println();
	}
	
	public static void main(String[] args) {
		
		long startTime = System.currentTimeMillis();
		Node node = new Node();
		StupidUDPClient client = new StupidUDPClient();
		StupidUDPServer udpServer = new StupidUDPServer(node.serverPort);
		
		Thread server = new Thread(udpServer);
		server.start();
		
		List<String> myMeasures = new ArrayList<String>();
		List<String> serverMeasures = new ArrayList<String>();
		
		long startMeasureTime = System.currentTimeMillis();
		long endMeasureTime;
		long diffMeasureTime;
		
		while(true) {
			
			String serverMeasure = udpServer.getMeasure();
//			System.out.println("\nDobio sam mjerenje od servera " + serverMeasure + "\n");
			
			if(serverMeasure != "") {
				node.vector[node.position] += 1;
				node.checkServerMeasure(serverMeasure);
				serverMeasures.add(serverMeasure); // povecavam iznos tablice kod primitka poruke
			}
			
			node.vector[node.position] += 1; // povecavam iznos tablice kod slanja poruke
			
			String myMeasure = node.randomMeasurement(startTime);
			
			myMeasures.add(myMeasure);
			
//			System.out.println("\nSaljem svim susjedima\n");
			for(int port: node.nodes) {
				if(port != node.serverPort) {
//					System.out.println("\nSaljem na: " + port + ", a moj je: " + node.serverPort + "\n");
					try {
						client.sendPacket(myMeasure, port);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			
			endMeasureTime = System.currentTimeMillis();
			diffMeasureTime = endMeasureTime - startMeasureTime;
			
			if(diffMeasureTime >= 5000) {
//				System.out.println("\nproslo je 5 sekundi, racunam prosjek i sortiram");
				node.printMeasures(myMeasures, serverMeasures);
				myMeasures.clear();
				serverMeasures.clear();
				startMeasureTime = System.currentTimeMillis();
			}
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}		
	}
	
}
