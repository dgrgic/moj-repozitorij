package hr.fer.rassus.dz.cvor;

import java.util.Comparator;

public class CompareVector implements Comparator<Measure> {

	@Override
	public int compare(Measure o1, Measure o2) {
		return o1.getVector().compareTo(o2.getVector());
	}

}
