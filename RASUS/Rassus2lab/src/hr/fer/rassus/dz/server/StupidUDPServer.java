/*
 * This code has been developed at Departement of Telecommunications,
 * Faculty of Electrical Engineering and Computing, University of Zagreb.
 */
package hr.fer.rassus.dz.server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

import hr.fer.rassus.dz.network.SimpleSimulatedDatagramSocket;

/**
 *
 * @author Krešimir Pripužić <kresimir.pripuzic@fer.hr>
 */
public class StupidUDPServer implements Runnable {
    
    private int port; // server port
    private String measure;
    
    public StupidUDPServer(int port) {
    	this.port = port;
    	this.measure = "";
    }
    
    public String getMeasure() {
    	return this.measure;
    }
    
	@Override
	public void run() {
        byte[] rcvBuf = new byte[256]; // received bytes
        byte[] sendBuf = new byte[256];// sent bytes
        String rcvStr;

        DatagramSocket socket = null;
		
		try {
			socket = new SimpleSimulatedDatagramSocket(this.port, 0.2, 200);
		} catch (SocketException | IllegalArgumentException e1) {
			e1.printStackTrace();
			System.exit(0);
		} //SOCKET -> BIND

        while (true) { //OBRADA ZAHTJEVA
            DatagramPacket packet = new DatagramPacket(rcvBuf, rcvBuf.length);

            try {
				socket.receive(packet);
	            rcvStr = new String(packet.getData(), packet.getOffset(),
	                    packet.getLength());
//	            System.out.printf("\nServer %d received from %d : " + rcvStr, port, packet.getPort());

	            measure = rcvStr;
	            	            
	            sendBuf = "yes".toUpperCase().getBytes();
	            
//	            System.out.println("\nServer sends: YES\n");

	            DatagramPacket sendPacket = new DatagramPacket(sendBuf,
	                    sendBuf.length, packet.getAddress(), packet.getPort());

				socket.send(sendPacket);

			} catch (IOException e) {
				e.printStackTrace();
			}

        }

		
	}

    /**
     * @param args the command line arguments
     */
//    public static void main(String args[]) throws IOException {
//        
//        byte[] rcvBuf = new byte[256]; // received bytes
//        byte[] sendBuf = new byte[256];// sent bytes
//        String rcvStr;
//
//        // create a UDP socket and bind it to the specified port on the local
//        // host
//        DatagramSocket socket = new SimpleSimulatedDatagramSocket(PORT, 0.2, 200); //SOCKET -> BIND
//
//        while (true) { //OBRADA ZAHTJEVA
//            // create a DatagramPacket for receiving packets
//            DatagramPacket packet = new DatagramPacket(rcvBuf, rcvBuf.length);
//
//            // receive packet
//            socket.receive(packet); //RECVFROM
//
//            // construct a new String by decoding the specified subarray of
//            // bytes
//            // using the platform's default charset
//            rcvStr = new String(packet.getData(), packet.getOffset(),
//                    packet.getLength());
//            System.out.println("Server received: " + rcvStr);
//
//            // encode a String into a sequence of bytes using the platform's
//            // default charset
//            sendBuf = rcvStr.toUpperCase().getBytes();
//            System.out.println("Server sends: " + rcvStr.toUpperCase());
//
//            // create a DatagramPacket for sending packets
//            DatagramPacket sendPacket = new DatagramPacket(sendBuf,
//                    sendBuf.length, packet.getAddress(), packet.getPort());
//
//            // send packet
//            socket.send(sendPacket); //SENDTO
//        }
//    }
}
