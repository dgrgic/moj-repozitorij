/*
 * This code has been developed at Departement of Telecommunications,
 * Faculty of Electrical Eengineering and Computing, University of Zagreb.
 */
package hr.fer.rassus.dz.client;

import hr.fer.rassus.dz.network.SimpleSimulatedDatagramSocket;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Krešimir Pripužić <kresimir.pripuzic@fer.hr>
 */
public class StupidUDPClient {

    private byte[] rcvBuf = new byte[256]; // received bytes
    private InetAddress address;
    private DatagramSocket socket; //SOCKET
    private boolean sent;
    private DatagramPacket lastSent;
    
    public StupidUDPClient() {
    	try {
			this.address = InetAddress.getByName("localhost");
			this.socket = new SimpleSimulatedDatagramSocket(0.2, 200);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (SocketException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
    }
    
    public void sendPacket(String sendString, int serverPort) throws IOException {
    	
    	sent = false; // flag to check if packet was successfully sent to server
    	byte[] sendBuf = new byte[256];// sent bytes
    	
    	for (int i = 0; i < sendString.length(); i++) {
            sendBuf[i] = (byte) sendString.charAt(i);
        }

        DatagramPacket packet = new DatagramPacket(sendBuf, sendBuf.length,
                address, serverPort);

        lastSent = new DatagramPacket(sendBuf, sendBuf.length, address, serverPort);
        
//        socket.send(packet); //SENDTO
//        
//        System.out.println("\n" + new String(sendBuf) + "\n");
        
        StringBuffer receiveString = new StringBuffer();

        while (!sent) {
        	
            socket.send(packet); //SENDTO
            
//            System.out.println("\n" + new String(sendBuf) + "\n");

            DatagramPacket rcvPacket = new DatagramPacket(rcvBuf, rcvBuf.length);

            try {
                
            	// ovdje provjeriti jel stigla potvrda i ako je nista, ako nije salji ponovno
                socket.receive(rcvPacket); //RECVFROM
                if(lastSent.getLength() != 0 && lastSent.equals(rcvPacket)) {
                	break;
                }
                sent = true;
            } catch (SocketTimeoutException e) {
                break;
            } catch (IOException ex) {
                Logger.getLogger(StupidUDPClient.class.getName()).log(Level.SEVERE, null, ex);
            }

            receiveString.append(new String(rcvPacket.getData(), rcvPacket.getOffset(), rcvPacket.getLength()));
            lastSent = rcvPacket;

        }
//        System.out.printf("\nClient received from %d: " + receiveString, serverPort);

    }

//    public static void main(String args[]) throws IOException {
//
//        String sendString = "Any string...";
//
//        byte[] rcvBuf = new byte[256]; // received bytes
//
//        // encode this String into a sequence of bytes using the platform's
//        // default charset and store it into a new byte array
//
//        // determine the IP address of a host, given the host's name
//        InetAddress address = InetAddress.getByName("localhost");
//
//        // create a datagram socket and bind it to any available
//        // port on the local host
//        //DatagramSocket socket = new SimulatedDatagramSocket(0.2, 1, 200, 50); //SOCKET
//        DatagramSocket socket = new SimpleSimulatedDatagramSocket(0.2, 200); //SOCKET
//
//        System.out.print("Client sends: ");
//        // send each character as a separate datagram packet
//        for (int i = 0; i < sendString.length(); i++) {
//            byte[] sendBuf = new byte[1];// sent bytes
//            sendBuf[0] = (byte) sendString.charAt(i);
//
//            // create a datagram packet for sending data
//            DatagramPacket packet = new DatagramPacket(sendBuf, sendBuf.length,
//                    address, PORT);
//
//            // send a datagram packet from this socket
//            socket.send(packet); //SENDTO
//            System.out.print(new String(sendBuf));
//        }
//        System.out.println("");
//
//        StringBuffer receiveString = new StringBuffer();
//
//        while (true) {
//            // create a datagram packet for receiving data
//            DatagramPacket rcvPacket = new DatagramPacket(rcvBuf, rcvBuf.length);
//
//            try {
//                // receive a datagram packet from this socket
//                socket.receive(rcvPacket); //RECVFROM
//            } catch (SocketTimeoutException e) {
//                break;
//            } catch (IOException ex) {
//                Logger.getLogger(StupidUDPClient.class.getName()).log(Level.SEVERE, null, ex);
//            }
//
//            // construct a new String by decoding the specified subarray of bytes
//            // using the platform's default charset
//            receiveString.append(new String(rcvPacket.getData(), rcvPacket.getOffset(), rcvPacket.getLength()));
//
//        }
//        System.out.println("Client received: " + receiveString);
//
//        // close the datagram socket
//        socket.close(); //CLOSE
//    }
}
