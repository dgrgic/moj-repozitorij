package hr.fer.rassus.homework.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import hr.fer.rassus.homework.model.Measurement;
import hr.fer.rassus.homework.model.SensorDescription;
import hr.fer.rassus.homework.model.UserAddress;

/**
 * 
 * @author dorijan Serves sensor's request through SensorController
 *
 */
@Service
public class ServiceImplementation implements IService {

	private static final Logger log = LoggerFactory.getLogger(ServiceImplementation.class);

	private Map<String, SensorDescription> sensors; // all registered sensors
	private Map<String, SensorDescription> sensorsNeighbour; // sensors with their neighbours
	private List<Measurement> measurements; // all measurements

	public ServiceImplementation() {
		sensors = new HashMap<>();
		sensorsNeighbour = new HashMap<>();
		measurements = new ArrayList<>();
	}

	@Override
	public boolean register(SensorDescription sensor) {
		if (sensors.containsKey(sensor.getUsername())) {
			return false;
		}
		sensors.put(sensor.getUsername(), sensor);
		return true;
	}

	@Override
	public UserAddress searchNeighbour(String username) {
		log.info("Looking for the neighbour of sensor " + username);

		if (!sensors.containsKey(username)) {
			log.error("Sensor " + username + " is not registered");
			return null; // sensor is not registered
		}

		else if (sensorsNeighbour.containsKey(username)) {
			SensorDescription neighbour = sensorsNeighbour.get(username);
			log.info("Neighbour for sensor " + username + " is " + neighbour.getUsername());
			return new UserAddress(neighbour.getIpAddress(), neighbour.getPort());
		}

		else { // sensor is registered and does not have a neighbour yet
			SensorDescription sensor = sensors.get(username);
			SensorDescription neighbour = null;
			double distance = 0;
			
			if(sensors.values().size() == 1) {
				log.info("Sensor " + username + " is the only one registered");
				return null;
			}
			
			for (SensorDescription s : sensors.values()) {
				if (sensor.equals(s))
					continue; // sensor can't be neighbour to itself
				double newDistance = haversineFormula(sensor.getLatitude(), sensor.getLongitude(), s.getLatitude(),
						s.getLongitude());
				if ((distance == 0) || (newDistance < distance)) {
					distance = newDistance;
					neighbour = s;
				}
			}

			sensorsNeighbour.put(username, neighbour);
			log.info("Neighbour for sensor " + username + " is " + neighbour.getUsername());
			return new UserAddress(neighbour.getIpAddress(), neighbour.getPort());
		}
	}

	@Override
	public boolean storeMeasurement(Measurement measurement) {
		if(!sensors.containsKey(measurement.getUsername())) {
			log.error("Sensor " + measurement.getUsername() + " is not registered");
			return false;
		}
		log.info("Storing measurement " + measurement.toString());
		measurements.add(measurement);
		log.info("All measurements: " + measurements.toString());
		return true;
	}

	private Double haversineFormula(double lat1, double lon1, double lat2, double lon2) {
		int radius = 6371;
		double dlon = lon2 - lon1;
		double dlat = lat2 - lat1;
		double a = Math.sqrt(Math.sin(dlat / 2)) + Math.cos(lat1) * Math.cos(lat2) * Math.sqrt(Math.sin(dlon / 2));
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double d = radius * c;
		return d;
	}

}
