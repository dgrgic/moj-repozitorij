package hr.fer.rassus.homework.service;

import hr.fer.rassus.homework.model.Measurement;
import hr.fer.rassus.homework.model.SensorDescription;
import hr.fer.rassus.homework.model.UserAddress;

/**
 * 
 * @author dorijan
 * Interface for server implementation
 *
 */

public interface IService {
	
	boolean register(SensorDescription description);
	UserAddress searchNeighbour(String username);
	boolean storeMeasurement(Measurement measurement);
	
}
