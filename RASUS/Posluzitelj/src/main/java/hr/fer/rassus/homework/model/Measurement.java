package hr.fer.rassus.homework.model;

/**
 * 
 * @author dorijan
 * Model that defines measurement
 *
 */
public class Measurement {

	private String username;
	private String parameter;
	private Float averageValue;
	
	public Measurement(String username, String parameter, Float averageValue) {
		super();
		this.username = username;
		this.parameter = parameter;
		this.averageValue = averageValue;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	public Float getAverageValue() {
		return averageValue;
	}

	public void setAverageValue(Float averageValue) {
		this.averageValue = averageValue;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((averageValue == null) ? 0 : averageValue.hashCode());
		result = prime * result + ((parameter == null) ? 0 : parameter.hashCode());
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Measurement other = (Measurement) obj;
		if (averageValue == null) {
			if (other.averageValue != null)
				return false;
		} else if (!averageValue.equals(other.averageValue))
			return false;
		if (parameter == null) {
			if (other.parameter != null)
				return false;
		} else if (!parameter.equals(other.parameter))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Measurement [username=" + username + ", parameter=" + parameter + ", averageValue=" + averageValue
				+ "]";
	}
	
}
