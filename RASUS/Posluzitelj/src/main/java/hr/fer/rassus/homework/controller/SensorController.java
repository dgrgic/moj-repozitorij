package hr.fer.rassus.homework.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.fer.rassus.homework.model.Measurement;
import hr.fer.rassus.homework.model.SensorDescription;
import hr.fer.rassus.homework.model.UserAddress;
import hr.fer.rassus.homework.service.IService;

/**
 * 
 * @author dorijan
 * Handle sensor's requests
 *
 */
@RestController
@RequestMapping(value = "sensors/")
public class SensorController {
	
	@Autowired
	private IService service;
	
	@PostMapping(value = "register/")
	public ResponseEntity<SensorDescription> register(@RequestBody SensorDescription sensor) {
		if(service.register(sensor)) {
			return new ResponseEntity<>(sensor, HttpStatus.CREATED);
		}
		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
	}
	
	@GetMapping(value = "neighbour/{username}")
	public ResponseEntity<UserAddress> searchNeighbour(@PathVariable("username") String username) {
		if(service.searchNeighbour(username) == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(service.searchNeighbour(username), HttpStatus.OK);
	}
	
	@PostMapping(value = "measurement/")
	public ResponseEntity<Measurement> storeMeasurement(@RequestBody Measurement measurement){
		if(service.storeMeasurement(measurement)) {
			return new ResponseEntity<>(measurement, HttpStatus.CREATED);
		}
		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
	}
}
