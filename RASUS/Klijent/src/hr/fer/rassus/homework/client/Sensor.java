package hr.fer.rassus.homework.client;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Sensor {
	
	private String ipAddress = "localhost";
	private int port;
	private double latitude;
	private double longitude;
	private String username;
	private List<String> measurements;
	private String[] parameters;
	
	public Sensor() {
		measurements = new ArrayList<String>();
		readMeasurements();
	}
	
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	@Override
	public String toString() {
		return "Sensor [ipAddress=" + ipAddress + ", port=" + port + ", latitude=" + latitude + ", longitude="
				+ longitude + ", username=" + username + "]";
	}
	
	// api metode
	@SuppressWarnings("unchecked")
	private boolean register() throws IOException {
		URL url = new URL("http://127.0.0.1:8080/sensors/register/");
		HttpURLConnection con = (HttpURLConnection)url.openConnection();
		JSONObject json = new JSONObject();
		
		con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json; utf-8");
		con.setDoOutput(true);
		con.setDoInput(true);
		
		json.put("username", this.getUsername());
		json.put("latitude", this.getLatitude());
		json.put("longitude", this.getLongitude());
		json.put("ipAddress", this.getIpAddress());
		json.put("port", this.getPort());
		
		OutputStreamWriter stream = new OutputStreamWriter(con.getOutputStream());
		stream.write(json.toJSONString());
		stream.flush();
		
		if(con.getResponseCode() != 201) {
			con.disconnect();
			return false;
		}
		con.disconnect();
		return true;
	}
	
	private JSONObject searchNeighbour() throws IOException {
		URL url = new URL("http://127.0.0.1:8080/sensors/neighbour/" + this.username);
		HttpURLConnection con = (HttpURLConnection)url.openConnection();
		
		con.setRequestMethod("GET");
		con.setRequestProperty("Content-Type", "application/json; utf-8");
		con.setDoInput(true); // primam podatke
		
		if(con.getResponseCode() == 404) {
			return null;
		}
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String response = reader.readLine();
		reader.close();
		
		JSONParser parser = new JSONParser();
		JSONObject json = null;
		
		try {
			json = (JSONObject)parser.parse(response);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		con.disconnect();
		return json;
		
	}
	
	@SuppressWarnings("unchecked")
	private void sendMeasurement(String measurementOne, String measurementTwo) throws IOException {
		URL url = new URL("http://127.0.0.1:8080/sensors/measurement/");
		
		HttpURLConnection con;
		JSONObject json;
		OutputStreamWriter stream;
				
		String[] first = measurementOne.split(",");
		List<String> firstMeasurement = new ArrayList<String>();
		Collections.addAll(firstMeasurement, first);
		
		if(firstMeasurement.size() != parameters.length) {
			firstMeasurement.add("0");
		}
		
		if(measurementTwo == null) {
			for(int i=0; i<parameters.length; i++) {
				if(firstMeasurement.get(i).isBlank()) {
					firstMeasurement.remove(i);
					firstMeasurement.add(i, "0");
				}
				
				con = (HttpURLConnection)url.openConnection();
				json = new JSONObject();
				
				con.setRequestMethod("POST");
				con.setRequestProperty("Content-Type", "application/json; utf-8");
				con.setDoOutput(true);
				con.setDoInput(true);
				
				json.put("username", this.getUsername());
				json.put("parameter", parameters[i]);
				json.put("averageValue", Float.valueOf(firstMeasurement.get(i)));
				
				stream = new OutputStreamWriter(con.getOutputStream());
				stream.write(json.toJSONString());
				stream.flush();
				
				if(con.getResponseCode() == 201) {
					System.out.println("Success " + json.toJSONString());
				}
				con.disconnect();
			}
		} else {
			String[] second = measurementTwo.split(",");
			List<String> secondMeasurement = new ArrayList<String>();
			Collections.addAll(secondMeasurement, second);
			
			if(secondMeasurement.size() != parameters.length) {
				secondMeasurement.add("0");
			}
			for(int i=0; i<parameters.length; i++) {
				if(firstMeasurement.get(i).isBlank()) {
					firstMeasurement.remove(i);
					firstMeasurement.add(i, "0");
				}
				if(secondMeasurement.get(i).isBlank()) {
					secondMeasurement.remove(i);
					secondMeasurement.add(i, "0");
				}
				con = (HttpURLConnection)url.openConnection();
				json = new JSONObject();
				
				con.setRequestMethod("POST");
				con.setRequestProperty("Content-Type", "application/json; utf-8");
				con.setDoOutput(true);
				con.setDoInput(true);
				
				json.put("username", this.getUsername());
				json.put("parameter", parameters[i]);
				json.put("averageValue", Util.average(firstMeasurement.get(i), secondMeasurement.get(i)));
				
				stream = new OutputStreamWriter(con.getOutputStream());
				stream.write(json.toJSONString());
				stream.flush();
				
				if(con.getResponseCode() == 201) {
					System.out.println("Success " + json.toJSONString());
				}
				con.disconnect();
			}
		}
	}
	
	// pomocne metode
	private String randomMeasurement(long start) {
		long endTime = System.currentTimeMillis();
		long activeTime = (long) ((endTime - start) / 1000F);
		int index = Math.floorMod(activeTime, 100) + 2;
		return measurements.get(index-1);
	}
	
	private void readMeasurements() {
		try {
			BufferedReader file =  new BufferedReader(new InputStreamReader(new FileInputStream(new File("mjerenja.txt"))));
			String line;
			while((line = file.readLine()) != null) {
				if(line.charAt(0) == 'T') {
					// ovdje spremiti taj prvi redak radi lakseg slanja jsona
					parameters = line.split(",");
					continue;
				}
				measurements.add(line);
			}
			file.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// main
	public static void main(String[] args) {
		Sensor sensor = new Sensor();
		long startTime = System.currentTimeMillis();

		sensor.setLatitude(Util.randomLat());
		sensor.setLongitude(Util.randomLong());
		sensor.setUsername(Util.randomUsername());
		sensor.setPort(Util.randomPort());

		System.out.println(sensor.toString());

		// pokusaj registracije senzora na api
		try {
			if(!sensor.register()) {
				System.out.println("Nisam se registirao");
				System.exit(0);
			}
		} catch(IOException e) {
			e.printStackTrace();
			System.exit(0);
		}
		
		System.out.println("Registrirao sam se. Idem dalje");
		Thread server = new Thread(new MultithreadedServer(sensor.getPort()));
		server.start();
		
		// pokusaj pronaci susjeda
		JSONObject neighbour = null;
		while(neighbour == null) {
			try {
				neighbour = sensor.searchNeighbour();
				System.out.println("Uspio sam dobiti susjeda " + neighbour);	
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(0);
			}
			
			String myMeasure;
			if(neighbour != null) {
				// susjed je pronaden
				try {
					Socket client = new Socket(String.valueOf(neighbour.get("ipAddress")), 
							Integer.parseInt(String.valueOf(neighbour.get("port"))));
					BufferedReader inFromServer = new BufferedReader(new InputStreamReader(
							client.getInputStream()));
					String neighbourMeasure;
					while((neighbourMeasure = inFromServer.readLine()) != null) {
						// dohvacaj podatke, umjeravaj ih i salji na api
						myMeasure = sensor.randomMeasurement(startTime);
						
						System.out.println("Dobio sam svoje mjerenje " + myMeasure);
						System.out.println("Dobio sam od susjeda " + neighbourMeasure);	
						try {
							sensor.sendMeasurement(myMeasure, neighbourMeasure);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					inFromServer.close();
					client.close();

				} catch (UnknownHostException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				
			} else {
				// nema susjeda, salji svoje mjerenje
				myMeasure = sensor.randomMeasurement(startTime);
				System.out.println("Dobio sam svoje mjerenje " + myMeasure);
				try {
					sensor.sendMeasurement(myMeasure, null);
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
