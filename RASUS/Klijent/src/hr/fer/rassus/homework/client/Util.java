package hr.fer.rassus.homework.client;

import java.util.Random;

/**
 * 
 * @author dorijan
 *
 */
public class Util {

	public static double randomLat() {
		double lat = 15.87 + new Random().nextDouble() * (16 - 15.87);
		return lat;
	}
	
	public static double randomLong() {
		double lat = 45.75 + new Random().nextDouble() * (45.85 - 45.75);
		return lat;
	}
	
	public static int randomPort() {
		int port = 1024 + new Random().nextInt(10000);
		return port;
	}
	
	public static String randomUsername() {
		String username = new String("sensor");
		int rand = new Random().nextInt(10000) + 1;
		username += String.valueOf(rand);
		return username;
	}
	
	public static float average(String left, String right) {
		float result = (float)(Float.valueOf(left) + Float.valueOf(right)) / 2;
		return result;
		
	}
}
