/*
 * This code has been developed at the Department of Telecommunications,
 * Faculty of Electrical Engineering and Computing, University of Zagreb.
 */
package hr.fer.rassus.homework.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
*
* @author Ivana Podnar �arko <ivana.podnar@fer.hr>
*/
public class Worker implements Runnable{
	private final Socket clientSocket;
	private final AtomicBoolean isRunning;
	private final AtomicInteger activeConnections;
	private List<String> measurements;
	
	public Worker(Socket clientSocket, AtomicBoolean isRunning, AtomicInteger activeConnections) {
		this.clientSocket = clientSocket;
		this.isRunning = isRunning;
		this.activeConnections = activeConnections;
		measurements = new ArrayList<String>();
		readMeasurements();
	}
	
	private void readMeasurements() {
		try {
			BufferedReader file =  new BufferedReader(new InputStreamReader(new FileInputStream(new File("mjerenja.txt"))));
			String line;
			while((line = file.readLine()) != null) {
				if(line.charAt(0) == 'T') {
					continue;
				}
				measurements.add(line);
			}
			file.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// metoda koja vraca redak
	private String randomMeasurement(long start) {
		long endTime = System.currentTimeMillis();
		long activeTime = (long) ((endTime - start) / 1000F);
		int index = Math.floorMod(activeTime, 100) + 2;
		return measurements.get(index-1);
	}

	@Override
	public void run() {
		long start = System.currentTimeMillis();
		System.out.println("Worker start");
		try (// create a PrintWriter from an existing OutputStream
				PrintWriter outToClient = new PrintWriter(new OutputStreamWriter(
						clientSocket.getOutputStream()), true);) {

			while(true) {

				String measure = randomMeasurement(start);
				System.out.println("Worker sending measure " + measure);
				outToClient.println(measure);
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		} catch (IOException ex) {
			System.err.println("Exception caught when trying to read or send data: " + ex);
		}
	}

}
