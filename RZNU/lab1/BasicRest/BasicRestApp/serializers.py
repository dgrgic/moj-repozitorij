from rest_framework import serializers
from .models import Photo
from users.serializers import UserSerializer


class PhotoSerializer(serializers.ModelSerializer):
    author = UserSerializer(many=False, read_only=True)

    class Meta:
        model = Photo
        fields = [
            'id',
            'author',
            'created_at',
            'description',
            'photo'
        ]
