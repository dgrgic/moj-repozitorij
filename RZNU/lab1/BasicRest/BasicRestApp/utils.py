from rest_framework import test, status
from django.contrib.auth import get_user_model
from users.serializers import UserSerializer
User = get_user_model()


class TestBase(test.APITestCase):

    def login_user(self, username, password):
        url = 'http://localhost:8000/api/login/'
        data = {
            'username': username,
            'password': password
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        return response.data['token']
