from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import PhotoViewSet

router = DefaultRouter()
router.register(r'photos', PhotoViewSet)

urlpatterns = [
    path('', include(router.urls)),
    # api/photos/ GET -> lista svih fotografija (autor, datum, opis, slika), POST -> (opis, slika) stvori novi objekt
    # api/photos/<int:pk>/ GET -> dohvati objekt, PUT/PATCh -> ažuriraj, DELETE -> obriši
]
