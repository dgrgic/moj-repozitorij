from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()


def photo_url(instance, filename):
    return 'user_{0}/photos/{1}'.format(instance.author.id, filename)


class Photo(models.Model):
    author = models.ForeignKey(User, related_name='photos', on_delete=models.CASCADE, default='')
    created_at = models.DateTimeField(auto_now_add=True)
    description = models.CharField(max_length=256)
    photo = models.ImageField(upload_to=photo_url, default='')
