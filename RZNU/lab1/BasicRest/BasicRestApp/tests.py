from .utils import TestBase
from rest_framework import test, status


class UserTests(TestBase):

    def test_user_registration(self):
        url = 'http://localhost:8000/api/users/registration/'
        data = {
            'username': 'grgutin',
            'password': 'kulpassword'
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_user_login(self):
        self.test_user_registration()
        url = 'http://localhost:8000/api/login/'
        data = {
            'username': 'grgutin',
            'password': 'kulpassword'
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_login_fail(self):
        self.test_user_registration()
        url = 'http://localhost:8000/api/login/'
        data = {
            'username': 'grgutin',
            'password': 'kulpassword'
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_photo_create(self):
        self.test_user_registration()
        if self.client.login(username='grgutin', password='kulpassword'):
            token = self.login_user(username='grgutin', password='kulpassword')
            client = test.APIClient()
            client.credentials(HTTP_AUTHORIZATION='Token ' + token)

            url = 'http://localhost:8000/api/photos/'
            data = {
                'title': 'naslov',
                'description': 'opis',
            }

            response = client.post(url, data, format='json')
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_photo_create_fail(self):
        self.test_user_registration()
        if self.client.login(username='grgutin', password='kulpassword'):
            token = self.login_user(username='grgutin', password='kulpassword')
            client = test.APIClient()
            client.credentials(HTTP_AUTHORIZATION='Token ' + token)

            url = 'http://localhost:8000/api/photos/'
            data = {
                'title': 'naslov',
            }

            response = client.post(url, data, format='json')
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_get_user_photos(self):
        self.test_user_registration()
        if self.client.login(username='grgutin', password='kulpassword'):
            token = self.login_user(username='grgutin', password='kulpassword')
            client = test.APIClient()
            client.credentials(HTTP_AUTHORIZATION='Token ' + token)

            url = 'http://localhost:8000/api/users/1/photos/'
            response = client.get(url, format='json')
            self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_user_photos_fail(self):
        self.test_user_registration()
        if self.client.login(username='grgutin', password='kulpassword'):
            token = self.login_user(username='grgutin', password='kulpassword')
            client = test.APIClient()
            client.credentials(HTTP_AUTHORIZATION='Token ' + token)

            url = 'http://localhost:8000/api/users/9/photos/'
            response = client.get(url, format='json')
            self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
