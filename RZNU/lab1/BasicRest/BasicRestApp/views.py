from django.shortcuts import render
from rest_framework import views, generics, viewsets, response, status, permissions
from rest_framework.response import Response
from .models import Photo
from .serializers import PhotoSerializer


class PhotoViewSet(viewsets.ModelViewSet):
    queryset = Photo.objects.all()
    http_method_names = ['get', 'post', 'put', 'delete']
    serializer_class = PhotoSerializer
    permission_classes = [permissions.IsAuthenticated]

    def perform_create(self, serializer):
        serializer.save(author_id=self.request.user.id)
