.. BasicRest documentation master file, created by
   sphinx-quickstart on Thu Oct 10 19:40:36 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

###########################
BasicRest API documentation
###########################
Requests which this API handles are based on **HTTP protocol**.
Every request should define header parameters **content** and **accept**.For some endpoints **authorizaton**
parameter needs to be included. Value for parameters content and accept is **application/json** (because we use JSON format for communication)
and value for authorization parameter is based on user's token : **Token <token>**. Base URL for the whole project is http://localhost:8000/api/. API defines
set of two resource groups, **USERS** and **PHOTOS**.

Users API
=========

Registration
------------
:Description: Register a new user. Password must have at least eight characters!
:Method: POST
:URL: users/registration/
:Request:

   ================= ===========
      Field             Type
   ================= ===========
   **username**         String
   **password**         String
   ================= ===========

:Response:

   ================= ===========
      Field             Type
   ================= ===========
   **username**         String
   **password**         String
   ================= ===========

Example of valid request
------------------------
::

   POST /users/registration/ HTTP/1.1
   {
      "username": "grga",
      "password": "kulpassword"
   }

::

   HTTP/1.1 201 Created
   {
      "username": "grga",
      "password": "kulpassword"
   }


Example of invalid requests
---------------------------

**Username already taken**
::

   POST /users/registration/ HTTP/1.1
   {
      "username": "grga",
      "password": "kulpassword"
   }

::

   HTTP/1.1 400 Bad request
   {
      "username": [
         "This field must be unique."
      ]
   }

**Password too short**
::

   POST /users/registration/ HTTP/1.1
   {
      "username": "ivanaa",
      "password": "pass"
   }

::

   HTTP/1.1 400 Bad request
   {
       "password": [
           "Ensure this field has at least 8 characters."
      ]
   }


Login
-----

:Description: Login endpoint for obtaining *rest_authtoken* **Token** object which is used to access resources.
:Method: POST
:URL: /login/
:Request:

   ================= ===========
      Field             Type
   ================= ===========
   **username**         String
   **password**         String
   ================= ===========

:Response:

   ================= ===========
      Field             Type
   ================= ===========
   **Token**         String
   ================= ===========

Example of valid request
------------------------
::

   POST /login/ HTTP/1.1
   {
      "username": "grga";
      "password": "kulpassword"
   }

::

   HTTP/1.1 200 Ok
   {
      "token" : "6e3d06c4d9188f0a072c7f9b9ff73ffaad1f98a1"
   }

Example of invalid requests
---------------------------
::

   POST /login/ HTTP/1.1
   {
      "username": "grgutin";
      "password": "kulpassword"
   }

::

   HTTP/1.1 400 Bad request
   {
      "non_field_errors": [
           "Unable to log in with provided credentials."
      ]
   }


List of users
-------------
:Description: Get a list of all users.
:Method: GET
:URL: /users/
:Request:

:Response:

   ================= ===========
      Field             Type
   ================= ===========
   **id**              String
   **username**        String
   ================= ===========

Single user
-------------
:Description: Get a single user object.
:Method: GET
:URL: /users/<int:pk>/
:Request:

:Response:

   ================= ===========
      Field             Type
   ================= ===========
   **id**              String
   **username**        String
   ================= ===========

Update/Delete user
------------------
:Description: To update a user, use PATCH method. For deletion user DELETE method.
:Method: PATCH/DELETE
:URL: /users/<int:pk>/
:Request:

   ================= ===========
      Field             Type
   ================= ===========
   **username**        String
   ================= ===========

:Response:

   ================= ===========
      Field             Type
   ================= ===========
   **id**              String
   **username**        String
   ================= ===========

Photos API
==========

Create photo
-------------
:Description: Endpoint for creating new photo object.
:Method: POST
:URL: /photos/
:Request:

   ================= ===========
      Field             Type
   ================= ===========
   **description**   String
   **photo**         Photo
   ================= ===========

:Response:

   ================= ===========
      Field             Type
   ================= ===========
   **description**   String
   **photo**         Photo
   **author**        User
   **created_at**    Timestamp
   ================= ===========

List of all photos
------------------
:Description: Get a list of all photos.
:Method: GET
:URL: /photos/
:Request:

:Response:

   ================= ===========
      Field             Type
   ================= ===========
   **description**   String
   **photo**         Photo
   **author**        User
   **created_at**    Timestamp
   ================= ===========

Single photo
------------------
:Description: Get a single photo object
:Method: GET
:URL: /photos/<int:pk>/
:Request:

:Response:

   ================= ===========
      Field             Type
   ================= ===========
   **description**   String
   **photo**         Photo
   **author**        User
   **created_at**    Timestamp
   ================= ===========

Update/Delete photo
-------------------

:Description: To update a photo, use PATCH method. For deletion use DELETE method
:Method: PATCH/DELETE
:URL: /photos/<int:pk>/
:Request:

   ================= ===========
      Field             Type
   ================= ===========
   **description**   String
   **photo**         Photo
   ================= ===========

:Response:

   ================= ===========
      Field             Type
   ================= ===========
   **description**   String
   **photo**         Photo
   **author**        User
   **created_at**    Timestamp
   ================= ===========


User's photos
-------------------

:Description: Get a list of photos linked to certain user.
:Method: GET
:URL: /users/<int:pk>/photos/
:Request:

:Response:

   ================= ===========
      Field             Type
   ================= ===========
   **description**   String
   **photo**         Photo
   **author**        User
   **created_at**    Timestamp
   ================= ===========

