from django.urls import path, include
from rest_framework.routers import DefaultRouter
from rest_framework.authtoken.views import obtain_auth_token
from .views import UserViewSet

router = DefaultRouter()

router.register(r'users', UserViewSet)

urlpatterns = [
    path('', include(router.urls)),
    # api/users/registration/ POST -> registracija (username, password) :> (username, password)
    # api/users/ GET -> lista korisnika
    # api/users/<int:pk>/ GET -> dohvati objekt, PUT/PATCH -> ažuriraj objekt, DELETE -> obriši objekt
    # api/users/<int:pk>/photos/ GET -> dohvati sve slike tog usera
    path('login/', obtain_auth_token),
    # api/login/ POST -> login (username, password) :> (token)
]
