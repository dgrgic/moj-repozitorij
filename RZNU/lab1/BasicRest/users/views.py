from django.shortcuts import render
from django.contrib.auth import get_user_model
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from .serializers import *
from BasicRestApp.serializers import PhotoSerializer

User = get_user_model()


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    # lookup_field = 'username'
    http_method_names = ['get', 'patch', 'post', 'delete']

    @action(methods=['post'], detail=False, permission_classes=[AllowAny], url_path='registration')
    def user_registration(self, request):
        serializer = UserRegistrationSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            user.is_active = True
            user.save()
            if user:
                return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(methods=['get'], detail=True, permission_classes=[IsAuthenticated])
    def photos(self, request, pk):
        try:
            user = User.objects.get(id=pk)
            photos = PhotoSerializer(user.photos, many=True)
            return Response(photos.data)
        except:
            return Response(status=status.HTTP_404_NOT_FOUND)
