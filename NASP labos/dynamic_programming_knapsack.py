items = [
    (1,5,3), 
    (1,3,2),
    (2,1,9),
    (2,7,4),
    (3,6,1),
    (3,9,10)]

max_weight = 15
categories = 0 # number of categories
items_per_category = [] # sum of items per category
items_in_category = [] # also sum of items per category but used for seeking offset

pom = 0
for i in items:
    if pom != i[0]:
        categories += 1
        pom = i[0]

print('Categories: {0}, latest category: {1}'.format(categories, pom))

for i in range(categories):
    items_per_category.append(0)
    items_in_category.append(0)

print('Items per category:', items_per_category)

# table with calculated weights for specific item
weight_table = [[0 for i in range(max_weight)]for i in range(len(items))]
# table with boolean values for specific item
keep_table = [[0 for i in range(max_weight)]for i in range(len(items))]

print('\nWeight table:\n')
for row in weight_table:
    print(row)

print('\nKeep table:\n')
for row in keep_table:
    print(row)
print()

row = 0
previous_max = 0
previous_max_offset = 0
max_location = (0,0) # for choosing process

for c in range(1, categories+1):
    # iterate over categories
    for r in items:
        # iterate over items (rows)
        if c == 1:
            # first category, insert item's value in table
            if r[0] == c:
                items_per_category[c-1] += 1
                for w in range(max_weight):
                    # iterate over weights (columns)
                    if r[1] <= w:
                        weight_table[row][w] = r[2]
                        keep_table[row][w] = 1
                row += 1
        else:
            # not a first category, calculate max value from previous category
            if r[0] == c:
                items_per_category[c-1] += 1
                for w in range(max_weight):
                    # iterate over weights (columns)
                    for p in range(1, items_per_category[c-2]+1):
                        # first iterate over previous values in the same coulmn
                        # to find max value for future calculations
                        if previous_max < weight_table[row-(p+items_in_category[c-1])][w]:
                            previous_max = weight_table[row-(p+items_in_category[c-1])][w]
                            max_location = (row-(p+items_in_category[c-1]), w)
                    #print(
                    #'Najveci od prethodnih za mjesto {0},{1} je {2}'.format(
                    #    row, w, previous_max
                    #))
                    if r[1] > w:
                        weight_table[row][w] = previous_max

                    else:
                        # optimizirati iteriranje po stupcima prethodne kategorije
                        # previous_max_offset spremati u listu od max_weight elemenata
                        for p in range(1, items_per_category[c-2]+1):
                            if previous_max_offset < weight_table[row-(p+items_in_category[c-1])][w-r[1]]:
                                previous_max_offset = weight_table[row-(p+items_in_category[c-1])][w-r[1]]


                        new_value = r[2] + previous_max_offset
                        #print(
                        #'Najveci lijevi za mjesto {0},{1} je {2}'.format(
                        #    row, w, previous_max_offset
                        #))
                        if new_value >= previous_max:
                            weight_table[row][w] = new_value
                            keep_table[row][w] = 1
                            #print('Upisujem na mjesto {0},{1} : {2}'.format(
                            #    row, w, new_value
                            #))
                        else:
                            weight_table[row][w] = previous_max
                            #print('Upisujem na mjesto {0},{1} : {2}'.format(
                            #    row, w, previous_max
                            #))
                    # end of column
                    previous_max, previous_max_offset = 0, 0
                row += 1
                items_in_category[c-1] += 1

print('\nNew weight table:\n')
for r in weight_table:
    print(r)

print('\nNew items per category:', items_per_category)

print('\nNew keep table:\n')
for r in keep_table:
    print(r)


row -= 1
items_in_category.clear()
for i in range(categories):
    items_in_category.append(0)
flag = False

while(True):
    if keep_table[row][w] == 1:
        if not flag:
            if items[row][0] == 1:
                flag = True
            print('Item to choose: {0}'.format(items[row]))
            w -= items[row][1]
            row -= items_per_category[categories-1]
            categories -= 1
            if row < 0 or w < 0:
                break
        else:
            categories -= 1
            continue
    else:
        if max_location[1] == w:
            #print('Maks lokacija uspjesno pronadena', max_location)
            row -= (row - max_location[0])
            if row < 0:
                break
        else:
            row -= 1
            if row < 0:
                break
